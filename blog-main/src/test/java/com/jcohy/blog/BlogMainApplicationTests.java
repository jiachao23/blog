package com.jcohy.blog;

import com.jcohy.blog.model.Menu;
import com.jcohy.blog.model.User;
import com.jcohy.blog.repository.MenuRepository;
import com.jcohy.blog.repository.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BlogMainApplicationTests {

	@Autowired
	UserRepository userRepository;

	@Autowired
	MenuRepository menuRepository;
	@Test
	public void contextLoads() {
		User user = userRepository.findUserById(1L);
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder(6);

		// 加密
		String encodedPassword = passwordEncoder.encode(user.getPassword().trim());
		user.setPassword(encodedPassword);
		userRepository.save(user);
	}

	@Test
	public void test1(){
		List<Menu> menuList = menuRepository.findAllByVisibleTrueAndTypeBeforeAndDeletedFalse(2);
		System.out.println(menuList);
	}
}
