package com.jcohy.blog;

import com.jcohy.blog.intercepter.CommonIntercepter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@SpringBootApplication
public class BlogMainApplication extends WebMvcConfigurerAdapter {

	@Autowired
	private CommonIntercepter commonInterceptor;
//	@Autowired
//	private LoginIntercepter loginIntercepter;
	public static void main(String[] args) {
		SpringApplication springApplication = new SpringApplication(BlogMainApplication.class);
		//
		springApplication.run(args);
	}

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		super.addInterceptors(registry);
		registry.addInterceptor(commonInterceptor)
				.addPathPatterns("/**");
//		registry.addInterceptor(loginIntercepter).addPathPatterns("/admin/**")
//				.excludePathPatterns("/admin");
	}

	@Override
	public void addViewControllers(ViewControllerRegistry registry) {

		//前台首页
		registry.addViewController("/").setViewName("front/index");
		//registry.addViewController("/").setViewName("redirect:/love");
		//登录主页
		registry.addViewController("/admin").setViewName("redirect:/login");
		//倒计时主页
		registry.addViewController("/love").setViewName("/love");
		//文章模块
//		registry.addViewController("/article/index").setViewName("/front/article/index");

		//后台
		//后台主页
		registry.addViewController("/admin/index").setViewName("admin/main");
		//博客管理主页
		registry.addViewController("/admin/blog/index").setViewName("admin/blog/index");
		//分类管理
		registry.addViewController("/admin/category/index").setViewName("admin/category/index");
		//资源管理
		registry.addViewController("/admin/resource/index").setViewName("admin/resource/index");
		//标签管理
		registry.addViewController("/admin/tag/index").setViewName("admin/tag/index");
		//链接管理
		registry.addViewController("/admin/link/index").setViewName("admin/link/index");
		//系统设置
		registry.addViewController("/admin/sys/index").setViewName("admin/sys/index");
		//公告首页
		registry.addViewController("/admin/notice/index").setViewName("admin/notice/index");
		//文件上传
		registry.addViewController("/upload/index").setViewName("front/upload");
		//添加用户模块
		registry.addViewController("/admin/user/index").setViewName("admin/user/index");
	}
}
