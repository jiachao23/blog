package com.jcohy.blog.exception;//package com.star.appstore.handle;

import com.jcohy.blog.common.Response;
import com.jcohy.blog.enums.LoginStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Copyright  : 2015-2033 Beijing Startimes Communication & Network Technology Co.Ltd
 *
 * @author jiac
 * @date 2017/10/18 10:22
 * ClassName  : GlobalExceptionHandler
 * Description  :
 */
@ControllerAdvice
public class GlobalExceptionHandler {

    private static Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ExceptionHandler(NoSuchUserException.class)
    @ResponseBody
    public Response NoSuchUserException(NoSuchUserException ex) {
        logger.error("code =  {},msg = {}",19931,ex.getMessage());
        Response response = new Response();
        response.setCode(LoginStatus.USER_NOT_EXIST.getCode());
        response.setMessage(LoginStatus.USER_NOT_EXIST.getMsg());
        return response;
    }

}
