package com.jcohy.blog.exception;

/**
 * Created by jiac on 2018/7/18.
 * ClassName  : com.jcohy.exception
 * Description  :
 */
public class ParamErrorException extends RuntimeException {


    public ParamErrorException(String message) {
        super(message);
    }

}
