package com.jcohy.blog.exception;

/**
 * Created by jiac on 2018/7/18.
 * ClassName  : com.jcohy.exception
 * Description  :
 */
public class NoSuchUserException extends RuntimeException {

    public NoSuchUserException(String message) {
        super(message);
    }
}
