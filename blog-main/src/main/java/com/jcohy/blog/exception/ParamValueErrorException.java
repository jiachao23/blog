package com.jcohy.blog.exception;

/**
 * Created by jiac on 2018/7/18.
 * ClassName  : com.jcohy.exception
 * Description  :
 */
public class ParamValueErrorException extends RuntimeException {
    public ParamValueErrorException(String message) {
        super(message);
    }
}
