package com.jcohy.blog.directive;

import com.jcohy.blog.model.Resource;
import com.jcohy.blog.service.ResourceService;
import freemarker.core.Environment;
import freemarker.template.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Created by jiac on 2018/6/26.
 * ClassName  : com.jcohy.directive
 * Description  :
 */
@Component
public class ResourceDirective implements TemplateDirectiveModel {

    @Autowired
    private ResourceService resourceService;
    @Override
    public void execute(Environment environment, Map map, TemplateModel[] templateModels, TemplateDirectiveBody templateDirectiveBody) throws TemplateException, IOException {
        List<Resource> resourceList = resourceService.findVisible();
        environment.setVariable("list",new DefaultObjectWrapperBuilder(Configuration.VERSION_2_3_25).build().wrap(resourceList));
        if (templateDirectiveBody != null) {
            templateDirectiveBody.render(environment.getOut());
        }
    }
}
