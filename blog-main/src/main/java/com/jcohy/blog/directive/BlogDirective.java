package com.jcohy.blog.directive;

import com.jcohy.blog.dto.BlogDto;
import com.jcohy.blog.service.BlogService;
import freemarker.core.Environment;
import freemarker.template.*;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Copyright  : 2015-2033 Beijing Startimes Communication & Network Technology Co.Ltd
 * Created by jiac on 2018/1/18 15:40.
 * ClassName  : BlogDirective
 * Description  :
 */
@Component
public class BlogDirective implements TemplateDirectiveModel {

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(BlogDirective.class);
    @Autowired
    private BlogService blogService;

    @Override
    public void execute(Environment environment, Map params, TemplateModel[] templateModels, TemplateDirectiveBody templateDirectiveBody) throws TemplateException, IOException {
        List<BlogDto> list = new ArrayList<BlogDto>();
        if(params != null && params.containsKey("type")){
            String type = params.get("type").toString();
            if("readNum".equalsIgnoreCase(type)){
                list = blogService.findHotN(5);
                logger.warn("Hot:{}",list.size());
            }

            if("shareNum".equalsIgnoreCase(type)){
                list = blogService.findFeaturedN(5);
                logger.warn("Featured:{}",list.size());
            }if("all".equalsIgnoreCase(type)){
                list = blogService.findAllByPrivacy();
                logger.warn("All:{}",list.size());
            }
            logger.error(list.toString());
            environment.setVariable("list", new DefaultObjectWrapperBuilder(Configuration.VERSION_2_3_25).build().wrap(list));
            if (templateDirectiveBody != null) {
                templateDirectiveBody.render(environment.getOut());
            }
        }
    }
}
