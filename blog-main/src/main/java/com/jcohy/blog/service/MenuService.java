package com.jcohy.blog.service;

import com.jcohy.blog.model.Menu;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Copyright  : 2015-2033 Beijing Startimes Communication & Network Technology Co.Ltd
 * Created by jiac on 2018/1/31 09:53.
 * ClassName  : MenuService
 * Description  :
 */
public interface MenuService {


    /**
     * 获取某用户有权限访问的菜单
     *
     * @param userId
     * @return
     */
    List<Menu> getMenuListTree(Long userId);

    /**
     * 分页获取所有菜单，不包括已删除的数据
     *
     * @return
     */
    Page<Menu> getAllMenus(Pageable request);


    /**
     * 查询可见菜单
     * @return
     */
    List<Menu> findVisible();


    /**
     * 增加，修改
     * @param menu
     */
    void saveOrUpdate(Menu menu, Long userId);


    /**
     *
     * 根据id删除
     * @param id
     */
    void delete(Long id);
}
