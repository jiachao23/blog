package com.jcohy.blog.service.impl;

import com.jcohy.blog.exception.ParamErrorException;
import com.jcohy.blog.model.Link;
import com.jcohy.blog.repository.LinkRepository;
import com.jcohy.blog.service.LinkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Copyright  : 2015-2033 Beijing Startimes Communication & Network Technology Co.Ltd
 * Created by jiac on 2017/12/26 13:32.
 * ClassName  : LinkServiceImpl
 * Description  :
 */
@Service
public class LinkServiceImpl implements LinkService {

    @Autowired
    private LinkRepository linkRepository;

    @Override
    public List<Link> findAll() {
        return linkRepository.findAll();
    }

    @Override
    public List<Link> findAllVisiable() {
        return linkRepository.findAllByStatus(1);
    }

    @Override
    public Page<Link> findAll(Pageable Pageable) {
        return linkRepository.findAll(Pageable);
    }

    @Override
    public Link findById(Long id) {
        return linkRepository.findOne(id);
    }

    @Override
    public Link saveOrUpdate(Link link,Long userId) {
        if(link == null){
            throw new ParamErrorException("没有友情链接");
        }
        Link _link = null;
        if(link.getId() == null){
            _link = link;
            _link.setStatus(1);
            _link.setCreatedBy(1L);
            _link.setDeleted(false);
            _link.setCreatedBy(userId);
            _link.setCreatedTime(new Date());
        }else{
            _link = linkRepository.findOne(link.getId());
            _link.setTitle(link.getTitle());
            _link.setUrl(link.getUrl());
            _link.setDescription(link.getDescription());
            _link.setEmail(link.getEmail());
            _link.setDeleted(false);
            _link.setCreatedBy(userId);
            _link.setUpdatedTime(new Date());

        }
        _link.setUpdatedBy(userId);
        _link.setUpdatedTime(new Date());
        return linkRepository.saveAndFlush(_link);
    }

    @Override
    public void delete(Long id) {
        linkRepository.delete(id);
    }

    @Override
    public Link change(Long id, String type) {
        Link link = linkRepository.findOne(id);
        switch (type){
            case "status":
                link.setStatus(link.getStatus() == 0 ? 1:0);
                break;
            default:
                break;
        }
        return linkRepository.save(link);
    }
}
