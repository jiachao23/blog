package com.jcohy.blog.service;

import com.jcohy.blog.model.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Copyright  : 2015-2033 Beijing Startimes Communication & Network Technology Co.Ltd
 * Created by jiac on 2018/2/6 14:56.
 * ClassName  : ResourceService
 * Description  :
 */
public interface ResourceService {
    /**
     * 查询可见资源
     * @return
     */
    List<Resource> findVisible();


    /**
     * 查询所有资源
     * @return
     */
    List<Resource> findAll();

    /**
     * 分页查询
     * @param pageable
     * @return
     */
    Page<Resource> findAll(Pageable pageable);

    /**
     * 通过Id查找
     * @param id
     * @return
     */
    Resource findById(Long id);

    /**
     * 增加，修改
     * @param resource
     */
    void saveOrUpdate(Resource resource, Long userId);

    /**
     *
     * 根据id删除
     * @param id
     */
    void delete(Long id);

    /**
     * 改变状态。
     * @param id
     * @param type
     */
    void change(Long id, String type);
}
