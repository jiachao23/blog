package com.jcohy.blog.service;

import com.jcohy.blog.common.Response;
import com.jcohy.blog.controller.admin.BlogSearchRequest;
import com.jcohy.blog.dto.BlogDto;
import com.jcohy.blog.model.Blog;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Copyright  : 2017- www.jcohy.com
 * Created by jiac on 20:18 2017/12/20
 * Email: jia_chao23@126.com
 * ClassName: BlogService
 * Description:
 **/
public interface BlogService {

        /**
         * 获取浏览量Top N
         * @param n
         * @return
         */
        List<BlogDto> findHotN(int n);

        /**
         * 获取推荐Top N
         * @param i
         * @return
         */
        List<BlogDto> findFeaturedN(int i);


        /**
         * 查询所有
         * @return
         */
        List<BlogDto> findAll();

        /**
         * 查询所有
         * @return
         */
        List<BlogDto> findAllByPrivacy(Pageable pageable);

        /**
         * 查询所有
         * @return
         */
        List<BlogDto> findAllByPrivacy();

        /**
         * 分页查询
         * @param blogSearchRequest
         * @return
         */
        List<BlogDto> findAll(BlogSearchRequest blogSearchRequest);

        /**
         * 通过Id查找
         * @param id
         * @return
         */
        Blog findById(Long id);

        /**
         * 增加，修改
         * @param blog
         */
        Response saveOrUpdate(BlogDto blog);


        /**
         *
         * 根据id删除
         * @param id
         */
        void delete(Long id);


        /**
         * 改变状态。
         * @param id
         * @param type
         */
        void change(Long id, String type);
}
