package com.jcohy.blog.service.impl;

import com.jcohy.blog.exception.ParamErrorException;
import com.jcohy.blog.model.Tag;
import com.jcohy.blog.repository.TagRepository;
import com.jcohy.blog.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Copyright  : 2015-2033 Beijing Startimes Communication & Network Technology Co.Ltd
 * Created by jiac on 2017/12/26 09:37.
 * ClassName  : TagServiceImpl
 * Description  :
 */
@Service
public class TagServiceImpl implements TagService {


    @Autowired
    private TagRepository tagRepository;

    @Override
    public List<Tag> findAllVisiable() {
        return  tagRepository.findAllByVisible(true);
    }

    @Override
    public List<Tag> findAll() {
        return tagRepository.findAll();
    }

    @Override
    public Page<Tag> findAll(Pageable pageable) {
        return tagRepository.findAll(pageable);
    }

    @Override
    public Tag findById(Long id) {
        return tagRepository.findOne(id);
    }

    @Override
    public Tag findByName(String name) {
        return tagRepository.findByName(name);
    }

    @Override
    public Tag saveOrUpdate(Tag tag,Long userId) {

        if(tag == null){
            throw new ParamErrorException("标签对象为空");
        }
        Tag _tag = null;
        if(tag.getId() == null){
            _tag = tag;
            _tag.setCreatedTime(new Date());
            _tag.setVisible(true);
            _tag.setCount(0);
            _tag.setDeleted(false);
            _tag.setCreatedBy(userId);
            _tag.setCreatedTime(new Date());
        }else{
            _tag = findById(tag.getId());
            _tag.setName(tag.getName());
            _tag.setDeleted(false);
            _tag.setCreatedBy(userId);
            _tag.setUpdatedTime(new Date());
        }
        _tag.setUpdatedBy(userId);
        _tag.setUpdatedTime(new Date());
        return tagRepository.save(_tag);
    }

    @Override
    public void saveByName(String name) {

    }

    @Override
    public void delete(Long id) {
        tagRepository.delete(id);
    }

    @Override
    public Tag change(Long id,String type) {
        Tag tag = tagRepository.findOne(id);
        switch (type){
            case "status":
                tag.setVisible(!tag.isVisible());
                break;
            default:
                break;
        }
        return tagRepository.save(tag);
    }
}
