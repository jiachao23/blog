package com.jcohy.blog.service;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;

/**
 * Copyright  : 2015-2033 Beijing
 * Created by jiac on 2018/2/6 15:15.
 * ClassName  : BaseService
 * Description  :
 */
public class   BaseService<T> {
    /**
     * 建立分页排序请求
     *
     * @param page
     * @param size
     * @param direction
     * @param sortCol
     * @return
     */
    protected PageRequest buildPageRequest(int page, int size, String sortCol, String direction, String defaultSortCol) {
        if (page < 0) {
            page = 0;
        }
        if(size < 0) {
            size = 10;
        }
        if (StringUtils.isNotBlank(direction) && StringUtils.isNotBlank(sortCol)) {
            if ("ASC".equalsIgnoreCase(direction)) {
                Sort sort = new Sort(Sort.Direction.ASC, sortCol);
                return new PageRequest(page, size, sort);
            } else if ("DESC".equalsIgnoreCase(direction)) {
                Sort sort = new Sort(Sort.Direction.DESC, sortCol);
                return new PageRequest(page, size, sort);
            }
        }
        Sort sort = new Sort(Sort.Direction.DESC, defaultSortCol);
        return new PageRequest(page, size, sort);
    }

}
