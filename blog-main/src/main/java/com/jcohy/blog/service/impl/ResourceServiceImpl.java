package com.jcohy.blog.service.impl;

import com.jcohy.blog.model.Resource;
import com.jcohy.blog.repository.ResourceRepository;
import com.jcohy.blog.service.ResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Copyright  : 2015-2033 Beijing Startimes Communication & Network Technology Co.Ltd
 * Created by jiac on 2018/2/6 14:57.
 * ClassName  : ResourceServiceImpl
 * Description  :
 */

@Service
public class ResourceServiceImpl implements ResourceService {


    @Autowired
    private ResourceRepository resourceRepository;

    @Value("${file.download}")
    private String downloadUrl;

    @Override
    public List<Resource> findVisible() {
        return resourceRepository.findAllByVisible(true);
    }

    @Override
    public List<Resource> findAll() {
        return resourceRepository.findAll();
    }

    @Override
    public Page<Resource> findAll(Pageable pageable) {
        return resourceRepository.findAll(pageable);
    }

    @Override
    public Resource findById(Long id) {
        return resourceRepository.findOne(id);
    }

    @Override
    public void saveOrUpdate(Resource resource,Long userId) {
        resource.setDeleted(false);
        resource.setCreatedBy(userId);
        resource.setCreatedTime(new Date());
        resource.setUpdatedBy(userId);
        resource.setUpdatedTime(new Date());
        resourceRepository.saveAndFlush(resource);
    }

    @Override
    public void delete(Long id) {
        resourceRepository.delete(id);
    }

    @Override
    public void change(Long id, String type) {
        Resource resource = resourceRepository.findOne(id);
        switch (type){
            case "visible":
                resource.setVisible(!resource.isVisible());
                break;
            default:
                break;
        }
        resourceRepository.save(resource);
    }
}
