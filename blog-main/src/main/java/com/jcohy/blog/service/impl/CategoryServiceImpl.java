package com.jcohy.blog.service.impl;


import com.jcohy.blog.exception.ParamErrorException;
import com.jcohy.blog.model.Category;
import com.jcohy.blog.repository.BlogRepository;
import com.jcohy.blog.repository.CategoryRepository;
import com.jcohy.blog.service.CategoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;

/**
 * Copyright  : 2015-2033 Beijing Startimes Communication & Network Technology Co.Ltd
 * Created by jiac on 2017/10/24 17:20.
 * ClassName  : CategoryServiceImpl
 * Description  :
 */
@Service
@Transactional
public class CategoryServiceImpl implements CategoryService {

    private Logger logger = LoggerFactory.getLogger(CategoryServiceImpl.class);
    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private BlogRepository blogRepository;

    @Override
    public List<Category> findVisibleList() {
        List<Category> nodeList = null;

        List<Category> categoryList = categoryRepository.findByVisibleAndDeleted(true, false);
        if(categoryList == null){
            return null;
        }else{
            if(categoryList.size()>0){
                nodeList = new ArrayList<>();

                for (Category node1 : categoryList) {
                    boolean mark = false;
                    if(node1.getParentId() != null){
                        for (Category node2 : categoryList) {
                            //1.
                            //把node1加入到其父节点的子节点列表中
                            if(node1.getParentId().equals(node2.getId())){
                                mark = true;
                                if (node2.getChildren() == null) {
                                    node2.setChildren(new LinkedHashSet<>());
                                }
                                node2.getChildren().add(node1);
                                break;
                            }
                        }
                    }
                    if (!mark) {
                        nodeList.add(node1);
                    }
                }
            }
        }
        logger.info("category list = {}",nodeList);
        return nodeList;
    }

    @Override
    public List<Category> findChildren(Long id) {
        List<Category> categories = categoryRepository.findByParentId(id);
        return categories;
    }

    @Override
    public List<Category> findAll() {
        return categoryRepository.findAll();
    }

    @Override
    public Page<Category> findAll(Pageable Pageable) {
        return categoryRepository.findAll(Pageable);
    }

    @Override
    public Category findById(Long id) {
        return categoryRepository.findOne(id);
    }

    @Override
    public Category findByName(String name) {
        return categoryRepository.findByName(name);
    }

    @Override
    public void saveOrUpdate(Category category,Long userId) {


        if(category == null){
            throw new ParamErrorException("分类对象为空");
        }
        Category saveCategory = null;
        if(category.getId() == null){
            saveCategory = category;
            saveCategory.setCreatedBy(userId);
            saveCategory.setCreatedTime(new Date());
            saveCategory.setCount(0);
            saveCategory.setVisible(true);
        }else{
            saveCategory = findById(category.getId());
            saveCategory.setName(category.getName());
            saveCategory.setType(category.getType());
            saveCategory.setDeleted(false);
            saveCategory.setCreatedBy(userId);
        }
        saveCategory.setUpdatedBy(userId);
        saveCategory.setUpdatedTime(new Date());
        categoryRepository.save(saveCategory);
    }


    @Override
    public void delete(Long id) {

        categoryRepository.delete(id);
    }

    @Override
    public Category change(Long id, String type) {
        Category category = categoryRepository.findOne(id);
        switch (type){
            case "status":
                category.setVisible(!category.isVisible());
                break;
            default:
                break;
        }
        return categoryRepository.save(category);
    }
}
