package com.jcohy.blog.service.impl;


import com.jcohy.blog.common.Response;
import com.jcohy.blog.controller.admin.BlogSearchRequest;
import com.jcohy.blog.dto.BlogDto;
import com.jcohy.blog.enums.BlogStatus;
import com.jcohy.blog.model.Blog;
import com.jcohy.blog.model.Tag;
import com.jcohy.blog.model.User;
import com.jcohy.blog.repository.BlogRepository;
import com.jcohy.blog.security.JwtTokenUtil;
import com.jcohy.blog.service.*;
import com.jcohy.blog.utils.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Copyright  : 2017- www.jcohy.com
 * Created by jiac on 20:18 2017/12/20
 * Email: jia_chao23@126.com
 * ClassName: BlogServiceImpl
 * Description:
 **/
@Service
public class BlogServiceImpl extends BaseService implements BlogService {


    @Value("${blog.coverUrl}")
    private String coverUrl;
    @Autowired
    private BlogRepository blogRepository;

    @Autowired
    private TagService tagService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private UserService userService;

    @Override
    public List<BlogDto> findHotN(int n) {
        Pageable pageable = new PageRequest(0,n);
        List<Blog> blogs = blogRepository.findByPrivacyOrderByReadNumDesc(1, pageable).getContent();
        return Blog2BlogDto(blogs);
    }

    @Override
    public List<BlogDto> findFeaturedN(int n) {
        Pageable pageable = new PageRequest(0,n);
        List<Blog> blogs =  blogRepository.findByPrivacyOrderByShareNumDesc(1,pageable).getContent();
        return Blog2BlogDto(blogs);
    }

    @Override
    public List<BlogDto> findAll() {
        List<Blog> blogs = blogRepository.findAll();

        return Blog2BlogDto(blogs);
    }

    public List<BlogDto> Blog2BlogDto(List<Blog> blogs){
        List<BlogDto> blogDtos = new ArrayList<>();
        for(Blog blog:blogs){
            BlogDto blogDto = Blog.of(blog);
            blogDtos.add(blogDto);
        }
        return blogDtos;
    }
    @Override
    public List<BlogDto> findAllByPrivacy(Pageable pageable) {
        Page<Blog> blogs = blogRepository.findByPrivacy(1,pageable);
        return Blog2BlogDto(blogs.getContent());
    }

    @Override
    public List<BlogDto> findAllByPrivacy() {
        List<Blog> blogs = blogRepository.findByPrivacy(1);
        return Blog2BlogDto(blogs);
    }

    @Override
    public List<BlogDto> findAll(BlogSearchRequest blogSearchRequest) {
//        Page<Blog> blogs = blogRepository.findAll(Pageable);
        PageRequest pageRequest = buildPageRequest(
                blogSearchRequest.getPage(),
                blogSearchRequest.getSize(),
                blogSearchRequest.getSort(),
                blogSearchRequest.getOrder(), "createdTime");
        Page<Blog> blogs = blogRepository.findAll(pageRequest);
        return Blog2BlogDto(blogs.getContent());
    }

    @Override
    public Blog findById(Long id) {
        return blogRepository.findOne(id);
    }

    @Transactional
    @Override
    public Response saveOrUpdate(BlogDto blogDto) {
        Response response = new Response();
        String token = blogDto.getUsername();
        String name = jwtTokenUtil.getUsernameFromToken(token);
        Long userId = jwtTokenUtil.getUserIdFromToken(token);
        blogDto.setUsername(name);
        Blog blog = BlogDto2Blog(blogDto,userId);
        User user = userService.getUser(name, 0);
        blog.setUser(user);
        if(StringUtils.isBlank(blogDto.getCoverURL())){
            blog.setCoverURL(coverUrl);
        }
        if(blogDto.getId() == null){
            blog.setStatus(0);
            blog.setReadNum(0);
            blog.setShareNum(0);
            blog.setHeartNum(0);
            blog.setCommentNum(0);
            blog.setDeleted(false);
            blog.setCreatedBy(user.getId());
            blog.setCreatedTime(new Date());
            blog.setUpdatedBy(user.getId());
            blog.setUpdatedTime(new Date());
        }

        blogRepository.saveAndFlush(blog);
        response.setCode(BlogStatus.SUCCESS.getCode());
        response.setMessage(BlogStatus.SUCCESS.getMsg());
        return response;
    }

    @Override
    public void delete(Long id) {
        blogRepository.delete(id);
    }

    @Override
    public void change(Long id, String type) {
        Blog blog = blogRepository.findOne(id);
        switch (type){
            case "isTop":
                blog.setIsTop(blog.getIsTop() == 0 ? 1 : 0);
                break;
            case "isCommend":
                blog.setIsCommend(blog.getIsCommend() == 0 ? 1:0);
                break;
            case "privacy":
                blog.setPrivacy(blog.getPrivacy() == 0 ? 1:0);
                break;
            default:
                break;
        }
        blogRepository.save(blog);
    }

    public Blog BlogDto2Blog(BlogDto blogDto,Long userId){
        Blog blog = new Blog();
        BeanUtils.copyProperties(blogDto,blog);
        Set<Tag> tags = blog.getTags();
        if(blogDto.getTags()!= null){
            String stringTags = blogDto.getTags();
            String[] split = stringTags.split(",");
            for(int i=0; i<split.length;i++){
                if(!StringUtils.isBlank(split[i])){
                    Tag tag = tagService.findByName(split[i]);
                    if(tag == null){
                        tag = new Tag();
                        tag.setName(split[i]);
                        tag.setVisible(true);
                        tag.setCount(0);
                        tag = tagService.saveOrUpdate(tag, userId);
                    }
                    tags.add(tag);
                }
            }
        }
        blog.setCategory(categoryService.findByName(blogDto.getCategoryName()));
        return blog;
    }

}
