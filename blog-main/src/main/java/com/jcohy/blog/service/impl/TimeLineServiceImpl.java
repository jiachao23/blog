package com.jcohy.blog.service.impl;

import com.jcohy.blog.exception.ParamErrorException;
import com.jcohy.blog.model.TimeLine;
import com.jcohy.blog.repository.TimeLineRepository;
import com.jcohy.blog.service.TimeLineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Copyright  : 2015-2033 Beijing Startimes Communication & Network Technology Co.Ltd
 * Created by jiac on 2018/1/31 14:36.
 * ClassName  : TimeLineServiceImpl
 * Description  :
 */

@Service
public class TimeLineServiceImpl implements TimeLineService {

    @Autowired
    private TimeLineRepository timeLineRepository;
    @Override
    public List<TimeLine> findAll() {
        return timeLineRepository.findAll();
    }

    @Override
    public TimeLine findById(Long id) {
        return timeLineRepository.findOne(id);
    }

    @Override
    public TimeLine saveOrUpdate(TimeLine timeLine,Long userId) {
        if(timeLine == null){
            throw new ParamErrorException("对象为空");
        }
        TimeLine _timeLine = null;
        if(timeLine.getId() == null){
            _timeLine = timeLine;
            _timeLine.setCreatedTime(new Date());
            _timeLine.setDeleted(false);
            _timeLine.setCreatedBy(userId);
            _timeLine.setUpdatedBy(userId);
            _timeLine.setUpdatedTime(new Date());
        }else{
            _timeLine = findById(timeLine.getId());
            if(timeLine.getDisplayName() != null) _timeLine.setDisplayName(timeLine.getDisplayName());
            if(timeLine.getDisplayDate() != null) _timeLine.setDisplayDate(timeLine.getDisplayDate());
            _timeLine.setUpdatedTime(new Date());
        }

        return timeLineRepository.save(timeLine);
    }
}
