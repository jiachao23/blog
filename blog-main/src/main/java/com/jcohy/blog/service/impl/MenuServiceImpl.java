package com.jcohy.blog.service.impl;

import com.jcohy.blog.model.Menu;
import com.jcohy.blog.repository.MenuRepository;
import com.jcohy.blog.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;

/**
 * Copyright  : 2015-2033 Beijing Startimes Communication & Network Technology Co.Ltd
 * Created by jiac on 2018/1/31 09:55.
 * ClassName  : MenuServiceImpl
 * Description  :
 */

@Service
public class MenuServiceImpl implements MenuService {

    @Autowired
    private MenuRepository menuRepository;
//
//    @Override
//    public List<Menu> getMenuListTree(Long userId, Locale locale) {
//
//        List<Menu> menuList = menuRepository.getUserMenus(userId);
//        List<Menu> nodeList = TreeUtils.getTreeList(menuList, locale);
//        return nodeList;
//    }
//
//    @Override
//    public Page<Menu> getAllMenus(Pageable request) {
//        return menuRepository.getMenuByDeleted(false, request);
//    }
//
//    @Override
//    public List<Menu>  getAllMenus(Locale locale) {
//        List<Menu> menuList = menuRepository.getMenuByDeleted(false);
//        List<Menu> nodeList = TreeUtils.getTreeList(menuList, locale);
//        return nodeList;
//    }

    @Override
    public List<Menu> getMenuListTree(Long userId) {

        return null;
    }

    @Override
    public Page<Menu> getAllMenus(Pageable request) {
        return null;
    }

    @Override
    public List<Menu> findVisible() {
        List<Menu> nodeList = null;

        List<Menu> menuList = menuRepository.findAllByVisibleTrueAndTypeBeforeAndDeletedFalse(2);
        if(menuList == null){
            return null;
        }else{
            if(menuList.size()>0){
                nodeList = new ArrayList<>();
                for (Menu node1 : menuList) {
                    boolean mark = false;
                    //顶级节点
                    if(node1.getParentId() != null){
                        //如果不是顶级节点，先找他的父节点
                        for(Menu node2 : menuList){
                            //1.
                            //把node1加入到其父节点的子节点列表中
                            if (node1.getParentId().equals(node2.getId())) {
                                mark = true;
                                if (node2.getChildren() == null) {
                                    node2.setChildren(new LinkedHashSet<>());
                                }
//                                if(!Locale.CHINA.equals(locale)) {
//                                    if(StringUtils.isNoneBlank(node1.getEnName())) {
//                                        node1.setName(node1.getEnName());
//                                    }
//                                }
                                node2.getChildren().add(node1);
                                break;
                            }
                        }
                    }
                    if (!mark) {
//                        if(!Locale.CHINA.equals(locale)) {
//                            if(StringUtils.isNoneBlank(node1.getEnName())) {
//                                node1.setName(node1.getEnName());
//                            }
//                        }
                        nodeList.add(node1);
                    }
                }
            }
        }
        System.out.println(nodeList);
        return nodeList;
    }



    @Override
    public void saveOrUpdate(Menu menu,Long userId) {
        menu.setDeleted(false);
        menu.setCreatedBy(userId);
        menu.setCreatedTime(new Date());
        menu.setUpdatedBy(userId);
        menu.setUpdatedTime(new Date());
        menuRepository.save(menu);
    }

    @Override
    public void delete(Long id) {
        menuRepository.delete(id);
    }
}
