package com.jcohy.blog.controller.admin;

import com.jcohy.blog.common.Response;
import com.jcohy.blog.config.JwtHelper;
import com.jcohy.blog.controller.BaseController;
import com.jcohy.blog.enums.CommonStatus;
import com.jcohy.blog.model.Category;
import com.jcohy.blog.service.CategoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Copyright  : 2015-2033 Beijing Startimes Communication & Network Technology Co.Ltd
 * Created by jiac on 2017/12/21 10:55.
 * ClassName  : AdminCategoryController
 * Description  :
 */

@Controller
@RequestMapping("/admin/category")
public class AdminCategoryController extends BaseController {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private JwtHelper jwtHelper;

    @GetMapping("/list")
    @ResponseBody
    public Response<List<Category>> all(ModelMap map){
        Response<List<Category>> listResponse = new Response<>();

        List<Category> categories = categoryService.findAll();
        listResponse.setData(categories);
        listResponse.setCode(CommonStatus.SUCCESS.getCode());
        listResponse.setMessage(CommonStatus.SUCCESS.getMsg());
        return listResponse;
    }

    @GetMapping("/form")
    public String form(@RequestParam(required = false) Long id, ModelMap map){
        if(id != null){
            Category category = categoryService.findById(id);
            map.put("category",category);
        }
        return "admin/category/form";
    }

    @PostMapping("/save")
    @ResponseBody
    public Response save(Category category){
        Response listResponse = new Response<>();
        try {

            categoryService.saveOrUpdate(category,jwtHelper.getUserId());
        } catch (Exception e) {
            listResponse.setCode(CommonStatus.OTHER.getCode());
            listResponse.setMessage(CommonStatus.OTHER.getMsg());
            return listResponse;
        }
        listResponse.setCode(CommonStatus.SUCCESS.getCode());
        listResponse.setMessage(CommonStatus.SUCCESS.getMsg());
        return listResponse;
    }

    @DeleteMapping("{id}/del")
    @ResponseBody
    public Response del(@PathVariable("id") Long id){

        Response listResponse = new Response<>();

        try {
            categoryService.delete(id);
        } catch (Exception e) {
            listResponse.setCode(CommonStatus.OTHER.getCode());
            listResponse.setMessage(CommonStatus.OTHER.getMsg());
            return listResponse;
        }
        listResponse.setCode(CommonStatus.SUCCESS.getCode());
        listResponse.setMessage(CommonStatus.SUCCESS.getMsg());
        return listResponse;
    }


    @GetMapping("/{id}/change")
    @ResponseBody
    public Response change(@PathVariable("id") Long id, String type){

        Response listResponse = new Response<>();
        try {
            categoryService.change(id,type);

        } catch (Exception e) {
            listResponse.setCode(CommonStatus.OTHER.getCode());
            listResponse.setMessage(CommonStatus.OTHER.getMsg());
            return listResponse;
        }
        listResponse.setCode(CommonStatus.SUCCESS.getCode());
        listResponse.setMessage(CommonStatus.SUCCESS.getMsg());
        return listResponse;
    }

    @GetMapping("/{id}")
    @ResponseBody
    public Response<List<Category>> getCategoryByTypeId(@PathVariable(name = "id",required = true) Long id){
        Response<List<Category>> listResponse = new Response<>();
        List<Category> categories = categoryService.findChildren(id);
        listResponse.setCode(CommonStatus.SUCCESS.getCode());
        listResponse.setMessage(CommonStatus.SUCCESS.getMsg());
        listResponse.setData(categories);
        return listResponse;
    }
}
