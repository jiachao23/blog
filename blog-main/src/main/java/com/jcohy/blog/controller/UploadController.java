package com.jcohy.blog.controller;

import com.jcohy.blog.common.Response;
import com.jcohy.blog.enums.CommonStatus;
import com.jcohy.blog.model.Resource;
import com.jcohy.blog.service.ResourceService;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

/**
 * Copyright  : 2015-2033 Beijing Startimes Communication & Network Technology Co.Ltd
 * Created by jiac on 2017/12/27 16:04.
 * ClassName  : UploadController
 * Description  :
 */
@Controller
public class UploadController {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Value("${file.uploadUrl}")
    private String uploadUrl;

    @Value("${file.download}")
    private String downloadUrl;

    @Autowired
    private ResourceService resourceService;

    @PostMapping("/upload/{type}")
    @ResponseBody
    public Response<Resource> upload(@RequestParam("file") MultipartFile file, @PathVariable String type,
                                     HttpServletRequest request) throws FileNotFoundException {

        Response<Resource> response = new Response<>();
        Resource resource = new Resource();
        //上传到服务器的地址
        File upload = null;
        //上传到服务器的文件名
        String fileName = null;

        //获取跟目录
        if(StringUtils.isEmpty(type)){
            response.setCode(CommonStatus.PARAM_ERROE.getCode());
            response.setMessage(CommonStatus.PARAM_ERROE.getMsg());
            return response;
        }
        //D:\jcohy\resource
        String url = uploadUrl+ type;
        upload = new File(url);

        //如果上传文件夹不存在，创建文件夹
        if(!upload.exists()){
            upload.mkdirs();
            logger.info("upload url:"+upload.getAbsolutePath());
        }

        //获取当前目录下的所有文件
        File[] files = upload.listFiles();
        //截取后缀，拼接新的文件名
        //1.jpg
        fileName = String.valueOf(files.length+1)+file.getOriginalFilename().substring(file.getOriginalFilename().indexOf("."),file.getOriginalFilename().length());

        try {
            FileUtils.copyInputStreamToFile(file.getInputStream(),new File(upload+File.separator+fileName));
            resource.setUploadUrl(upload+File.separator+fileName);
            resource.setDownloadUrl(downloadUrl+type+"/"+fileName);
            resourceService.saveOrUpdate(resource, 1L);

        } catch (IOException e) {
            response.setCode(CommonStatus.OTHER.getCode());
            response.setMessage(CommonStatus.OTHER.getMsg());
            return response;
        }
        response.setCode(CommonStatus.SUCCESS.getCode());
        response.setMessage(CommonStatus.SUCCESS.getMsg());
        response.setData(resource);
        return response;
    }

    @GetMapping("/download/{type}/{name}")
    public void download(@PathVariable("name") String name,
                         @PathVariable("type") String type,
                         HttpServletResponse response) throws IOException {


        File file = new File(uploadUrl+type,name);
        if(file.exists()) {
            // 设置强制下载不打开
            response.setContentType("application/force-download");
            // 设置文件名
            response.addHeader("Content-Disposition", "attachment;fileName=" + name);

            FileInputStream inputStream = new FileInputStream(file);
            byte[] data = new byte[(int)file.length()];
            int length = inputStream.read(data);
            inputStream.close();

            OutputStream stream = response.getOutputStream();
            stream.write(data);
            stream.flush();
            stream.close();
        }
    }
}
