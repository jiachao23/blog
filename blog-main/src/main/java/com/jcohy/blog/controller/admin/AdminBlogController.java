package com.jcohy.blog.controller.admin;


import com.jcohy.blog.annotation.ConsoleLog;
import com.jcohy.blog.common.Response;
import com.jcohy.blog.config.JwtHelper;
import com.jcohy.blog.controller.BaseController;
import com.jcohy.blog.dto.BlogDto;
import com.jcohy.blog.enums.BlogStatus;
import com.jcohy.blog.enums.CommonStatus;
import com.jcohy.blog.model.Blog;
import com.jcohy.blog.model.Category;
import com.jcohy.blog.model.Tag;
import com.jcohy.blog.service.BlogService;
import com.jcohy.blog.service.CategoryService;
import com.jcohy.blog.service.TagService;
import com.jcohy.blog.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Copyright  : 2017- www.jcohy.com
 * Created by jiac on 20:17 2017/12/20
 * Email: jia_chao23@126.com
 * ClassName: AdminBlogController
 * Description:
 **/
@Controller
@RequestMapping("/admin/blog")
public class AdminBlogController extends BaseController {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private BlogService blogService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private TagService tagService;


    @GetMapping("/list")
    @ResponseBody
    public Response<List<BlogDto>> all(BlogSearchRequest blogSearchRequest){
        Response<List<BlogDto>> blogDtoResponse = new Response<>();
//        List<BlogDto> blogDtos = blogService.findAll();
        List<BlogDto> blogDtos = blogService.findAll(blogSearchRequest);
        blogDtoResponse.setData(blogDtos);
        blogDtoResponse.setCode(CommonStatus.SUCCESS.getCode());
        blogDtoResponse.setMessage(CommonStatus.SUCCESS.getMsg());
        return blogDtoResponse;
    }

    @ConsoleLog(module = "保存博客",description = "保存博客")
    @PostMapping("/save")
    @ResponseBody
    public Response<BlogDto> save(BlogDto blog){
        Response response = new Response();
        String username = blog.getUsername();
        if(StringUtils.isBlank(blog.getTags())){
            response.setCode(BlogStatus.BLOG_TAG_NOT_NULL.getCode());
            response.setMessage(BlogStatus.BLOG_TAG_NOT_NULL.getMsg());
            return response;
        }
        if(blog.getType() == null){
            response.setCode(BlogStatus.BLOG_TYPE_NOT_NULL.getCode());
            response.setMessage(BlogStatus.BLOG_TYPE_NOT_NULL.getMsg());
        }

        if(username == null){
            response.setCode(BlogStatus.USER_NOT_EXIST.getCode());
            response.setMessage(BlogStatus.USER_NOT_EXIST.getMsg());
            return response;
        }
        response = blogService.saveOrUpdate(blog);
        return response;

    }

    @DeleteMapping("/{id}/del")
    @ResponseBody
    public Response<BlogDto> delete(@PathVariable("id")Long id){

        Response<BlogDto> blogDtoResponse = new Response<>();

        try {
            blogService.delete(id);
        } catch (Exception e) {
            blogDtoResponse.setCode(CommonStatus.OTHER.getCode());
            blogDtoResponse.setMessage(CommonStatus.OTHER.getMsg());
            return blogDtoResponse;
        }
        blogDtoResponse.setCode(CommonStatus.SUCCESS.getCode());
        blogDtoResponse.setMessage(CommonStatus.SUCCESS.getMsg());
        return blogDtoResponse;
    }

    @GetMapping("/form")
    public String form(@RequestParam(required = false) Long id, ModelMap map){
        List<Category> categories = categoryService.findVisibleList();
        List<Tag> tags = tagService.findAll();
        map.put("categories",categories);
        map.put("tags",tags);
        if(id != null){
            Blog blog = blogService.findById(id);
            map.put("blog",blog);
        }
        return "admin/blog/form";
    }


    @GetMapping("{id}/change")
    @ResponseBody
    public Response<BlogDto> change(@PathVariable("id") Long id, String type){
        Response<BlogDto> blogDtoResponse = new Response<>();
        try {
            blogService.change(id,type);

        } catch (Exception e) {
            blogDtoResponse.setCode(CommonStatus.OTHER.getCode());
            blogDtoResponse.setMessage(CommonStatus.OTHER.getMsg());
            return blogDtoResponse;
        }
        blogDtoResponse.setCode(CommonStatus.SUCCESS.getCode());
        blogDtoResponse.setMessage(CommonStatus.SUCCESS.getMsg());
        return blogDtoResponse;
    }

}
