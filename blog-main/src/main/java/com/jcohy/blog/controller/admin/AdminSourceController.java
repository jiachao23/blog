package com.jcohy.blog.controller.admin;

import com.jcohy.blog.common.Response;
import com.jcohy.blog.config.JwtHelper;
import com.jcohy.blog.enums.CommonStatus;
import com.jcohy.blog.model.Category;
import com.jcohy.blog.model.Resource;
import com.jcohy.blog.service.CategoryService;
import com.jcohy.blog.service.ResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by jiac on 2018/2/6 14:55.
 * ClassName  : AdminSourceController
 * Description  :
 */

@Controller
@RequestMapping("/admin/resource")
public class AdminSourceController {

    @Autowired
    private ResourceService resourceService;

    @Autowired
    private CategoryService categoryService;
    @Autowired
    private JwtHelper jwtHelper;

    @GetMapping("/list")
    @ResponseBody
    public Response<List<Resource>> all(ModelMap map){
        Response<List<Resource>> listResponse = new Response<>();
        
        List<Resource> resources = resourceService.findVisible();

        listResponse.setData(resources);
        listResponse.setCode(CommonStatus.SUCCESS.getCode());
        listResponse.setMessage(CommonStatus.SUCCESS.getMsg());
        return listResponse;
    }
    @GetMapping("/form")
    public String form(@RequestParam(required = false) Long id, ModelMap map){
        List<Category> categories = categoryService.findVisibleList();
        map.put("categories",categories);
        if(id != null){
            Resource resource = resourceService.findById(id);
            map.put("resource",resource);
        }
        return "admin/resource/form";
    }

    @PostMapping("/save")
    @ResponseBody
    public Response save(Resource resource){
        Response listResponse = new Response<>();
        try {
            if(!resource.isVisible()){
                resource.setVisible(true);
            }
            resourceService.saveOrUpdate(resource,jwtHelper.getUserId());
        } catch (Exception e) {
            listResponse.setCode(CommonStatus.OTHER.getCode());
            listResponse.setMessage(CommonStatus.OTHER.getMsg());
            return listResponse;
        }
        listResponse.setCode(CommonStatus.SUCCESS.getCode());
        listResponse.setMessage(CommonStatus.SUCCESS.getMsg());
        return listResponse;
    }

    @DeleteMapping("{id}/del")
    @ResponseBody
    public Response del(@PathVariable("id") Long id){
        Response listResponse = new Response<>();
        try {
            resourceService.delete(id);
        } catch (Exception e) {
            listResponse.setCode(CommonStatus.OTHER.getCode());
            listResponse.setMessage(CommonStatus.OTHER.getMsg());
            return listResponse;
        }
        listResponse.setCode(CommonStatus.SUCCESS.getCode());
        listResponse.setMessage(CommonStatus.SUCCESS.getMsg());
        return listResponse;
    }
    @GetMapping("{id}/change")
    @ResponseBody
    public Response change(@PathVariable("id") Long id, String type){
        Response listResponse = new Response<>();
        try {
            resourceService.change(id,type);

        } catch (Exception e) {
            listResponse.setCode(CommonStatus.OTHER.getCode());
            listResponse.setMessage(CommonStatus.OTHER.getMsg());
            return listResponse;
        }
        listResponse.setCode(CommonStatus.SUCCESS.getCode());
        listResponse.setMessage(CommonStatus.SUCCESS.getMsg());
        return listResponse;
    }
}
