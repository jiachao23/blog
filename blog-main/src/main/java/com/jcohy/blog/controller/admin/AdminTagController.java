package com.jcohy.blog.controller.admin;

import com.jcohy.blog.common.Response;
import com.jcohy.blog.config.JwtHelper;
import com.jcohy.blog.enums.CommonStatus;
import com.jcohy.blog.model.Tag;
import com.jcohy.blog.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Copyright  : 2015-2033 Beijing Startimes Communication & Network Technology Co.Ltd
 * Created by jiac on 2017/12/26 09:35.
 * ClassName  : AdminTagController
 * Description  :
 */
@Controller
@RequestMapping("/admin/tag")
public class AdminTagController {

    @Autowired
    private TagService tagService;

    @Autowired
    private JwtHelper jwtHelper;

    @GetMapping("/list")
    @ResponseBody
    public Response<List<Tag>> all(ModelMap map){
        Response<List<Tag>> listResponse = new Response<>();
        List<Tag> tags = tagService.findAll();
        listResponse.setData(tags);
        listResponse.setCode(CommonStatus.SUCCESS.getCode());
        listResponse.setMessage(CommonStatus.SUCCESS.getMsg());
        return listResponse;
    }

    @GetMapping("/form")
    public String form(@RequestParam(required = false) Long id, ModelMap map){

        if(id != null){
            Tag tag = tagService.findById(id);
            map.put("tag",tag);
        }
        return "admin/tag/form";
    }

    @PostMapping("/save")
    @ResponseBody
    public Response save(Tag tag){
        Response listResponse = new Response<>();
        try {
            tagService.saveOrUpdate(tag,jwtHelper.getUserId());
        } catch (Exception e) {
            listResponse.setCode(CommonStatus.OTHER.getCode());
            listResponse.setMessage(CommonStatus.OTHER.getMsg());
            return listResponse;
        }
        listResponse.setCode(CommonStatus.SUCCESS.getCode());
        listResponse.setMessage(CommonStatus.SUCCESS.getMsg());
        return listResponse;
    }

    @DeleteMapping("{id}/del")
    @ResponseBody
    public Response del(@PathVariable("id") Long id){
        Response listResponse = new Response<>();
        try {
            tagService.delete(id);
        } catch (Exception e) {
            listResponse.setCode(CommonStatus.OTHER.getCode());
            listResponse.setMessage(CommonStatus.OTHER.getMsg());
            return listResponse;
        }
        listResponse.setCode(CommonStatus.SUCCESS.getCode());
        listResponse.setMessage(CommonStatus.SUCCESS.getMsg());
        return listResponse;
    }

    @GetMapping("/{id}/change")
    @ResponseBody
    public Response change(@PathVariable("id") Long id, String type){
        Response listResponse = new Response<>();
        try {
            tagService.change(id,type);

        } catch (Exception e) {
            listResponse.setCode(CommonStatus.OTHER.getCode());
            listResponse.setMessage(CommonStatus.OTHER.getMsg());
            return listResponse;
        }
        listResponse.setCode(CommonStatus.SUCCESS.getCode());
        listResponse.setMessage(CommonStatus.SUCCESS.getMsg());
        return listResponse;
    }
}
