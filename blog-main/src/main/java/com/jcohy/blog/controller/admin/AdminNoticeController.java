package com.jcohy.blog.controller.admin;

import com.jcohy.blog.common.Response;
import com.jcohy.blog.config.JwtHelper;
import com.jcohy.blog.enums.CommonStatus;
import com.jcohy.blog.model.Notice;
import com.jcohy.blog.service.NoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Copyright  : 2015-2033 Beijing Startimes Communication & Network Technology Co.Ltd
 * Created by jiac on 2018/1/31 15:23.
 * ClassName  : AdminNoticeController
 * Description  :
 */
@Controller
@RequestMapping("/admin/notice")
public class AdminNoticeController {

    @Autowired
    private NoticeService noticeService;

    @Autowired
    private JwtHelper jwtHelper;

    @GetMapping("/list")
    @ResponseBody
    public Response<List<Notice>> all(ModelMap map){
        Response<List<Notice>> listResponse = new Response<>();
        List<Notice> notices = noticeService.findAll();
        listResponse.setData(notices);
        listResponse.setCode(CommonStatus.SUCCESS.getCode());
        listResponse.setMessage(CommonStatus.SUCCESS.getMsg());
        return listResponse;
    }

    @GetMapping("/form")
    public String form(@RequestParam(required = false) Long id, ModelMap map){

        if(id != null){
            Notice notice = noticeService.findById(id);
            map.put("notice",notice);
        }
        return "admin/notice/form";
    }

    @PostMapping("/save")
    @ResponseBody
    public Response save(Notice notice){
        Response listResponse = new Response<>();
        try {
            if(!notice.isVisible()){
                notice.setVisible(true);
            }
            noticeService.saveOrUpdate(notice,jwtHelper.getUserId());
        } catch (Exception e) {
            listResponse.setCode(CommonStatus.OTHER.getCode());
            listResponse.setMessage(CommonStatus.OTHER.getMsg());
            return listResponse;
        }
        listResponse.setCode(CommonStatus.SUCCESS.getCode());
        listResponse.setMessage(CommonStatus.SUCCESS.getMsg());
        return listResponse;
    }

    @DeleteMapping("{id}/del")
    @ResponseBody
    public Response del(@PathVariable("id") Long id){
        Response listResponse = new Response<>();
        try {
            noticeService.delete(id);
        } catch (Exception e) {
            listResponse.setCode(CommonStatus.OTHER.getCode());
            listResponse.setMessage(CommonStatus.OTHER.getMsg());
            return listResponse;
        }
        listResponse.setCode(CommonStatus.SUCCESS.getCode());
        listResponse.setMessage(CommonStatus.SUCCESS.getMsg());
        return listResponse;
    }
    @GetMapping("{id}/change")
    @ResponseBody
    public Response change(@PathVariable("id") Long id, String type){
        Response listResponse = new Response<>();
        try {
            noticeService.change(id,type);

        } catch (Exception e) {
            listResponse.setCode(CommonStatus.OTHER.getCode());
            listResponse.setMessage(CommonStatus.OTHER.getMsg());
            return listResponse;
        }
        listResponse.setCode(CommonStatus.SUCCESS.getCode());
        listResponse.setMessage(CommonStatus.SUCCESS.getMsg());
        return listResponse;
    }
}
