package com.jcohy.blog.controller.front;

import com.jcohy.blog.common.Response;
import com.jcohy.blog.controller.admin.BlogSearchRequest;
import com.jcohy.blog.dto.BlogDto;
import com.jcohy.blog.model.Blog;
import com.jcohy.blog.service.BlogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * Copyright  : 2015-2033 Beijing Startimes Communication & Network Technology Co.Ltd
 * Created by jiac on 2018/1/31 11:57.
 * ClassName  : BlogController
 * Description  :
 */
@Controller
@RequestMapping("/article")
public class BlogController {

    @Autowired
    private BlogService blogService;

    @RequestMapping("/index")
    public String index(BlogSearchRequest blogSearchRequest){
//        PageRequest pageRequest = new PageRequest(0, 5);
//        Response<List<BlogDto>> response = new Response();
//        List<BlogDto> blogDtos = blogService.findAll(blogSearchRequest);
//        response.setData(blogDtos);
//        response.setCode(200);
//        response.setTotal(Long.valueOf(blogDtos.size()));
//        response.setStart(0);
//        System.out.println(page.getTotalElements());
//        map.put("page", page);
        return "front/article/index";
    }

    @RequestMapping("/view/{id}")
    public String view(@PathVariable("id") Long id, ModelMap map){
        System.out.println(id);
        Blog blog = blogService.findById(id);
        map.put("blog",blog);
        return "front/article/detail";
    }
}
