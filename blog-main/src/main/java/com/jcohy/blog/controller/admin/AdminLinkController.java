package com.jcohy.blog.controller.admin;

import com.jcohy.blog.common.Response;
import com.jcohy.blog.config.JwtHelper;
import com.jcohy.blog.enums.CommonStatus;
import com.jcohy.blog.model.Link;
import com.jcohy.blog.service.LinkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Copyright  : 2015-2033 Beijing Startimes Communication & Network Technology Co.Ltd
 * Created by jiac on 2017/12/26 13:44.
 * ClassName  : AdminLinkController
 * Description  :
 */
@Controller
@RequestMapping("/admin/link")
public class AdminLinkController {

    @Autowired
    private LinkService linkService;

    @Autowired
    private JwtHelper jwtHelper;

    @GetMapping("/list")
    @ResponseBody
    public Response<List<Link>> all(ModelMap map){
        Response<List<Link>> listResponse = new Response<>();
        List<Link> linkList = linkService.findAll();
        listResponse.setData(linkList);
        listResponse.setCode(CommonStatus.SUCCESS.getCode());
        listResponse.setMessage(CommonStatus.SUCCESS.getMsg());
        return listResponse;
    }

    @GetMapping("/form")
    public String form(@RequestParam(required = false) Long id, ModelMap map){

        if(id != null){
            Link link = linkService.findById(id);
            map.put("link",link);
        }
        return "admin/link/form";
    }

    @PostMapping("/save")
    @ResponseBody
    public Response<Link> save(Link link){
        Response<Link> listResponse = new Response<>();
        try {
            linkService.saveOrUpdate(link,jwtHelper.getUserId());
        } catch (Exception e) {
            listResponse.setCode(CommonStatus.OTHER.getCode());
            listResponse.setMessage(CommonStatus.OTHER.getMsg());
            return listResponse;
        }
        listResponse.setCode(CommonStatus.SUCCESS.getCode());
        listResponse.setMessage(CommonStatus.SUCCESS.getMsg());
        return listResponse;
    }

    @DeleteMapping("{id}/del")
    @ResponseBody
    public Response<Link>  del(@PathVariable("id") Long id){

        Response<Link> listResponse = new Response<>();

        try {
            linkService.delete(id);
        } catch (Exception e) {
            listResponse.setCode(CommonStatus.OTHER.getCode());
            listResponse.setMessage(CommonStatus.OTHER.getMsg());
            return listResponse;
        }
        listResponse.setCode(CommonStatus.SUCCESS.getCode());
        listResponse.setMessage(CommonStatus.SUCCESS.getMsg());
        return listResponse;
    }

    @GetMapping("/{id}/change")
    @ResponseBody
    public Response<Link> change(@PathVariable("id") Long id, String type){
        Response<Link> listResponse = new Response<>();

        try {
            linkService.change(id,type);

        } catch (Exception e) {
            listResponse.setCode(CommonStatus.OTHER.getCode());
            listResponse.setMessage(CommonStatus.OTHER.getMsg());
            return listResponse;
        }
        listResponse.setCode(CommonStatus.SUCCESS.getCode());
        listResponse.setMessage(CommonStatus.SUCCESS.getMsg());
        return listResponse;
    }
}
