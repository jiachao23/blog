package com.jcohy.blog.controller.admin;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * Created by jiac on 2018/10/8.
 * ClassName  : com.jcohy.blog.controller.admin
 * Description  :
 */
@ApiModel(value = "博客查询请求")
public class BlogSearchRequest implements Serializable{

    @ApiModelProperty(value = "博客名")
    private String name;

    @ApiModelProperty(value = "博客标签")
    private String tags;

    @ApiModelProperty(value = "博客分类")
    private String category;

    @ApiModelProperty(value = "博客作者")
    private String author;

    @ApiModelProperty(value = "第几页: 从1开始, 默认为第1页")
    private Integer page;

    @ApiModelProperty(value = "一页返回记录条数: 默认返回10条")
    private Integer size;

    @ApiModelProperty(value = "排序字段: 默认按创建时间倒叙")
    private String sort;

    @ApiModelProperty(value = "排序方式:asc or desc")
    private String order;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BlogSearchRequest that = (BlogSearchRequest) o;

        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (tags != null ? !tags.equals(that.tags) : that.tags != null) return false;
        if (category != null ? !category.equals(that.category) : that.category != null) return false;
        if (author != null ? !author.equals(that.author) : that.author != null) return false;
        if (page != null ? !page.equals(that.page) : that.page != null) return false;
        if (size != null ? !size.equals(that.size) : that.size != null) return false;
        if (sort != null ? !sort.equals(that.sort) : that.sort != null) return false;
        return order != null ? order.equals(that.order) : that.order == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (tags != null ? tags.hashCode() : 0);
        result = 31 * result + (category != null ? category.hashCode() : 0);
        result = 31 * result + (author != null ? author.hashCode() : 0);
        result = 31 * result + (page != null ? page.hashCode() : 0);
        result = 31 * result + (size != null ? size.hashCode() : 0);
        result = 31 * result + (sort != null ? sort.hashCode() : 0);
        result = 31 * result + (order != null ? order.hashCode() : 0);
        return result;
    }
}
