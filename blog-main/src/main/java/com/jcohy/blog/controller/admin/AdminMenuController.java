package com.jcohy.blog.controller.admin;

import com.jcohy.blog.controller.BaseController;
import com.jcohy.blog.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by jiac on 2018/9/6.
 * ClassName  : com.jcohy.controller.admin
 * Description  :
 */
public class AdminMenuController extends BaseController {

    @Autowired
    private MenuService menuService;

//    /**
//     * 用户菜单列表
//     */
//    @RequestMapping("/share/menu/user")
//    public ResponseEntity<List> userMenu(HttpServletRequest request) {
//
//        Locale locale = LocaleContextHolder.getLocale();
//
//        List<Menu> menuList = menuService.getAllMenus(locale);
//
//        //return Response.ok().put("menuList", menuList);
//        return ResponseEntity.ok(menuList);
//    }
//
//    @RequestMapping("/share/menu/list")
//    public ResponseEntity<Page> listMenus(HttpServletRequest request) {
//        Pageable pageable = new PageRequest(1, 2);
//        Page<Menu> menuList = menuService.getAllMenus(pageable);
//        return ResponseEntity.ok(menuList);
//    }
}
