package com.jcohy.blog.controller.front;

import com.jcohy.blog.service.ResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Copyright  : 2015-2033 Beijing
 * Created by jiac on 2018/3/9 17:10.
 * ClassName  : ResourceController
 * Description  :
 */
@Controller
@RequestMapping("/resource")
public class ResourceController {


    @Autowired
    private ResourceService resourceService;

    @RequestMapping("/index")
    public String index(){
        return "front/resource/index";
    }
}
