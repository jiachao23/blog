package com.jcohy.blog.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Created by jiac on 2018/7/2.
 * ClassName  : com.jcohy.config
 * Description  :
 */
@Component
@ConfigurationProperties(prefix = "jwt")
public class JwtHelper {
    private Long userId;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
