﻿<#include "/front/layout/_titleNav.ftl">
<@html>
<div class="top">
    <section class="box">
        <ul class="texts">
            <p>越是错综复杂的问题。</p>
            <p>就越要根据简单的原理和朴素的思想进行判断和行动。</p>
            <p>我想这是拨开云雾见南山，直接洞悉事物本质和解决问题的最佳方法。</p>
            <p>                                         ——稻盛和夫 《活法》。</p>
        </ul>
        <div class="avatar"><a href="#"><span>Jcohy</span></a> </div>
    </section>
</div>
<!-- 主体 -->
<div class="blog-body">
    <!-- 这个一般才是真正的主体内容 -->
    <div class="blog-container">
        <blockquote class="layui-elem-quote sitemap layui-breadcrumb shadow">
            <a href="/" title="网站首页">网站首页</a>
            <a><cite>资源分享</cite></a>
        </blockquote>
        <div class="blog-main">
            <div class="child-nav shadow">
                <span class="child-nav-btn child-nav-btn-this">网站导航</span>
                <#--<span class="child-nav-btn" onclick="location.href='mixed_vip.html'">文档分享</span>-->
                <#--<span class="child-nav-btn" onclick="location.href='mixed_game.html'">案例分享</span>-->
                <span class="child-nav-btn">文档分享</span>
                <span class="child-nav-btn">案例分享</span>
            </div>
            <div class="mixed-main" id="pic"></div>
        </div>
        <div class="clear"></div>
    </div>
</div>


</@html>