
<div class="blog-module shadow">
    <div class="blog-module-title">热文排行</div>
    <ul class="fa-ul blog-module-ul">
	<@blogList type="readNum">
		<#list list as x>
            <li><i class="fa-li fa fa-hand-o-right"></i><a href="${ctx!}/article/view/${x.id}">${x.title}</a></li>
		</#list>
	</@blogList>
    </ul>
</div>

<div class="blog-module shadow">
    <div class="blog-module-title">分享排行</div>
    <ul class="fa-ul blog-module-ul">
	<@blogList type="shareNum">
		<#list list as x>
            <li><i class="fa-li fa fa-hand-o-right"></i><a href="${ctx!}/article/view/${x.id}">${x.title}</a>
            </li>
		</#list>
	</@blogList>
    </ul>
</div>

<div class="blog-module shadow">
    <div class="blog-module-title">标签分类</div>
    <ul class="blogroll">
	<@tagList>
		<#list list as x>
            <#if x.id%3 ==1>
                <li><a href="${x.url}" target="_blank"><span class="layui-badge layui-bg-green">${x.name}</span></a></li>
            <#elseif x.id%3==2>
                <li><a href="${x.url}" target="_blank"><span class="layui-badge layui-bg-orange">${x.name}</span></a></li>
            <#else >
                <li><a href="${x.url}" target="_blank"><span class="layui-badge layui-bg-blue">${x.name}</span></a></li>
            </#if >
		</#list>
	</@tagList>
    </ul>
</div>

<div class="blog-module shadow">
    <div class="blog-module-title">资源分享</div>
    <ul class="fa-ul blog-module-ul">
        <@resourceList>
            <#list list as x>
               <li><i class="fa-li fa fa-hand-o-right"></i><a href="${x.downloadUrl}" target="_blank">${x.name}</a></li>
            </#list>
        </@resourceList>
    </ul>
</div>

<div class="blog-module shadow">
    <div class="blog-module-title">时光轴</div>
    <dl class="footprint">
    <@timeLineList>
        <#list list as x>
            <dt>${x.displayDate}</dt>
            <dd>${x.displayName}</dd>
        </#list>
    </@timeLineList>
    </dl>
</div>

<div class="blog-module shadow">
    <div class="blog-module-title">友情链接</div>
    <ul class="blogroll">
		<@linkList>
			<#list list as x>
				<li><a href="${x.url}" target="_blank">${x.title}</a></li>
			</#list>
		</@linkList>
    </ul>
</div>