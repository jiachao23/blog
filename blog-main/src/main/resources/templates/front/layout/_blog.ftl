<div class="blog-module shadow" style="position: relative">
    <div class="blog-module-title" style="line-height: 34px;font-size: 16px">
        <h4>热文排行
            <a class="moreitem" href="${ctx!}/article/index">
                更多&nbsp;&nbsp;
            </a>
        </h4>

    </div>

    <div class="layui-tab">
        <ul class="layui-tab-title">
            <li class="layui-this">阅读排行</li>
            <li>分享排行</li>
        <#--<li>其他</li>-->
        </ul>
        <div class="layui-tab-content">
            <div class="layui-tab-item layui-show">
                <ul class="fa-ul blog-module-ul" style="margin-left: 0">
                    <@blogList type="readNum">
                        <#list list as x>
                            <div class="article shadow">
                                <div class="article-left">
                                    <img src="${ctx!}/${(x.coverURL)!}" alt="${(x.coverURL)!}" />
                                </div>
                                <div class="article-right">
                                    <div class="article-title">
                                        <a href="${ctx!}/article/view/${x.id}">${(x.title)!}</a>
                                    </div>
                                    <div class="article-abstract">
                                        ${(x.summary)!}
                                    </div>
                                </div>
                                <div class="clear"></div>
                                <div class="article-footer">
                                    <span><i class="fa fa-clock-o"></i>&nbsp;&nbsp;${(x.createDate)!}</span>
                                    <span class="article-author"><i class="fa fa-user"></i>&nbsp;&nbsp;${(x.author.nickName)!}</span>
                                    <#--<span><i class="fa fa-tag"></i>&nbsp;&nbsp;<a href="#">${(x.tags)!}</a></span>-->
                                    <span class="article-viewinfo"><i class="fa fa-eye"></i>&nbsp;${(x.readNum)!}</span>
                                    <span class="article-viewinfo"><i class="fa fa-star"></i>&nbsp;${(x.heartNum)!}</span>
                                    <span class="article-viewinfo"><i class="fa fa-share-square-o"></i>&nbsp;${(x.shareNum)!}</span>
                                </div>
                            </div>
                        </#list>
                    </@blogList>
                </ul>
            </div>
            <div class="layui-tab-item">
                <ul class="fa-ul blog-module-ul" style="margin-left: 0">
                    <@blogList type="shareNum">
                        <#list list as x>
                            <div class="article shadow">
                                <div class="article-left">
                                    <img src="${ctx!}/${(x.coverURL)!}" alt="${(x.coverURL)!}" />
                                </div>
                                <div class="article-right">
                                    <div class="article-title">
                                        <a href="${ctx!}/article/view/${x.id}">${(x.title)!}</a>
                                    </div>
                                    <div class="article-abstract">
                                        ${(x.summary)!}
                                    </div>
                                </div>
                                <div class="clear"></div>
                                <div class="article-footer">
                                    <span><i class="fa fa-clock-o"></i>&nbsp;&nbsp;${(x.createDate)!}</span>
                                    <span class="article-author"><i class="fa fa-user"></i>&nbsp;&nbsp;${(x.author.nickName)!}</span>
                                    <#--<span><i class="fa fa-tag"></i>&nbsp;&nbsp;<a href="#">${(x.tags)!}</a></span>-->
                                    <span class="article-viewinfo"><i class="fa fa-eye"></i>&nbsp;${(x.readNum)!}</span>
                                    <span class="article-viewinfo"><i class="fa fa-star"></i>&nbsp;${(x.heartNum)!}</span>
                                    <span class="article-viewinfo"><i class="fa fa-share-square-o"></i>&nbsp;${(x.shareNum)!}</span>
                                </div>
                            </div>
                        </#list>
                    </@blogList>
                </ul>
            </div>
        </div>
    </div>
</div>