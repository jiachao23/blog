
var TOKEN_KEY = "JcohyToken"

function getToken() {
  return localStorage.getItem(TOKEN_KEY);
}

function setToken(token) {
  localStorage.setItem(TOKEN_KEY,token)
}

function removeToken() {
  localStorage.removeItem(TOKEN_KEY)
}

function showTokenInformation() {
  var jwtToken = getJwtToken();
  var decodedToken = jwt_decode(jwtToken);
  console.log(decodedToken);
}

function doLogout() {
  removeJwtToken();
  parent.location.href = OP.CONFIG.localhostPath + '/login.html';
}

function createAuthorizationTokenHeader() {
  var token = getJwtToken();
  if (token) {
    return {"Authorization": token};
  } else {
    return {};
  }
}

function refreshToken() {
  $.ajax({
    async: false,
    url: OP.CONFIG.localhostPath + "/auth/refresh",
    type: "GET",
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    headers: createAuthorizationTokenHeader(),
    success: function (data, textStatus, jqXHR) {
      if(data) {
        if(data.code == 200) {
          setJwtToken(data.token);
        }
      }
    }
  });
}


function initPageElements(page) {
  var param={url: page};
  var result;
  $.axsPostForm(
    OP.CONFIG.localhostPath + "/share/permission/authorized",
    param,
    function (data) {
      if(data && data.code==200) {
        result = data.perms;
        //console.log(data.perms)
        for(var i = 0; i < data.perms.length; i++) {
          $('[op-permission="'+data.perms[i]+'"]').show();
        }
      }
    });
  return result;
}

/**
 * ajax封装
 * url 发送请求的地址
 * data 发送到服务器的数据，数组存储，如：{"date": new Date().getTime(), "state": 1}
 * async 默认值: true。默认设置下，所有请求均为异步请求。如果需要发送同步请求，请将此选项设置为 false。
 *       注意，同步请求将锁住浏览器，用户其它操作必须等待请求完成才可以执行。
 * type 请求方式("POST" 或 "GET")， 默认为 "GET"
 * dataType 预期服务器返回的数据类型，常用的如：xml、html、json、text
 * successfn 成功回调函数
 * errorfn 失败回调函数
 */
jQuery.axForm=function(url, data, async, type, successfn, errorfn) {
  async = (async==null || async=="" || typeof(async)=="undefined")? "true" : async;
  type = (type==null || type=="" || typeof(type)=="undefined")? "post" : type;
  dataType = (dataType==null || dataType=="" || typeof(dataType)=="undefined")? "json" : dataType;
  data = (data==null || data=="" || typeof(data)=="undefined")? {"date": new Date().getTime()} : data;
  $.ajax({
    type: type,
    async: async,
    data: data,
    url: url,
    dataType: dataType,
    contentType: "application/x-www-form-urlencoded",
    headers: createAuthorizationTokenHeader(),
    success: function(d){
      refreshToken();
      successfn(d);
    },
    error: function(e){
      errorfn(e);
    }
  });
};

/**
 * ajax封装
 * url 发送请求的地址
 * data 发送到服务器的数据，数组存储，如：{"date": new Date().getTime(), "state": 1}
 * successfn 成功回调函数
 */
jQuery.axsPost=function(url, data, successfn) {
  data = (data==null || data=="" || typeof(data)=="undefined")? {"date": new Date().getTime()} : data;
  $.ajax({
    type: "post",
    data: data,
    async: false,
    url: url,
    dataType: "json",
    contentType: "application/json; charset=utf-8",
    headers: createAuthorizationTokenHeader(),
    success: function(d){
      refreshToken();
      successfn(d);
    }
  });
};

/**
 * ajax封装
 * url 发送请求的地址
 * data 发送到服务器的数据，数组存储，如：{"date": new Date().getTime(), "state": 1}
 * successfn 成功回调函数
 */
jQuery.axsPostForm=function(url, data, successfn) {
  data = (data==null || data=="" || typeof(data)=="undefined")? {"date": new Date().getTime()} : data;
  $.ajax({
    type: "post",
    data: data,
    async: false,
    url: url,
    dataType: "json",
    contentType: "application/x-www-form-urlencoded",
    headers: createAuthorizationTokenHeader(),
    success: function(d){
      refreshToken();
      successfn(d);
    }
  });
};

/**
 * ajax封装
 * url 发送请求的地址
 * data 发送到服务器的数据，数组存储，如：{"date": new Date().getTime(), "state": 1}
 * successfn 成功回调函数
 */
jQuery.axsGet=function(url, data, successfn) {
  data = (data==null || data=="" || typeof(data)=="undefined")? {"date": new Date().getTime()} : data;
  $.ajax({
    type: "GET",
    data: data,
    async: false,
    url: url,
    dataType: "json",
    headers: createAuthorizationTokenHeader(),
    success: function(d){
      refreshToken();
      successfn(d);
    }
  });
};

/**
 * ajax封装
 * url 发送请求的地址
 * data 发送到服务器的数据，数组存储，如：{"date": new Date().getTime(), "state": 1}
 * successfn 成功回调函数
 */
jQuery.axs=function(url, data, successfn) {
  data = (data==null || data=="" || typeof(data)=="undefined")? {"date": new Date().getTime()} : data;
  $.ajax({
    type: "post",
    data: data,
    url: url,
    dataType: "json",
    headers: createAuthorizationTokenHeader(),
    success: function(d){
      refreshToken();
      successfn(d);
    }
  });
};

/**
 * ajax封装
 * url 发送请求的地址
 * data 发送到服务器的数据，数组存储，如：{"date": new Date().getTime(), "state": 1}
 * dataType 预期服务器返回的数据类型，常用的如：xml、html、json、text
 * successfn 成功回调函数
 * errorfn 失败回调函数
 */
jQuery.axse=function(url, data, successfn, errorfn) {
  data = (data==null || data=="" || typeof(data)=="undefined")? {"date": new Date().getTime()} : data;
  $.ajax({
    type: "post",
    data: data,
    url: url,
    dataType: "json",
    headers: createAuthorizationTokenHeader(),
    success: function(d){
      refreshToken();
      successfn(d);
    },
    error: function(e){
      errorfn(e);
    }
  });
};
