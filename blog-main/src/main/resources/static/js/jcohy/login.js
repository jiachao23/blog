
$(function () {
    // VARIABLES =============================================================
    var $notLoggedIn = $("#notLoggedIn");
    var $loggedIn = $("#loggedIn").hide();
    var $loggedInBody = $("#loggedInBody");
    var $response = $("#response");
    var $login = $("#login");
    var $userInfo = $("#userInfo").hide();

    // REGISTER EVENT LISTENERS =============================================================
    $("#loginForm").submit(function (event) {
        event.preventDefault();

        var $form = $(this);
        var formData = {
            username: $form.find('input[name="username"]').val(),
            password: $form.find('input[name="password"]').val()
        };
        if(!formData.username || formData.username =='') {
            vm.error = true;
            vm.errorMsg = "帐号不能为空";
            return false;
        }
        if(!formData.password || formData.password =='') {
            vm.error = true;
            vm.errorMsg = "密码不能为空";
            return false;
        }
        doLogin(formData);
    });
});

function doLogin(loginData) {
    var lang = Cookies.get('jcohy_op_lang')
    $.ajax({
        url: OP.CONFIG.localhostPath + "/auth?lang=" + lang,
        type: "POST",
        data: JSON.stringify(loginData),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, textStatus, jqXHR) {
            if(data) {
                if(data.code == 200) {
                    setJwtToken(data.token);
                    parent.location.href = JCOHY.CONFIG.localhostPath + '/index.html';
                } else {
                    vm.error = true;
                    vm.errorMsg = data.msg;
                }
            }
        }
    });
}