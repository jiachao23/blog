

var JCOHY = JCOHY ||{}
JCOHY.CONFIG = getRootPath()

var basePath = JCOHY.CONFIG.localhostPath;
function getRootPath() {
  //获取当前网址，"http://localhost:8081/login"
  var curWwwPath = window.document.location.href;
  ///pathName:"/login"
  var pathName = window.document.location.pathname;

  var pos = curWwwPath.indexOf(pathName);

  //获取主机地址，如： "http://localhost:8081"
  var localhostPath = curWwwPath.substring(0, pos);
  //获取带"/"的项目名，如：/console.没有则为空
  var projectName = pathName.substring(0, pathName.substr(1).indexOf('/') + 1);
  //console.log(projectName);"http://localhost:8081"
  var rootPath = localhostPath + projectName;
  return {"localhostPath": localhostPath, "rootPath": rootPath};
}

$(document).ajaxError(function (jqXHR, textStatus, errorThrown) {
  if (textStatus && textStatus.status) {
    if (textStatus.status == '401') {
      parent.location.href = JCOHY.CONFIG.localhostPath + '/login';
    } else if (textStatus.status == '403') {
      alert("没有权限执行该操作");
    }
  }
});

window.msg = function (msg) {
  parent.layer.msg(msg, {shade: 0.3})
};

//重写alert
window.alert = function (msg, callback) {
  parent.layer.alert(msg, function (index) {
    if(parent) {
      parent.layer.close(index);
    } else {
      layer.close(index);
    }
    if (typeof(callback) === "function") {
      callback("ok");
    }
  });
}

window.alert3 = function (msg, callback, ok, title) {
  parent.layer.alert(msg, {btn: [ok], title: title}, function (index) {
    if(parent) {
      parent.layer.close(index);
    } else {
      layer.close(index);
    }
    if (typeof(callback) === "function") {
      callback("ok");
    }
  });
}

//重写confirm式样框
window.confirm = function (msg, callback) {
  parent.layer.confirm(msg, {btn: ['确定', '取消']},
    function (index) {//确定事件

      if(parent) {
        parent.layer.close(index);
      } else {
        layer.close(index);
      }
      if (typeof(callback) === "function") {
        callback("ok");
      }
    });
}

//重写confirm式样框
window.confirm3 = function (msg, callback, ok, cancel, title) {
  if(ok == null || ok == '') {
    ok = '确定';
  }
  if(cancel == null || cancel == '') {
    cancel = '取消';
  }
  parent.layer.confirm(msg, {btn: [ok, cancel], title: title},
    function (index) {//确定事件

      if(parent) {
        parent.layer.close(index);
      } else {
        layer.close(index);
      }
      if (typeof(callback) === "function") {
        callback("ok");
      }
    });
}

window.confirm2 = function (msg, callback) {
  parent.layer.confirm(msg, {btn: ['确定', '取消']},
    function (index) {//确定事件
      if(parent) {
        parent.layer.close(index);
      } else {
        layer.close(index);
      }
      if (typeof(callback) === "function") {
        callback("ok");
      }
    },
    function (index) {
      if(parent) {
        parent.layer.close(index);
      } else {
        layer.close(index);
      }
      if (typeof(callback) === "function") {
        callback("no");
      }
    });
}

//递归删除空属性防止把null变成空值
function deleteEmptyProp(obj) {
  for (var a in obj) {
    if (typeof (obj[a]) == "object" && obj[a] != null) {
      deleteEmptyProp(obj[a]);
    }
    else {
      if (!obj[a]) {
        delete obj[a];
      }
    }
  }
  return obj;
}

function ajaxPost(url, params, callback) {
  var result = null;

  console.log("before remove empty prop:");
  console.log(params);
  if (params && typeof params == "object") {
    params = deleteEmptyProp(params);
  }
  console.log("after remove empty prop:");
  console.log(params);

  jQuery.ajax({
    type: 'POST',
    async: false,
    url: url,
    data: params,
    dataType: 'json',
    headers: createAuthorizationTokenHeader(),
    //contentType : 'application/json',
    success: function (data, status) {
      result = data;
      if (data && data.code && data.code == '101') {
        parent.modals.error("操作失败，请刷新重试，具体错误：" + data.message);
        return false;
      }
      if (callback) {
        callback.call(this, data, status);
      }
    },
    error: function (err, err1, err2) {
      console.log("ajaxPost发生异常，请仔细检查请求url是否正确，如下面错误信息中出现success，则表示csrftoken更新，请忽略");
      //console.log(err.responseText);
      if (err && err.readyState && err.readyState == '4') {
        var sessionstatus = err.getResponseHeader("session-status");
        if (sessionstatus == "timeout") {
          //如果超时就处理 ，指定要跳转的页面
          window.location.href = basePath + "/";
        }
        else if (err1 == "parsererror") {//csrf异常
          var responseBody = err.responseText;
          if (responseBody) {
            responseBody = "{'retData':" + responseBody;
            var resJson = eval('(' + responseBody + ')');
            jQuery("#csrftoken").val(resJson.csrf.CSRFToken);
            this.success(resJson.retData, 200);
          }
          return;
        } else {
          parent.modals.error({
            text: JSON.stringify(err) + '<br/>err1:' + JSON.stringify(err1) + '<br/>err2:' + JSON.stringify(err2),
            large: true
          });
          return;
        }
      }

      parent.modals.error({
        text: JSON.stringify(err) + '<br/>err1:' + JSON.stringify(err1) + '<br/>err2:' + JSON.stringify(err2),
        large: true
      });
    }
  });

  return result;
}

/**
 * 格式化日期
 */
function formatDate(date, format) {
  if (!date)return date;
  date = (typeof date == "number") ? new Date(date) : date;
  return date.Format(format);
}

Date.prototype.Format = function (fmt) {
  var o = {
    "m+": this.getMonth() + 1, // 月份
    "d+": this.getDate(), // 日
    "h+": this.getHours(), // 小时
    "i+": this.getMinutes(), // 分
    "s+": this.getSeconds(), // 秒
    "q+": Math.floor((this.getMonth() + 3) / 3), // 季度
    "S": this.getMilliseconds()
    // 毫秒
  };
  if (/(y+)/.test(fmt))
    fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
  for (var k in o)
    if (new RegExp("(" + k + ")").test(fmt))
      fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
  return fmt;
}

/**
 * 比较两个时间的大小 d1>d2 返回大于0
 * @param d1
 * @param d2
 * @returns {number}
 * @constructor
 */
function DateDiff(d1, d2) {
  var result = Date.parse(d1.replace(/-/g, "/")) - Date.parse(d2.replace(/-/g, "/"));
  return result;
}

/**
 * 字符串转日期
 * @returns {number}
 */
String.prototype.strToDate = function () {
  if (this && this != "") {
    return Date.parse(this.replace(/-/g, "/"));
  }
  else
    return "";
}
/**
 * 将map类型[name,value]的数据转化为对象类型
 */
function getObjectFromMap(aData) {
  var map = {};
  for (var i = 0; i < aData.length; i++) {
    var item = aData[i];
    if (!map[item.name]) {
      map[item.name] = item.value;
    }
  }
  return map;
}


/**
 * 获取下一个编码 000001，000001000006，6
 * 得到结果 000001000007
 */
function getNextCode(prefix, maxCode, length) {
  if (maxCode == null) {
    var str = "";
    for (var i = 0; i < length - 1; i++) {
      str += "0";
    }
    return prefix + str + 1;
  } else {
    var str = "";
    var sno = parseInt(maxCode.substring(prefix.length)) + 1;
    for (var i = 0; i < length - sno.toString().length; i++) {
      str += "0";
    }
    return prefix + str + sno;
  }

}

var HtmlUtil = {
  /*1.用浏览器内部转换器实现html转码*/
  htmlEncode: function (html) {
    //1.首先动态创建一个容器标签元素，如DIV
    var temp = document.createElement("div");
    //2.然后将要转换的字符串设置为这个元素的innerText(ie支持)或者textContent(火狐，google支持)
    (temp.textContent != undefined ) ? (temp.textContent = html) : (temp.innerText = html);
    //3.最后返回这个元素的innerHTML，即得到经过HTML编码转换的字符串了
    var output = temp.innerHTML;
    temp = null;
    return output;
  },
  /*2.用浏览器内部转换器实现html解码*/
  htmlDecode: function (text) {
    //1.首先动态创建一个容器标签元素，如DIV
    var temp = document.createElement("div");
    //2.然后将要转换的字符串设置为这个元素的innerHTML(ie，火狐，google都支持)
    temp.innerHTML = text;
    //3.最后返回这个元素的innerText(ie支持)或者textContent(火狐，google支持)，即得到经过HTML解码的字符串了。
    var output = temp.innerText || temp.textContent;
    temp = null;
    return output;
  }
};

String.prototype.startWith = function (s) {
  if (s == null || s == "" || this.length == 0 || s.length > this.length)
    return false;
  if (this.substr(0, s.length) == s)
    return true;
  else
    return false;
  return true;
}

String.prototype.replaceAll = function (s1, s2) {
  return this.replace(new RegExp(s1, "gm"), s2);
}

String.prototype.format = function () {
  if (arguments.length == 0) return this;
  for (var s = this, i = 0; i < arguments.length; i++)
    s = s.replace(new RegExp("\\{" + i + "\\}", "g"), arguments[i]);
  return s;
};


//})(jQuery)


//判断字符串中是否包含汉字,包含返回true，否则返回false
function checkChineseCharacter(s) {
  var reg = /.*[\u4E00-\u9FA5\uF900-\uFA2D]+.*$/;
  if (reg.test(s)) {
    return true;
  }
  return false;
}

/**
 * 校验新密码
 * 不能为空；
 * 新密码6-16位，必须包括数字、大写字母、小写字母、特殊字符中的至少两种；
 * @param passwd
 * @returns {String}
 */
function checkNewPassword(passwd) {
  var message = "";
  if (passwd == null || passwd == "") {
    message = getMsg("NOT_NULL", new Array("新密码"));
  } else if (checkChineseCharacter(trimAll(passwd))) {
    message = getMsg("PWD_NEWPWD_RULE", new Array());
  } else {
    message = checkStrong(trimAll(passwd));
  }
  return message;
}

/**
 * 判断字符类别
 * @param charIn
 * @returns {Number}
 */
function charMode(charIn) {
  if (charIn >= 48 && charIn <= 57) 	//数字
    return 1;
  if (charIn >= 65 && charIn <= 90) 	//大写字母
    return 2;
  if (charIn >= 97 && charIn <= 122) 	//小写字母
    return 4;
  else
    return 8; 		//特殊字符
}

/**
 * 计算出密码当中一共有多少种字符
 * @param num
 * @returns
 */
function bitTotal(num) {
  modes = 0;
  for (i = 0; i < 4; i++) {
    if (num & 1) {
      modes++;
    }
    num >>>= 1;
  }
  return modes;
}


/**
 * 返回密码的强度级别
 * @param passwd
 * @returns
 */
function checkStrong(passwd) {
  var message = "";
  if (trimAll(passwd).length < 8 || trimAll(passwd).length > 32) { // 密码太短或太长
    message = getMsg("PWD_NEWPWD_RULE", new Array());
  }
  modes = 0;
  for (i = 0; i < passwd.length; i++) {
    //测试每一个字符的类别并统计一共有多少种
    modes |= charMode(passwd.charCodeAt(i));
  }
  num = 0;
  num = bitTotal(modes);
  if (num < 2) {
    message = getMsg("PWD_NEWPWD_RULE", new Array());
  }
  return message;
}

//删除左边的半角空格
function ltrim(str) {
  return str.replace(/(^\s*)/g, "");
}

//删除右边的半角空格
function rtrim(str) {
  return str.replace(/(\s*$)/g, "");
}

//删除左右两端的全角空格
function trimAll(str) {
  return str.replace(/(^[\s\u3000]*)|([\s\u3000]*$)/g, "");
}

//删除左边的全角空格
function ltrimAll(str) {
  return str.replace(/(^[\s\u3000]*)/g, "");
}

//删除右边的全角空格
function rtrimAll(str) {
  return str.replace(/([\s\u3000]*$)/g, "");
}

function showSuccessMsg(msg) {
  if (msg != null && msg != '') {
    var shortCutFunction = "Success";

    var title = $('#title').val() || '';

    toastr.options = {
      "closeButton": true,
      "debug": false,
      "positionClass": "toast-bottom-right",
      "onclick": null,
      "showDuration": "1000",
      "hideDuration": "1000",
      "timeOut": "6000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"

    };
    var $toast = toastr["success"](msg, title); // Wire up an event handler to a button in the toast, if it exists
    $toastlast = $toast;
    if ($toast.find('#okBtn').length) {
      $toast.delegate('#okBtn', 'click', function () {
        $toast.remove();
      });
    }
  }
}
function showErrorMsg(msg) {
  if (msg != null && msg != '') {
    var shortCutFunction = "Error";

    var title = $('#title').val() || '';

    toastr.options = {
      "closeButton": true,
      "debug": false,
      "positionClass": "toast-bottom-right",
      "onclick": null,
      "showDuration": "1000",
      "hideDuration": "1000",
      "timeOut": "6000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"

    };
    var $toast = toastr["error"](msg, title); // Wire up an event handler to a button in the toast, if it exists
    $toastlast = $toast;
    if ($toast.find('#okBtn').length) {
      $toast.delegate('#okBtn', 'click', function () {
        $toast.remove();
      });
    }
  }
}

function showWarningMsg(msg) {
  if (msg != null && msg != '') {
    var shortCutFunction = "warning";

    var title = $('#title').val() || '';

    toastr.options = {
      "closeButton": true,
      "debug": false,
      "positionClass": "toast-bottom-right",
      "onclick": null,
      "showDuration": "1000",
      "hideDuration": "1000",
      "timeOut": "6000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"

    };
    var $toast = toastr["warning"](msg, title); // Wire up an event handler to a button in the toast, if it exists
    $toastlast = $toast;
    if ($toast.find('#okBtn').length) {
      $toast.delegate('#okBtn', 'click', function () {
        $toast.remove();
      });
    }
  }
}

function showInfoMsg(msg) {
  if (msg != null && msg != '') {
    var shortCutFunction = "info";

    var title = $('#title').val() || '';

    toastr.options = {
      "closeButton": true,
      "debug": false,
      "positionClass": "toast-bottom-right",
      "onclick": null,
      "showDuration": "1000",
      "hideDuration": "1000",
      "timeOut": "6000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"

    };
    var $toast = toastr["info"](msg, title); // Wire up an event handler to a button in the toast, if it exists
    $toastlast = $toast;
    if ($toast.find('#okBtn').length) {
      $toast.delegate('#okBtn', 'click', function () {
        $toast.remove();
      });
    }
  }
}

function showMsg(msg, type) {
  if (msg != null && msg != '') {
    var shortCutFunction = type;

    var title = $('#title').val() || '';

    toastr.options = {
      "closeButton": true,
      "debug": false,
      "positionClass": "toast-bottom-right",
      "onclick": null,
      "showDuration": "1000",
      "hideDuration": "1000",
      "timeOut": "6000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"

    };
    var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
    $toastlast = $toast;
    if ($toast.find('#okBtn').length) {
      $toast.delegate('#okBtn', 'click', function () {
        $toast.remove();
      });
    }
  }
}


function checkMobile(value) {
  var mobile = eval(mobileRule);
  return mobile.test(value);
}

/* 替换html特殊字符 */
function escapeHtml(unsafe) {
  if (unsafe == null || unsafe == undefined || unsafe == "") {
    return "";
  } else {
    return unsafe.replace(/&/g, "&amp;")
      .replace(/</g, "&lt;")
      .replace(/>/g, "&gt;")
      .replace(/"/g, "&quot;")
      .replace(/'/g, "&#039;");
  }
}

function escapeJSChar(jsChar) {
  if (jsChar == null || jsChar == undefined || jsChar == "") {
    return "";
  } else {
    return jsChar.replace(/\\/g, "\\\\");
  }
}

/* 替换html特殊字符 */
function back2Html(unsafe) {
  if (unsafe == null || unsafe == undefined || unsafe == "") {
    return "";
  } else {
    return unsafe.replace(/&amp;/g, "&")
      .replace(/&lt;/g, "<")
      .replace(/&gt;/g, ">")
      .replace(/&quot;/g, "\"")
      .replace(/&#039;/g, "'");
  }
}

function checkUnsafeChar(unsafe) {
  //判断字符串中是否包<>"'&,包含返回true，否则返回false
  var reg = /.*[<>"'&\\]+.*$/;
  if (reg.test(unsafe)) {
    return true;
  }
  return false;
}

// format("yyyy-MM-dd hh:mm:ss")
Date.prototype.format = function (fmt) {
  var o = {
    "M+": this.getMonth() + 1,                 //月份
    "d+": this.getDate(),                    //日
    "h+": this.getHours(),                   //小时
    "m+": this.getMinutes(),                 //分
    "s+": this.getSeconds(),                 //秒
    "q+": Math.floor((this.getMonth() + 3) / 3), //季度
    "S": this.getMilliseconds()             //毫秒
  };
  if (/(y+)/.test(fmt))
    fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
  for (var k in o)
    if (new RegExp("(" + k + ")").test(fmt))
      fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
  return fmt;
}

function getUUID() {
  var guid = "";
  for (var i = 1; i <= 32; i++) {
    var n = Math.floor(Math.random() * 16.0).toString(16);
    guid += n;
    if ((i == 8) || (i == 12) || (i == 16) || (i == 20)) guid += "-";
  }
  return guid;
}

Array.prototype.contains = function (obj) {
  var i = this.length;
  while (i--) {
    if (this[i] === obj) {
      return true;
    }
  }
  return false;
}


function indexFormatter(value, row, index) {
  return index + 1;
}