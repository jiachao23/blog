layui.use(['laypage', 'layer','flow'], function(){
  var laypage = layui.laypage
    ,layer = layui.layer;

  var flow = layui.flow;
  var data = null;


  // $.ajax({
  //   type: "GET",
  //   async: false,
  //   url: "/admin/blog/list?page="+(page-1)+"&size=5",
  //   success: function(d){
  //     lis = d.data;
  //     createHtml(lis)
  //   }
  // });

  flow.load({
    elem: '#LAY_demo1' //流加载容器
    ,scrollElem: '#LAY_demo1' //滚动条所在元素，一般不用填，此处只是演示需要。
    ,done: function(page, next){ //执行下一页的回调

      //模拟数据插入
      setTimeout(function(){
        var lis = [];
        // for(var i = 0; i < 8; i++){
        //   lis.push('<li>'+ ( (page-1)*8 + i + 1 ) +'</li>')
        // }
        $.ajax({
          type: "GET",
          async: false,
          url: "/admin/blog/list?page="+(page-1)+"&size=5",
          success: function(d){
            lis = createHtml(d.data)
          }
        });
        //执行下一页渲染，第二参数为：满足“加载更多”的条件，即后面仍有分页
        //pages为Ajax返回的总页数，只有当前页小于总页数的情况下，才会继续出现加载更多
        next(lis.join(''), page < 10); //假设总页数为 10
      }, 500);
    }
  });
  function createHtml(data) {
    var htmlList = [];
    for(var i=0;i<data.length;i++){
      var htmlText = '';
      htmlText='<div class="article shadow">'
      htmlText+= '<div class="article-left">'
      htmlText+= '<img src="/'+data[i].coverURL+'" alt="'+data[i].coverURL+'" />'
      htmlText+= '</div>'
      htmlText+= '<div class="article-right">'
      htmlText+= ' <div class="article-title">'
      htmlText+= ' <a href="/article/view/'+data[i].id+'">'+data[i].title+'</a>'
      htmlText+= '</div>'
      htmlText+= '<div class="article-abstract">'+data[i].summary+'</div>'
      htmlText+= '</div>'
      htmlText+= '<div class="clear"></div>'
      htmlText+= '<div class="article-footer">'
      htmlText+= '<span><i class="fa fa-clock-o"></i>&nbsp;&nbsp;'+formatDate(data[i].createdTime,"yyyy-mm-dd hh:ii:ss")+'</span>'
      htmlText+= '<span class="article-author"><i class="fa fa-user"></i>&nbsp;&nbsp;'+data[i].username+'</span>'
      htmlText+='<span><i class="fa fa-tag"></i>&nbsp;&nbsp;<a href="#">'+data[i].tags+'</a></span>'
      htmlText+='<span class="article-viewinfo"><i class="fa fa-eye"></i>&nbsp;'+data[i].readNum+'</span>'
      htmlText+='<span class="article-viewinfo"><i class="fa fa-star"></i>&nbsp;'+data[i].heartNum+'</span>'
      htmlText+='<span class="article-viewinfo"><i class="fa fa-share-square-o"></i>&nbsp;'+data[i].shareNum+'</span>'
      htmlText+=' </div>'
      htmlText+=' </div>'
      htmlList.push(htmlText)
    }
    return htmlList;
  }
  //调用分页
  // laypage.render({
  //   elem: 'demo20'
  //   ,count: data.length
  //   ,curr:function () {
  //
  //   }
  //   ,layout:['prev','page','next']
  //   ,jump: function(obj,first){
  //     if(!first){
  //       layer.msg('第 ' + obj.curr + ' 页');
  //       location.href = '/admin/blog/list?page=' + obj.curr+'size=5';
  //     }
  //     //模拟渲染
  //     // document.getElementById('biuuu_city_list').innerHTML = function(){
  //     //   var arr = []
  //     //     ,thisData = data.concat().splice(obj.curr*obj.limit - obj.limit, obj.limit);
  //     //   layui.each(thisData, function(index, item){
  //     //     arr.push('<li>'+ item +'</li>');
  //     //   });
  //     //   return arr.join('');
  //     // }();
  //   }
  // });

});