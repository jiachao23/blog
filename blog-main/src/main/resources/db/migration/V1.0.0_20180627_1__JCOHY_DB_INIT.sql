/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50637
Source Host           : localhost:3306
Source Database       : jcohy

Target Server Type    : MYSQL
Target Server Version : 50637
File Encoding         : 65001

Date: 2017-12-29 16:14:57
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for blog
-- ----------------------------
DROP TABLE IF EXISTS `jcohy_blog`;
CREATE TABLE `jcohy_blog` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL COMMENT '标题|jiac|20180626',
  `content` longtext COMMENT '内容|jiac|20180626',
  `privacy` int(11) DEFAULT NULL COMMENT '权限级别 0 公开 1私密|jiac|20180626',
  `status` int(11) DEFAULT NULL COMMENT '状态|jiac|20180626',
  `summary` varchar(500) DEFAULT NULL COMMENT '摘要|jiac|20180626',
  `istop` int(11)  DEFAULT '0' COMMENT '是否置顶|jiac|20180626',
  `iscommend` int(11) DEFAULT '0' COMMENT '是否推荐|jiac|20180626',
  `comment_num` int(11) NOT NULL DEFAULT '0' COMMENT '评论数|jiac|20180626',
  `heart_num` int(11) NOT NULL DEFAULT '0' COMMENT '点赞数|jiac|20180626',
  `share_num` int(11) NOT NULL DEFAULT '0' COMMENT '分享数|jiac|20180626',
  `read_num` int(11) NOT NULL DEFAULT '0' COMMENT '阅读数|jiac|20180626',
  `coverURL` varchar(255) DEFAULT NULL COMMENT '封面图片地址|jiac|20180626',
  `publish_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '文章发布时间|jiac|20180626',
  `url` VARCHAR (255) NOT NULL DEFAULT '0' COMMENT '博客地址|jiac|20180626',
  `type` int(11) NOT NULL DEFAULT '1' COMMENT '博文类型，0表示普通博文，1表示富博文(带封面图片)|jiac|20180626',
  `user_id` bigint(20) DEFAULT NULL COMMENT '作者|jiac|20180626',
  `category_id` bigint(20) DEFAULT NULL COMMENT '分类|jiac|20180626',
  `deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否已删除|jiac|20180626',
  `delete_by` bigint(20) DEFAULT NULL  COMMENT '删除者|jiac|20180626',
  `deleted_time` datetime DEFAULT NULL COMMENT '删除时间|jiac|20180626',
  `created_by` bigint(20) NOT NULL DEFAULT '1' COMMENT '创建者|jiac|20180626',
  `created_time` datetime NOT NULL COMMENT '创建时间|jiac|20180626',
  `updated_by` bigint(20) NOT NULL DEFAULT '1' COMMENT '修改者|jiac|20180626',
  `updated_time` datetime NOT NULL COMMENT '修改时间|jiac|20180626',
  PRIMARY KEY (`id`),
  KEY `FK_Reference_user_id` (`user_id`),
  KEY `FK_Reference_category_id` (`category_id`),
  CONSTRAINT `FK_Reference_user_id` FOREIGN KEY (`user_id`) REFERENCES `jcohy_user` (`id`),
  CONSTRAINT `FK_Reference_category_id` FOREIGN KEY (`category_id`) REFERENCES `jcohy_category` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8  COMMENT='博客信息|jiac|20180626';

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `jcohy_category`;
CREATE TABLE `jcohy_category` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `count` int(11) NOT NULL DEFAULT '0' COMMENT '数量|jiac|20180626',
  `name` varchar(255) DEFAULT NULL COMMENT '名称|jiac|20180626',
  `en_name` varchar(255) DEFAULT NULL COMMENT '英文名称|jiac|20180626',
  `parent_id` bigint(20) DEFAULT NULL COMMENT '上级分类ID|jiac|20180626',
  `level` int(11) NOT NULL DEFAULT '0' COMMENT '级别|jiac|20180626',
  `type` int(11) DEFAULT NULL COMMENT '类型|jiac|20180626',
  `target` varchar(10) DEFAULT NULL COMMENT '打开方式|jiac|20180626',
  `visible` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0,不可见，1可见|jiac|20180626''',
  `orders` bigint(20) DEFAULT NULL COMMENT '排序|jiac|20180626',
  `deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否已删除|jiac|20180626',
  `delete_by` bigint(20) NOT NULL DEFAULT '1' COMMENT '删除者|jiac|20180626',
  `deleted_time` datetime DEFAULT NULL COMMENT '删除时间|jiac|20180626',
  `created_by` bigint(20) NOT NULL DEFAULT '1' COMMENT '创建者|jiac|20180626',
  `created_time` datetime NOT NULL COMMENT '创建时间|jiac|20180626',
  `updated_by` bigint(20) NOT NULL DEFAULT '1' COMMENT '修改者|jiac|20180626',
  `updated_time` datetime NOT NULL COMMENT '修改时间|jiac|20180626',
  PRIMARY KEY (`id`),
  KEY `FK_Reference_m_category_id` (`parent_id`),
  CONSTRAINT `FK_Reference_m_category_id` FOREIGN KEY (`parent_id`) REFERENCES `jcohy_category` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8  COMMENT='分类表|jiac|20180626';


-- ----------------------------
-- Table structure for link
-- ----------------------------
DROP TABLE IF EXISTS `jcohy_link`;
CREATE TABLE `jcohy_link` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL COMMENT '标题|jiac|20180626',
  `url` varchar(500) DEFAULT NULL COMMENT '地址|jiac|20180626',
  `email` varchar(255) DEFAULT NULL COMMENT '站长邮箱|jiac|20180626',
  `description` varchar(255) DEFAULT NULL COMMENT '描述|jiac|20180626',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '状态，可见或者不可见|jiac|20180626',
  `logo` VARCHAR(255) DEFAULT '' COMMENT 'logo|jiac|20180626',
  `deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否已删除|jiac|20180626',
  `delete_by` bigint(20) NOT NULL DEFAULT '1' COMMENT '删除者|jiac|20180626',
  `deleted_time` datetime DEFAULT NULL COMMENT '删除时间|jiac|20180626',
  `created_by` bigint(20) NOT NULL DEFAULT '1' COMMENT '创建者|jiac|20180626',
  `created_time` datetime NOT NULL COMMENT '创建时间|jiac|20180626',
  `updated_by` bigint(20) NOT NULL DEFAULT '1' COMMENT '修改者|jiac|20180626',
  `updated_time` datetime NOT NULL COMMENT '修改时间|jiac|20180626',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8  COMMENT='友情链接表|jiac|20180626';

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `jcohy_menu`;
CREATE TABLE `jcohy_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(100) DEFAULT NULL COMMENT '菜单名称|jiac|20180626',
  `en_name` varchar(100) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL COMMENT '链接|jiac|20180626',
  `parent_id` bigint(20) DEFAULT NULL COMMENT '上级菜单ID|jiac|20180626',
  `icon` varchar(50) NOT NULL DEFAULT 'fa fa-dashboard' COMMENT '菜单图标|jiac|20180626',
  `level` int(11) NOT NULL DEFAULT '0' COMMENT '菜单级别|jiac|20180626',
  `type` int(11) DEFAULT NULL COMMENT '类型 0：目录 1：菜单|jiac|20180626',
  `target` varchar(10) DEFAULT NULL COMMENT '打开方式|jiac|20180626',
  `visible` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0,不可见，1可见|jiac|20180626''',
  `orders` bigint(20) DEFAULT NULL COMMENT '排序|jiac|20180626',
  `deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否已删除|jiac|20180626',
  `delete_by` bigint(20) NOT NULL DEFAULT '1' COMMENT '删除者|jiac|20180626',
  `deleted_time` datetime DEFAULT NULL COMMENT '删除时间|jiac|20180626',
  `created_by` bigint(20) NOT NULL DEFAULT '1' COMMENT '创建者|jiac|20180626',
  `created_time` datetime NOT NULL COMMENT '创建时间|jiac|20180626',
  `updated_by` bigint(20) NOT NULL DEFAULT '1' COMMENT '修改者|jiac|20180626',
  `updated_time` datetime NOT NULL COMMENT '修改时间|jiac|20180626',
  PRIMARY KEY (`id`),
  KEY `FK_Reference_m_menu_id` (`parent_id`),
  CONSTRAINT `FK_Reference_m_menu_id` FOREIGN KEY (`parent_id`) REFERENCES `jcohy_menu` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `jcohy_menu_permission`;
CREATE TABLE `jcohy_menu_permission` (
  `menu_id` bigint(20) NOT NULL,
  `permission_id` bigint(20) NOT NULL,
  PRIMARY KEY (`menu_id`,`permission_id`),
  KEY `FK_Reference_mp_permission_id` (`permission_id`),
  CONSTRAINT `FK_Reference_mp_menu_id` FOREIGN KEY (`menu_id`) REFERENCES `jcohy_menu` (`id`),
  CONSTRAINT `FK_Reference_mp_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `jcohy_permission` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for permission
-- ----------------------------
DROP TABLE IF EXISTS `jcohy_permission`;
CREATE TABLE `jcohy_permission` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键|jiac|20180626',
  `system_id` int(10) NOT NULL DEFAULT '1' COMMENT '系统编号|jiac|20180626',
  `parent_id` bigint(20) DEFAULT NULL COMMENT '所属上级|jiac|20180626',
  `name` varchar(20) DEFAULT NULL COMMENT '名称|jiac|20180626',
  `en_name` varchar(45) DEFAULT NULL COMMENT '英文名称|jiac|20180626',
  `type` tinyint(4) DEFAULT NULL COMMENT '类型(0:目录,1:菜单,2:功能)|jiac|20180626',
  `permission_value` varchar(50) DEFAULT NULL COMMENT '权限值|jiac|20180626',
  `url` varchar(100) DEFAULT NULL COMMENT '路径|jiac|20180626',
  `method` varchar(45) DEFAULT NULL COMMENT '‘GET’,’POST’,’PUT’,’DELETE’|jiac|20180626',
  `icon` varchar(50) DEFAULT NULL COMMENT '图标',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '状态(0:禁止,1:正常)|jiac|20180626',
  `orders` bigint(20) DEFAULT NULL COMMENT '排序',
  `is_sys` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否系统保留，系统保留功能普通用户不可见|jiac|20180626',
  `deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否已删除|jiac|20180626',
  `delete_by` bigint(20) NOT NULL DEFAULT '1' COMMENT '删除者|jiac|20180626',
  `deleted_time` datetime DEFAULT NULL COMMENT '删除时间|jiac|20180626',
  `created_by` bigint(20) NOT NULL DEFAULT '1' COMMENT '创建者|jiac|20180626',
  `created_time` datetime NOT NULL COMMENT '创建时间|jiac|20180626',
  `updated_by` bigint(20) NOT NULL DEFAULT '1' COMMENT '修改者|jiac|20180626',
  `updated_time` datetime NOT NULL COMMENT '修改时间|jiac|20180626',
  PRIMARY KEY (`id`),
  KEY `FK_Reference_15` (`system_id`),
  KEY `FK_Reference_20` (`parent_id`),
  CONSTRAINT `FK_Reference_15` FOREIGN KEY (`system_id`) REFERENCES `jcohy_console_system` (`id`),
  CONSTRAINT `FK_Reference_20` FOREIGN KEY (`parent_id`) REFERENCES `jcohy_permission` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `jcohy_role`;
CREATE TABLE `jcohy_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键|jiac|20180626',
  `state` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态, 0:禁用, 1:启用|jiac|20180626',
  `name` varchar(50) DEFAULT NULL COMMENT '角色名称|jiac|20180626',
  `code` varchar(50) DEFAULT NULL COMMENT '角色代码, 已ROLE_为前缀的英文|jiac|20180626',
  `description` varchar(255) DEFAULT NULL COMMENT '角色描述|jiac|20180626',
  `is_sys` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否系统保留，系统保留功能普通用户不可见|jiac|20180626',
  `authority_model` varchar(45) NOT NULL DEFAULT '4' COMMENT '1:仅本人，2:本部门，3:本部门及其下属部门，4:本机构及其下属部门，5:所有数据|jiac|20180626',
  `deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否已删除|jiac|20180626',
  `delete_by` bigint(20) NOT NULL DEFAULT '1' COMMENT '删除者|jiac|20180626',
  `deleted_time` datetime DEFAULT NULL COMMENT '删除时间|jiac|20180626',
  `created_by` bigint(20) NOT NULL DEFAULT '1' COMMENT '创建者|jiac|20180626',
  `created_time` datetime NOT NULL COMMENT '创建时间|jiac|20180626',
  `updated_by` bigint(20) NOT NULL DEFAULT '1' COMMENT '修改者|jiac|20180626',
  `updated_time` datetime NOT NULL COMMENT '修改时间|jiac|20180626',
  PRIMARY KEY (`id`),
  KEY `Index_1_rolename` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- ----------------------------
-- Table structure for options
-- ----------------------------
DROP TABLE IF EXISTS `jcohy_options`;
CREATE TABLE `jcohy_options` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `option_key` varchar(255) DEFAULT NULL COMMENT 'key|jiac|20180626',
  `option_value` varchar(255) DEFAULT NULL COMMENT 'value|jiac|20180626',
  `description` varchar(255) DEFAULT NULL COMMENT 'description|jiac|20180626',
  `deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否已删除|jiac|20180626',
  `delete_by` bigint(20) NOT NULL DEFAULT '1' COMMENT '删除者|jiac|20180626',
  `deleted_time` datetime DEFAULT NULL COMMENT '删除时间|jiac|20180626',
  `created_by` bigint(20) NOT NULL DEFAULT '1' COMMENT '创建者|jiac|20180626',
  `created_time` datetime NOT NULL COMMENT '创建时间|jiac|20180626',
  `updated_by` bigint(20) NOT NULL DEFAULT '1' COMMENT '修改者|jiac|20180626',
  `updated_time` datetime NOT NULL COMMENT '修改时间|jiac|20180626',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='网站信息|jiac|20180117';

-- ----------------------------
-- Table structure for role_permission
-- ----------------------------
DROP TABLE IF EXISTS `jcohy_role_permission`;
CREATE TABLE `jcohy_role_permission` (
  `role_id` bigint(20) NOT NULL,
  `permission_id` bigint(20) NOT NULL,
  PRIMARY KEY (`role_id`,`permission_id`),
  KEY `FK_Reference_permission_id` (`permission_id`),
  CONSTRAINT `FK_Reference_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `jcohy_permission` (`id`),
  CONSTRAINT `FK_Reference_role_id` FOREIGN KEY (`role_id`) REFERENCES `jcohy_role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- ----------------------------
-- Table structure for tag
-- ----------------------------
DROP TABLE IF EXISTS `jcohy_tag`;
CREATE TABLE `jcohy_tag` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `count` int(11) NOT NULL DEFAULT '0' COMMENT '当前标签下的数量|jiac|20180626',
  `name` varchar(255) DEFAULT NULL COMMENT '名称|jiac|20180626',
  `visible` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0,不可见，1可见|jiac|20180626''',
  `deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否已删除|jiac|20180626',
  `delete_by` bigint(20) NOT NULL DEFAULT '1' COMMENT '删除者|jiac|20180626',
  `deleted_time` datetime DEFAULT NULL COMMENT '删除时间|jiac|20180626',
  `created_by` bigint(20) NOT NULL DEFAULT '1' COMMENT '创建者|jiac|20180626',
  `created_time` datetime NOT NULL COMMENT '创建时间|jiac|20180626',
  `updated_by` bigint(20) NOT NULL DEFAULT '1' COMMENT '修改者|jiac|20180626',
  `updated_time` datetime NOT NULL COMMENT '修改时间|jiac|20180626',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8  COMMENT='tags表|jiac|20180117';

DROP TABLE IF EXISTS `jcohy_blog_tag`;
CREATE TABLE `jcohy_blog_tag` (
  `blog_id` bigint(20) NOT NULL,
  `tag_id` bigint(20) NOT NULL,
  PRIMARY KEY (`blog_id`,`tag_id`),
  KEY `FK_Reference_bt_blog_id` (`blog_id`),
  CONSTRAINT `FK_Reference_bt_blog_id` FOREIGN KEY (`blog_id`) REFERENCES `jcohy_blog` (`id`),
  CONSTRAINT `FK_Reference_bt_tag_id` FOREIGN KEY (`tag_id`) REFERENCES `jcohy_tag` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8  COMMENT='blog_tag|jiac|20180117';

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `jcohy_user`;
CREATE TABLE `jcohy_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(255) DEFAULT NULL COMMENT '真实姓名|jiac|20180626',
  `username` varchar(100) NOT NULL COMMENT '昵称|jiac|20180626',
  `password` varchar(255) NOT NULL COMMENT '密码MD5(密码+盐)',
  `salt` varchar(32) DEFAULT NULL COMMENT '盐',
  `avatar` varchar(255) DEFAULT NULL COMMENT '头像|jiac|20180626',
  `phone` varchar(20) DEFAULT NULL COMMENT '电话',
  `email` varchar(100) DEFAULT NULL COMMENT '邮箱',
  `sex` tinyint(4) DEFAULT NULL COMMENT '性别',
  `description` varchar(255) DEFAULT NULL COMMENT '简介|jiac|20180626',
  `ip` varchar(255) DEFAULT NULL COMMENT '注册修改ip|jiac|20180626',
  `last_login_date` datetime DEFAULT NULL COMMENT '最后登录时间|jiac|20180626',
  `last_password_reset_date` datetime DEFAULT NULL COMMENT '密码最后修改时间|jiac|20180626',
  `state` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态(0:锁定，1:正常,2:激活,3:未激活))|jiac|20180626',
  `is_sys` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否系统保留，系统保留功能普通用户不可见|jiac|20180626',
  `type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '用户类型:  0:数据库用户，1:LDAP用户|jiac|20180626',
  `deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否已删除|jiac|20180626',
  `delete_by` bigint(20) NOT NULL DEFAULT '1' COMMENT '删除者|jiac|20180626',
  `deleted_time` datetime DEFAULT NULL COMMENT '删除时间|jiac|20180626',
  `created_by` bigint(20) NOT NULL DEFAULT '1' COMMENT '创建者|jiac|20180626',
  `created_time` datetime NOT NULL COMMENT '创建时间|jiac|20180626',
  `updated_by` bigint(20) NOT NULL DEFAULT '1' COMMENT '修改者|jiac|20180626',
  `updated_time` datetime NOT NULL COMMENT '修改时间|jiac|20180626',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8  COMMENT='用户表|jiac|20180117';

DROP TABLE IF EXISTS `jcohy_user_role`;
CREATE TABLE `jcohy_user_role` (
  `user_id` bigint(20) NOT NULL,
  `role_id` bigint(20) NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `FK_Reference_ur_role_id` (`role_id`),
  CONSTRAINT `FK_Reference_ur_role_id` FOREIGN KEY (`role_id`) REFERENCES `jcohy_role` (`id`),
  CONSTRAINT `FK_Reference_ur_user_id` FOREIGN KEY (`user_id`) REFERENCES `jcohy_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- ----------------------------
-- Table structure for notice
-- ----------------------------
DROP TABLE IF EXISTS `jcohy_notice`;
CREATE TABLE `jcohy_notice` (
   `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `content` mediumtext NOT NULL COMMENT '公告内容|jiac|20180626',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT 'notice链接|jiac|20180626',
  `start` datetime NOT NULL COMMENT '开始时间|jiac|20180626',
  `end` datetime NOT NULL COMMENT '结束时间|jiac|20180626',
  `visible` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0,不可见，1可见|jiac|20180626''',
  `deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否已删除|jiac|20180626',
  `delete_by` bigint(20) NOT NULL DEFAULT '1' COMMENT '删除者|jiac|20180626',
  `deleted_time` datetime DEFAULT NULL COMMENT '删除时间|jiac|20180626',
  `created_by` bigint(20) NOT NULL DEFAULT '1' COMMENT '创建者|jiac|20180626',
  `created_time` datetime NOT NULL COMMENT '创建时间|jiac|20180626',
  `updated_by` bigint(20) NOT NULL DEFAULT '1' COMMENT '修改者|jiac|20180626',
  `updated_time` datetime NOT NULL COMMENT '修改时间|jiac|20180626',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='公告|jiac|20180117';

-- -----------------------------------
-- Table structure for comment  评论
-- -----------------------------------
DROP TABLE IF EXISTS `jcohy_comment`;
CREATE TABLE `jcohy_comment` (
   `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nickname` varchar(20) NOT NULL COMMENT '评论人姓名|jiac|20180626',
  `content` text NOT NULL COMMENT '评论内容|jiac|20180626',
  `like_num` int(11) NOT NULL DEFAULT '0' COMMENT '评论|jiac|20180626',
  `hate_num` int(11) NOT NULL DEFAULT '0' COMMENT '评论|jiac|20180626',
  `reply_num` int(11) NOT NULL DEFAULT '0' COMMENT '回复|jiac|20180626',
  `parent` int(20) DEFAULT NULL COMMENT '评论|jiac|20180626',
  `blog_id` int(20) NOT NULL COMMENT '评论|jiac|20180626',
  `head_url` varchar(100) DEFAULT NULL,
  `check` int(11) DEFAULT '0' COMMENT '是否审核，0表示未审核，1表示审核|jiac|20180626',
  `status` int(11) DEFAULT '1' COMMENT '是否合法，0未通过，1通过|jiac|20180626',
  `deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否已删除|jiac|20180626',
  `delete_by` bigint(20) NOT NULL DEFAULT '1' COMMENT '删除者|jiac|20180626',
  `deleted_time` datetime DEFAULT NULL COMMENT '删除时间|jiac|20180626',
  `created_by` bigint(20) NOT NULL DEFAULT '1' COMMENT '创建者|jiac|20180626',
  `created_time` datetime NOT NULL COMMENT '创建时间|jiac|20180626',
  `updated_by` bigint(20) NOT NULL DEFAULT '1' COMMENT '修改者|jiac|20180626',
  `updated_time` datetime NOT NULL COMMENT '修改时间|jiac|20180626',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='评论|jiac|20180117';

-- -----------------------------------
-- Table structure for timeline  时光轴
-- -----------------------------------
DROP TABLE IF EXISTS `jcohy_timeline`;
CREATE TABLE `jcohy_timeline` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `display_name` varchar(50) NOT NULL COMMENT '显示名称|jiac|20180626',
  `display_date` varchar(50) NOT NULL COMMENT '显示日期|jiac|20180626',
  `deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否已删除|jiac|20180626',
  `delete_by` bigint(20) NOT NULL DEFAULT '1' COMMENT '删除者|jiac|20180626',
  `deleted_time` datetime DEFAULT NULL COMMENT '删除时间|jiac|20180626',
  `created_by` bigint(20) NOT NULL DEFAULT '1' COMMENT '创建者|jiac|20180626',
  `created_time` datetime NOT NULL COMMENT '创建时间|jiac|20180626',
  `updated_by` bigint(20) NOT NULL DEFAULT '1' COMMENT '修改者|jiac|20180626',
  `updated_time` datetime NOT NULL COMMENT '修改时间|jiac|20180626',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='type|jiac|20180117';

-- ----------------------------
-- Table structure for console_dict
-- ----------------------------
DROP TABLE IF EXISTS `jcohy_console_dict`;
CREATE TABLE `jcohy_console_dict` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(20) DEFAULT NULL,
  `name` varchar(255) NOT NULL COMMENT '中文名称|jiac|20180626',
  `en_name` varchar(255) DEFAULT NULL COMMENT '英文名称|jiac|20180626',
  `param_name` varchar(100) NOT NULL COMMENT '参数名称或变量名称|jiac|20180626',
  `param_value` varchar(100) NOT NULL COMMENT '参数值|jiac|20180626',
  `value_type` varchar(32) NOT NULL COMMENT '参数类型：0:string, 1:integer, 2: long, 3:date|jiac|20180626',
  `param_value_sub` varchar(255) DEFAULT NULL COMMENT '保存参数值子项或附加值|jiac|20180626',
  `type` tinyint(4) NOT NULL COMMENT '数据记录类型：0:码表项， 1:标签项|jiac|20180626',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注|jiac|20180626',
  `deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否已删除|jiac|20180626',
  `delete_by` bigint(20) NOT NULL DEFAULT '1' COMMENT '删除者|jiac|20180626',
  `deleted_time` datetime DEFAULT NULL COMMENT '删除时间|jiac|20180626',
  `created_by` bigint(20) NOT NULL DEFAULT '1' COMMENT '创建者|jiac|20180626',
  `created_time` datetime NOT NULL COMMENT '创建时间|jiac|20180626',
  `updated_by` bigint(20) NOT NULL DEFAULT '1' COMMENT '修改者|jiac|20180626',
  `updated_time` datetime NOT NULL COMMENT '修改时间|jiac|20180626',
  PRIMARY KEY (`id`),
  KEY `Index_param_name` (`param_name`,`parent_id`),
  KEY `FK_Reference_1` (`parent_id`),
  CONSTRAINT `FK_Reference_1` FOREIGN KEY (`parent_id`) REFERENCES `jcohy_console_dict` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='系统字典表，保存系统预设变量值|jiac|20180626';

-- ----------------------------
-- Table structure for jcohy_log
-- ----------------------------
DROP TABLE IF EXISTS `jcohy_log`;
CREATE TABLE `jcohy_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号|jiac|20180626',
  `module` varchar(50) DEFAULT NULL COMMENT '功能模块|jiac|20180626',
  `description` varchar(100) DEFAULT NULL COMMENT '操作描述|jiac|20180626',
  `user_id` bigint(20) DEFAULT NULL COMMENT '操作用户Id|jiac|20180626',
  `username` varchar(20) DEFAULT NULL COMMENT '操作用户|jiac|20180626',
  `start_time` bigint(20) DEFAULT NULL COMMENT '操作时间|jiac|20180626',
  `spend_time` int(11) DEFAULT NULL COMMENT '消耗时间|jiac|20180626',
  `base_path` varchar(100) DEFAULT NULL COMMENT '根路径|jiac|20180626',
  `url` varchar(200) DEFAULT NULL COMMENT 'URL|jiac|20180626',
  `method` varchar(10) DEFAULT NULL COMMENT '请求类型|jiac|20180626',
  `parameter` mediumtext COMMENT '请求参数|jiac|20180626',
  `http_status_code` varchar(20) DEFAULT NULL COMMENT '状态|jiac|20180626',
  `ip` varchar(128) DEFAULT NULL COMMENT 'IP地址|jiac|20180626',
  `result` mediumtext COMMENT '响应结果|jiac|20180626',
  PRIMARY KEY (`id`),
  KEY `Index_1_optime` (`start_time`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='操作日志|jiac|20180626';


-- ----------------------------
-- Table structure for resource
-- ----------------------------
DROP TABLE IF EXISTS `jcohy_resource`;
CREATE TABLE `jcohy_resource` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL  COMMENT '名称|jiac|20180206',
  `description` varchar(255) DEFAULT NULL COMMENT '描述|jiac|20180206',
  `keyword` varchar(20) DEFAULT '' COMMENT '关键字|jiac|20180206',
  `upload_url` varchar(255) NOT NULL DEFAULT '' COMMENT '上传地址|jiac|20180206',
  `download_url` varchar(255) NOT NULL DEFAULT '' COMMENT '下载地址|jiac|20180206',
  `category_id` bigint(20) DEFAULT NULL  COMMENT 'software可见|jiac|20180131',
  `visible` int(11) NOT NULL DEFAULT '0' COMMENT '此软件是否可见|jiac|20180206',
  `deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否已删除|jiac|20180626',
  `delete_by` bigint(20) NOT NULL DEFAULT '1' COMMENT '删除者|jiac|20180626',
  `deleted_time` datetime DEFAULT NULL COMMENT '删除时间|jiac|20180626',
  `created_by` bigint(20) NOT NULL DEFAULT '1' COMMENT '创建者|jiac|20180626',
  `created_time` datetime NOT NULL COMMENT '创建时间|jiac|20180626',
  `updated_by` bigint(20) NOT NULL DEFAULT '1' COMMENT '修改者|jiac|20180626',
  `updated_time` datetime NOT NULL COMMENT '修改时间|jiac|20180626',
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  CONSTRAINT `FK_resource_category` FOREIGN KEY (`category_id`) REFERENCES `jcohy_category` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='资源表|jiac|20180206';


