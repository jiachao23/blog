


create table jcohy_console_system
(
  id                   int not null auto_increment comment '编号',
  name                 varchar(20) not null comment '系统名称',
  code                 varchar(20) not null,
  title                varchar(20) comment '系统标题',
  description          varchar(300) comment '系统描述',
  status               tinyint(4) comment '状态(-1:黑名单,1:正常)',
  orders               bigint(20) comment '排序',
  deleted              bool not null default 0 comment '是否已删除',
  deleted_time         datetime comment '删除时间',
  created_by           bigint not null comment '创建者',
  created_time         datetime not null comment '创建时间',
  updated_by           bigint not null comment '修改者',
  updated_time         datetime not null comment '修改时间',
  primary key (id)
);

alter table jcohy_console_system comment '系统表';

/*==============================================================*/
/* Index: Index_1_code                                          */
/*==============================================================*/
create unique index Index_1_code on jcohy_console_system
(
  code
);


