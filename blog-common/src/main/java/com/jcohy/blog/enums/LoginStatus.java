package com.jcohy.blog.enums;

/**
 * Created by jiac on 2018/7/17.
 * ClassName  : com.jcohy.enums
 * Description  :
 */
public enum LoginStatus {
    SUCCESS(0, "Successful"),
    USER_NOT_EXIST(1, "User does not exist"),
    PASSWORD_ERROR(2, "Password error"),
    NEED_CHANGE_PWD(3,"Need to modify the password"),
    APPVERSION_ERROR(4, "APP Version code error"),
    PARAME_RROR(5, "Parameter error"),
    USER_EXISTS(6, "The user has already existed"),
    VERIFY_CODE_INVALID(7, "Verification code error"),
    EMAIL_ADDRESS_IS_WRONG(8, "Mailbox format error"),
    PHONE_NUMBER_IS_WRONG(9, "Phone number error"),
    CONFLICTAPPLOGON(10, "Has been logon in the mutex application"),
    MAXSESSIONLIMIT(11, "Session has reached the upper limit"),

    OTHERS(99, "Other error");
    private final int code;
    private final String msg;

    private LoginStatus(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    public static LoginStatus codeOf(int code) {
        for (LoginStatus status : values()) {
            if (status.code == code) {
                return status;
            }
        }
        return OTHERS;
    }
}
