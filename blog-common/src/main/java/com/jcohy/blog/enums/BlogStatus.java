package com.jcohy.blog.enums;

/**
 * Created by jiac on 2018/9/27.
 * ClassName  : com.jcohy.enums
 * Description  :
 */
public enum BlogStatus {
    SUCCESS(0, "操作成功"),
    USER_NOT_EXIST(1, "用户不存在"),
    BLOG_TAG_NOT_NULL(7, "博客的标签不能为空"),
    BLOG_TYPE_NOT_NULL(8, "博客的分类不能为空"),
    PARAME_RROR(5, "参数错误"),
    USER_EXISTS(6, "用户已存在"),
    OTHERS(99, "其他错误");

    private final int code;
    private final String msg;
    private BlogStatus(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    public static BlogStatus codeOf(int code) {
        for (BlogStatus status : values()) {
            if (status.code == code) {
                return status;
            }
        }
        return OTHERS;
    }
}
