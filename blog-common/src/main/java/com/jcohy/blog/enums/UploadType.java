package com.jcohy.blog.enums;

/**
 * Created by jiac on 2018/9/7.
 * ClassName  : com.jcohy.enums
 * Description  :
 */
public enum UploadType {
    BLOG("blog"),
    RESOURCE("resource"),
    Video("video");

    private String type;

    UploadType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
