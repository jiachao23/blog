package com.jcohy.blog.enums;

/**
 * Created by jiac on 2018/9/7.
 * ClassName  : com.jcohy.enums
 * Description  :
 */
public enum CommonStatus {

    SUCCESS(0,"SUCCESS"),
    PARAM_ERROE(5,"PARAMS EORROE"),
    OTHER(99,"Other error");

    CommonStatus(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    private Integer code;
    private String msg;

    public Integer getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

}
