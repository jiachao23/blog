package com.jcohy.blog.enums;

/**
 * Created by jiac on 2018/9/11.
 * ClassName  : com.star.uums.dto
 * Description  :
 */
public enum CachePrefix {

    JCOHY_USER_KEY(0,"jcohy:user:");

    private Integer code;

    private String keyfix;

    private CachePrefix(Integer code , String keyfix) {
        this.keyfix = keyfix;
        this.code = code;
    }

    public String getKeyfix() {
        return keyfix;
    }

    public void setKeyfix(String keyfix) {
        this.keyfix = keyfix;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public static String typeOf(Integer code){
        for(CachePrefix cachePrefix:values()){
            if(cachePrefix.getCode().equals(code)){
                return cachePrefix.getKeyfix();
            }
        }
        return null;
    }
}
