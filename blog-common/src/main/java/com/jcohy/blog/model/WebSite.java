package com.jcohy.blog.model;

import javax.persistence.*;

/**
 * Copyright  : 2015-2033 Beijing
 * Created by jiac on 2018/3/9 17:40.
 * ClassName  : WebSite
 * Description  :
 */

@Entity
@Table(name = "jcohy_website")
public class WebSite extends AbstractModel {


    private String name;


    private String url;


    private String description;


    private String coverUrl;


    private Integer status;


    private Category category;

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "url")
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "cover_url")
    public String getCoverUrl() {
        return coverUrl;
    }

    public void setCoverUrl(String coverUrl) {
        this.coverUrl = coverUrl;
    }

    @Column(name = "status")
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @ManyToOne
    @JoinColumn(name = "category_id")
    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}
