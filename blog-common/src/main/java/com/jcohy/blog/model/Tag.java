package com.jcohy.blog.model;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;


/**
 * Copyright  : 2017- www.jcohy.com
 * Created by jiac on 2:06 2017/12/16
 * Email: jia_chao23@126.com
 * ClassName: Tag
 * Description:
 **/

@Entity
@Table(name = "jcohy_tag")
public class Tag extends AbstractModel implements Serializable{

	private String name;
	/**
	 * 显示状态
	 * <br>
	 * 0 显示
	 * <br>
	 * 1隐藏
	 * <br>
	 */
	private boolean visible;

	@Column(nullable = false,columnDefinition="int default 0")
	private Integer count;


	private Set<Blog> blogs = new HashSet<Blog>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	@ManyToMany(mappedBy="tags")
	@JsonIgnore
	public Set<Blog> getBlogs() {
		return blogs;
	}

	public void setBlogs(Set<Blog> blogs) {
		this.blogs = blogs;
	}
}
