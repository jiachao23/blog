package com.jcohy.blog.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Copyright  : 2015-2033 Beijing Startimes Communication & Network Technology Co.Ltd
 * Created by jiac on 2018/1/4 10:33.
 * ClassName  : Comment
 * Description  :
 */
@Entity
@Table(name = "jcohy_comment")
public class Comment extends AbstractModel {


    private String nickName;
    private String email;
    private String content;
    private Integer likeNum;
    private Integer hateNum;
    private Integer parent;
    private Integer blogId;
    private Integer replyNum;
    private String headUrl;
    /**
     * 是否审核，0表示未审核，1表示审核
     */
    private Integer check;
    /**
     * 是否合法，0未通过，1通过
     */
    private Integer status;

    @Column(name = "nickname")
    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Column(name = "content")
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Column(name = "like_num")
    public Integer getLikeNum() {
        return likeNum;
    }

    public void setLikeNum(Integer likeNum) {
        this.likeNum = likeNum;
    }

    @Column(name = "hate_num")
    public Integer getHateNum() {
        return hateNum;
    }

    public void setHateNum(Integer hateNum) {
        this.hateNum = hateNum;
    }

    @Column(name = "parent")
    public Integer getParent() {
        return parent;
    }

    public void setParent(Integer parent) {
        this.parent = parent;
    }

    @Column(name = "blog_id")
    public Integer getBlogId() {
        return blogId;
    }

    public void setBlogId(Integer blogId) {
        this.blogId = blogId;
    }

    @Column(name = "reply_num")
    public Integer getReplyNum() {
        return replyNum;
    }

    public void setReplyNum(Integer replyNum) {
        this.replyNum = replyNum;
    }

    @Column(name = "head_url")
    public String getHeadUrl() {
        return headUrl;
    }

    public void setHeadUrl(String headUrl) {
        this.headUrl = headUrl;
    }

    @Column(name = "check",columnDefinition = "int default 0")
    public Integer getCheck() {
        return check;
    }

    public void setCheck(Integer check) {
        this.check = check;
    }

    @Column(name = "status" ,columnDefinition = "int default 1")
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
