package com.jcohy.blog.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by jiac on 2018/6/28.
 * ClassName  : com.jcohy.model
 * Description  :
 */

@Entity
@Table(name = "jcohy_dict")
public class Dict extends AbstractModel{
    private Dict parent;
    private String name;
    private String enName;
    private String paramName;
    private String paramValue;
    private String valueType;
    private String paramValueSub;
    private Integer type;
    private String remark;

    private Set<Dict> children = new LinkedHashSet<>();

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "parent_id")
    @JsonIgnore
    public Dict getParent() {
        return parent;
    }

    public void setParent(Dict parent) {
        this.parent = parent;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "en_name")
    public String getEnName() {
        return enName;
    }

    public void setEnName(String enName) {
        this.enName = enName;
    }

    @Basic
    @Column(name = "param_name")
    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    @Basic
    @Column(name = "param_value")
    public String getParamValue() {
        return paramValue;
    }

    public void setParamValue(String paramValue) {
        this.paramValue = paramValue;
    }

    @Basic
    @Column(name = "value_type", nullable = true)
    public String getValueType() {
        return valueType;
    }

    public void setValueType(String valueType) {
        this.valueType = valueType;
    }

    @Basic
    @Column(name = "param_value_sub")
    public String getParamValueSub() {
        return paramValueSub;
    }

    public void setParamValueSub(String paramValueSub) {
        this.paramValueSub = paramValueSub;
    }

    @Basic
    @Column(name = "type")
    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    @Basic
    @Column(name = "remark")
    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @OneToMany(mappedBy = "parent", fetch = FetchType.LAZY)
    public Set<Dict> getChildren() {
        return children;
    }

    public void setChildren(Set<Dict> children) {
        this.children = children;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Dict dict = (Dict) o;

        if (name != null ? !name.equals(dict.name) : dict.name != null) return false;
        if (enName != null ? !enName.equals(dict.enName) : dict.enName != null) return false;
        if (paramName != null ? !paramName.equals(dict.paramName) : dict.paramName != null) return false;
        if (paramValue != null ? !paramValue.equals(dict.paramValue) : dict.paramValue != null) return false;
        if (valueType != null ? !valueType.equals(dict.valueType) : dict.valueType != null) return false;
        if (paramValueSub != null ? !paramValueSub.equals(dict.paramValueSub) : dict.paramValueSub != null)
            return false;
        return type != null ? type.equals(dict.type) : dict.type == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (enName != null ? enName.hashCode() : 0);
        result = 31 * result + (paramName != null ? paramName.hashCode() : 0);
        result = 31 * result + (paramValue != null ? paramValue.hashCode() : 0);
        result = 31 * result + (valueType != null ? valueType.hashCode() : 0);
        result = 31 * result + (paramValueSub != null ? paramValueSub.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Dict{" +
                ", name='" + name + '\'' +
                ", enName='" + enName + '\'' +
                ", paramName='" + paramName + '\'' +
                ", paramValue='" + paramValue + '\'' +
                ", valueType='" + valueType + '\'' +
                ", paramValueSub='" + paramValueSub + '\'' +
                ", type='" + type + '\'' +
                ", remark='" + remark + '\'' +
                ", children=" + children +
                '}';
    }
}
