package com.jcohy.blog.model;

import javax.persistence.*;

/**
 * Copyright  : 2015-2033 Beijing Startimes Communication & Network Technology Co.Ltd
 * Created by jiac on 2018/2/6 13:30.
 * ClassName  : Resource
 * Description  :
 */
@Entity
@Table(name = "jcohy_resource")
public class Resource extends AbstractModel {

    private String name;
    private String description;
    private String keyword;
    private String downloadUrl;
    private String uploadUrl;
    private boolean visible;
    private Category category;

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "keyword")
    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    @Column(name = "download_url")
    public String getDownloadUrl() {
        return downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }

    @Column(name = "upload_url")
    public String getUploadUrl() {
        return uploadUrl;
    }

    public void setUploadUrl(String uploadUrl) {
        this.uploadUrl = uploadUrl;
    }

    @Column(name = "visible")
    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    @ManyToOne(cascade = { CascadeType.ALL })
    @JoinColumn(name ="category_id")
    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Resource{");
        sb.append("name='").append(name).append('\'');
        sb.append(", description='").append(description).append('\'');
        sb.append(", keyword='").append(keyword).append('\'');
        sb.append(", downloadUrl='").append(downloadUrl).append('\'');
        sb.append(", uploadUrl='").append(uploadUrl).append('\'');
        sb.append(", visible=").append(visible);
        sb.append(", category=").append(category);
        sb.append('}');
        return sb.toString();
    }
}
