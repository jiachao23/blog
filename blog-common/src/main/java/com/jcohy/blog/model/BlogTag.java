package com.jcohy.blog.model;

import javax.persistence.*;

/**
 * Created by jiac on 2018/6/28.
 * ClassName  : com.jcohy.model
 * Description  :
 */
@Entity
@Table(name = "jcohy_blog_tag")
@IdClass(BlogTagPK.class)
public class BlogTag {

    private long blogId;
    private long tagId;

    @Column(name = "blog_id", nullable = false)
    @Id
    public long getBlogId() {
        return blogId;
    }

    public void setBlogId(long blogId) {
        this.blogId = blogId;
    }

    @Column(name = "tag_id" ,nullable = false)
    public long getTagId() {
        return tagId;
    }

    public void setTagId(long tagId) {
        this.tagId = tagId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BlogTag that = (BlogTag) o;

        if (blogId != that.blogId) return false;
        if (tagId != that.tagId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (blogId ^ (blogId >>> 32));
        result = 31 * result + (int) (tagId ^ (tagId >>> 32));
        return result;
    }

}
