package com.jcohy.blog.model;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Copyright  : 2015-2033 Beijing Startimes Communication & Network Technology Co.Ltd
 * Created by jiac on 2018/1/4 10:56.
 * ClassName  : TimeLine
 * Description  :
 */

@Entity
@Table(name = "jcohy_timeline")
public class TimeLine extends AbstractModel{


    private String displayName;


    private String displayDate;

    @Column(name = "display_name")
    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    @Column(name = "display_date")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    public String getDisplayDate() {
        return displayDate;
    }

    public void setDisplayDate(String displayDate) {
        this.displayDate = displayDate;
    }
}
