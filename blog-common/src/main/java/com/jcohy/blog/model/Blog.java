package com.jcohy.blog.model;

import com.jcohy.blog.dto.BlogDto;
import org.springframework.beans.BeanUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by jiac on 2018/6/28.
 * ClassName  : com.jcohy.model
 * Description  :
 */

@Entity
@Table(name = "jcohy_blog")
public class Blog extends AbstractModel implements Serializable {

    private String title;
    private String content;
    private Integer privacy;//权限级别.1 公开 0私密
    private Integer status;
    private String summary;
    private Integer type;
    private Integer isTop;
    private Integer isCommend;
    private Integer commentNum;
    private Integer shareNum;
    private Integer heartNum;
    private Integer readNum;
    private String coverURL;
    private String url;
    private User user;
    private Category category;
    private Date publishTime;

    private Set<Tag> tags = new LinkedHashSet<>();

    @Column(name = "title",length = 500)
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Column(name = "type")
    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    @Lob
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Column(name = "privacy")
    public Integer getPrivacy() {
        return privacy;
    }

    public void setPrivacy(Integer privacy) {
        this.privacy = privacy;
    }

    @Column(name = "status")
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Column(name = "summary")
    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    @ManyToMany
    @JoinTable(
            name = "jcohy_blog_tag",
            joinColumns = {@JoinColumn(name = "blog_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "tag_id", referencedColumnName = "id")})
    public Set<Tag> getTags() {
        return tags;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    @Column(name = "istop")
    public Integer getIsTop() {
        return isTop;
    }

    public void setIsTop(Integer isTop) {
        this.isTop = isTop;
    }

    @Column(name = "iscommend")
    public Integer getIsCommend() {
        return isCommend;
    }

    public void setIsCommend(Integer isCommend) {
        this.isCommend = isCommend;
    }

    @Column(name = "comment_num")
    public Integer getCommentNum() {
        return commentNum;
    }

    public void setCommentNum(Integer commentNum) {
        this.commentNum = commentNum;
    }

    @Column(name = "share_num")
    public Integer getShareNum() {
        return shareNum;
    }

    public void setShareNum(Integer shareNum) {
        this.shareNum = shareNum;
    }

    @Column(name = "heart_num")
    public Integer getHeartNum() {
        return heartNum;
    }

    public void setHeartNum(Integer heartNum) {
        this.heartNum = heartNum;
    }

    @Column(name = "read_num")
    public Integer getReadNum() {
        return readNum;
    }

    public void setReadNum(Integer readNum) {
        this.readNum = readNum;
    }

    @Column(name = "coverURL")
    public String getCoverURL() {
        return coverURL;
    }

    public void setCoverURL(String coverURL) {
        this.coverURL = coverURL;
    }

    @Column(name = "url")
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @ManyToOne
    @JoinColumn(name = "user_id")
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @ManyToOne(cascade = { CascadeType.ALL })
    @JoinColumn(name ="category_id")
    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Basic
    @Column(name = "publish_time", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    public Date getPublishTime() {
        return publishTime;
    }

    public void setPublishTime(Date publishTime) {
        this.publishTime = publishTime;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Blog{");
        sb.append("title='").append(title).append('\'');
        sb.append(", content='").append(content).append('\'');
        sb.append(", privacy=").append(privacy);
        sb.append(", status=").append(status);
        sb.append(", summary='").append(summary).append('\'');
        sb.append(", tags=").append(tags);
        sb.append(", type=").append(type);
        sb.append(", isTop=").append(isTop);
        sb.append(", isCommend=").append(isCommend);
        sb.append(", commentNum=").append(commentNum);
        sb.append(", shareNum=").append(shareNum);
        sb.append(", heartNum=").append(heartNum);
        sb.append(", readNum=").append(readNum);
        sb.append(", coverURL='").append(coverURL).append('\'');
        sb.append(", url='").append(url).append('\'');
        sb.append(", user=").append(user);
        sb.append(", category=").append(category);
        sb.append(", publishTime=").append(publishTime);
        sb.append('}');
        return sb.toString();
    }

    public static BlogDto of(Blog blog){
        BlogDto blogDto = new BlogDto();
        BeanUtils.copyProperties(blog,blogDto);
        if(blog.getUser() != null){
            blogDto.setUsername(blog.getUser().getUsername());
        }
        blogDto.setUsername(blog.getUser().getUsername());
        if(blog.getCategory() != null){
            blogDto.setCategoryName(blog.getCategory().getName());
        }
        StringBuilder sb = new StringBuilder();
        Set<Tag> tags = blog.getTags();
        List<String> collect=tags.stream().map(Tag::getName).collect(Collectors.toList());
        String tags1 = String.join(",", collect.toString());
        blogDto.setTags(tags1);
        return blogDto;
    }
}
