package com.jcohy.blog.model;

import javax.persistence.*;

/**
 * Created by jiac on 2018/6/28.
 * ClassName  : com.jcohy.model
 * Description  :
 */
@Entity
@Table(name = "jcohy_role_permission")
@IdClass(RolePermissionPK.class)
public class RolePermission {
    private long roleId;
    private long permissionId;

    @Id
    @Column(name = "role_id", nullable = false)
    public long getRoleId() {
        return roleId;
    }

    public void setRoleId(long roleId) {
        this.roleId = roleId;
    }

    @Id
    @Column(name = "permission_id", nullable = false)
    public long getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(long permissionId) {
        this.permissionId = permissionId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RolePermission that = (RolePermission) o;

        if (roleId != that.roleId) return false;
        if (permissionId != that.permissionId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (roleId ^ (roleId >>> 32));
        result = 31 * result + (int) (permissionId ^ (permissionId >>> 32));
        return result;
    }
}
