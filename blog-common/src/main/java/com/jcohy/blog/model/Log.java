package com.jcohy.blog.model;

import javax.persistence.*;

/**
 * Created by xufubiao on 2017/8/5.
 */
@Entity
@Table(name = "jcohy_log")
public class Log {
    private long id;
    private String module;          //功能模块
    private String description;     //操作的功能
    private Long userId;
    private String username;
    private Long startTime;
    private Integer spendTime;
    private String basePath;
    private String url;
    private String method;
    private String parameter;
    private String httpStatusCode;
    private String ip;
    private String result;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "description", nullable = true, length = 100)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "user_id")
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "username", nullable = true, length = 20)
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Basic
    @Column(name = "start_time", nullable = true)
    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    @Basic
    @Column(name = "spend_time", nullable = true)
    public Integer getSpendTime() {
        return spendTime;
    }

    public void setSpendTime(Integer spendTime) {
        this.spendTime = spendTime;
    }

    @Basic
    @Column(name = "base_path", nullable = true, length = 100)
    public String getBasePath() {
        return basePath;
    }

    public void setBasePath(String basePath) {
        this.basePath = basePath;
    }

    @Basic
    @Column(name = "url", nullable = true, length = 200)
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Basic
    @Column(name = "method", nullable = true, length = 10)
    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    @Basic
    @Column(name = "parameter", nullable = true, length = -1)
    public String getParameter() {
        return parameter;
    }

    public void setParameter(String parameter) {
        this.parameter = parameter;
    }


    @Basic
    @Column(name = "http_status_code")
    public String getHttpStatusCode() {
        return httpStatusCode;
    }

    public void setHttpStatusCode(String httpStatusCode) {
        this.httpStatusCode = httpStatusCode;
    }

    @Basic
    @Column(name = "ip", nullable = true, length = 30)
    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    @Basic
    @Column(name = "result", nullable = true, length = -1)
    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    @Basic
    @Column(name = "module", nullable = true, length = 100)
    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Log log = (Log) o;

        if (id != log.id) return false;
        if (description != null ? !description.equals(log.description) : log.description != null) return false;
        if (username != null ? !username.equals(log.username) : log.username != null) return false;
        if (startTime != null ? !startTime.equals(log.startTime) : log.startTime != null) return false;
        if (spendTime != null ? !spendTime.equals(log.spendTime) : log.spendTime != null) return false;
        if (basePath != null ? !basePath.equals(log.basePath) : log.basePath != null) return false;
        if (url != null ? !url.equals(log.url) : log.url != null) return false;
        if (method != null ? !method.equals(log.method) : log.method != null) return false;
        if (parameter != null ? !parameter.equals(log.parameter) : log.parameter != null) return false;
        if (ip != null ? !ip.equals(log.ip) : log.ip != null) return false;
        if (result != null ? !result.equals(log.result) : log.result != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result1 = (int) (id ^ (id >>> 32));
        result1 = 31 * result1 + (description != null ? description.hashCode() : 0);
        result1 = 31 * result1 + (username != null ? username.hashCode() : 0);
        result1 = 31 * result1 + (startTime != null ? startTime.hashCode() : 0);
        result1 = 31 * result1 + (spendTime != null ? spendTime.hashCode() : 0);
        result1 = 31 * result1 + (basePath != null ? basePath.hashCode() : 0);
        result1 = 31 * result1 + (url != null ? url.hashCode() : 0);
        result1 = 31 * result1 + (method != null ? method.hashCode() : 0);
        result1 = 31 * result1 + (parameter != null ? parameter.hashCode() : 0);
        result1 = 31 * result1 + (ip != null ? ip.hashCode() : 0);
        result1 = 31 * result1 + (result != null ? result.hashCode() : 0);
        return result1;
    }
}
