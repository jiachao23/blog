package com.jcohy.blog.model;

import javax.persistence.*;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by jiac on 2018/9/28.
 * ClassName  : com.jcohy.blog.model
 * Description  :
 */
@Entity
@Table(name = "jcohy_permission")
public class Permission extends AbstractModel {
    private Integer systemId;
    private Long parentId;
    private String name;
    private String enName;
    private Byte type;              //0:目录,1:菜单,2:功能
    private String permissionValue;
    private String url;
    private String method;          //方法类型：GET,POST,PUT,DELETE
    private String icon;
    private Byte status;
    private Long orders;
    private Boolean isSys = false;

    private Set<Menu> menus = new LinkedHashSet<>();
    private Set<Permission> children = new LinkedHashSet<>();

    public Permission() {
    }

    public Permission(String name, String permissionValue) {
        this.name = name;
        this.permissionValue = permissionValue;
    }

    @Basic
    @Column(name = "system_id", nullable = true)
    public Integer getSystemId() {
        return systemId;
    }

    public void setSystemId(Integer systemId) {
        this.systemId = systemId;
    }

    @Basic
    @Column(name = "parent_id", nullable = true)
    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    @Basic
    @Column(name = "name", nullable = true, length = 20)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "en_name", nullable = true, length = 100)
    public String getEnName() {
        return enName;
    }

    public void setEnName(String enName) {
        this.enName = enName;
    }

    @Basic
    @Column(name = "type", nullable = true)
    public Byte getType() {
        return type;
    }

    public void setType(Byte type) {
        this.type = type;
    }

    @Basic
    @Column(name = "permission_value", nullable = true, length = 50)
    public String getPermissionValue() {
        return permissionValue;
    }

    public void setPermissionValue(String permissionValue) {
        this.permissionValue = permissionValue;
    }

    @Basic
    @Column(name = "url", nullable = true, length = 100)
    public String getUrl() {
        return url;
    }

    public void setUrl(String uri) {
        this.url = uri;
    }

    @Basic
    @Column(name = "method", nullable = true, length = 100)
    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    @Basic
    @Column(name = "icon", nullable = true, length = 50)
    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    @Basic
    @Column(name = "status", nullable = true)
    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    @Basic
    @Column(name = "orders", nullable = true)
    public Long getOrders() {
        return orders;
    }

    public void setOrders(Long orders) {
        this.orders = orders;
    }

    @Basic
    @Column(name = "is_sys", nullable = true)
    public Boolean getIsSys() {
        return isSys;
    }

    public void setIsSys(Boolean sys) {
        isSys = sys;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "jcohy_menu_permission",
            joinColumns = {@JoinColumn(name = "permission_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "menu_id", referencedColumnName = "id")})
    public Set<Menu> getMenus() {
        return menus;
    }

    public void setMenus(Set<Menu> menus) {
        this.menus = menus;
    }

    @Transient
    public Set<Permission> getChildren() {
        return children;
    }

    public void setChildren(Set<Permission> children) {
        this.children = children;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Permission that = (Permission) o;

        if (parentId != null ? !parentId.equals(that.parentId) : that.parentId != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (type != null ? !type.equals(that.type) : that.type != null) return false;
        if (permissionValue != null ? !permissionValue.equals(that.permissionValue) : that.permissionValue != null)
            return false;
        return url != null ? url.equals(that.url) : that.url == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (parentId != null ? parentId.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (permissionValue != null ? permissionValue.hashCode() : 0);
        result = 31 * result + (url != null ? url.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Permission{");
        sb.append("systemId=").append(systemId);
        sb.append(", parentId=").append(parentId);
        sb.append(", name='").append(name).append('\'');
        sb.append(", type=").append(type);
        sb.append(", permissionValue='").append(permissionValue).append('\'');
        sb.append(", url='").append(url).append('\'');
        sb.append(", icon='").append(icon).append('\'');
        sb.append(", status=").append(status);
        sb.append(", orders=").append(orders);
        sb.append(", menus=").append(menus);
        sb.append('}');
        return sb.toString();
    }
}

