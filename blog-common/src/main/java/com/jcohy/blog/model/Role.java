package com.jcohy.blog.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.LinkedHashSet;
import java.util.Set;

import static java.util.stream.Collectors.toSet;

/**
 * Created by jiac on 2018/9/28.
 * ClassName  : com.jcohy.blog.model
 * Description  :
 */
@Entity
@Table(name = "jcohy_role")
public class Role extends AbstractModel {
    private Boolean state = true;
    private String name;            //角色名称
    private String code;            //以ROLE_为前缀的英文字符串
    private String description;
    private Boolean isSys = false;
    private Integer authorityModel; //'1:仅本人，2:本部门，3:本部门及其下属部门，4:本机构及其下属部门，5:所有数据'

    private Set<Permission> permissions = new LinkedHashSet<>();
    private Set<User> users = new LinkedHashSet<>();

    @Basic
    @Column(name = "state", nullable = false)
    public Boolean getState() {
        return state;
    }

    public void setState(Boolean state) {
        this.state = state;
    }

    @Basic
    @Column(name = "name", nullable = true, length = 50)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "code", nullable = false, length = 50)
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Basic
    @Column(name = "description", nullable = true, length = 255)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "is_sys", nullable = true)
    public Boolean getIsSys() {
        return isSys;
    }

    public void setIsSys(Boolean sys) {
        isSys = sys;
    }

    @Basic
    @Column(name = "authority_model", nullable = false)
    public Integer getAuthorityModel() {
        return authorityModel;
    }

    public void setAuthorityModel(Integer authorityModel) {
        this.authorityModel = authorityModel;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "jcohy_role_permission",
            joinColumns = {@JoinColumn(name = "role_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "permission_id", referencedColumnName = "id")})
    @JsonIgnore
    public Set<Permission> getPermissions() {
        return permissions;
    }

    public void setPermissions(Set<Permission> permissions) {
        this.permissions = permissions;
    }

    @ManyToMany(mappedBy = "roles", fetch = FetchType.LAZY)
    @JsonIgnore
    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    @Transient
    @JsonIgnore
    public Set<String> getStringPermissions() {
        if (this.permissions != null) {
            return this.permissions.stream().map(role -> role.getName()).collect(toSet());
        } else {
            return null;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Role role = (Role) o;

        if (state != null ? !state.equals(role.state) : role.state != null) return false;
        if (name != null ? !name.equals(role.name) : role.name != null) return false;
        return code != null ? code.equals(role.code) : role.code == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (state != null ? state.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (code != null ? code.hashCode() : 0);
        return result;
    }
}
