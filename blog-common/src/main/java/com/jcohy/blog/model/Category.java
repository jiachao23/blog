package com.jcohy.blog.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.Set;


/**
 * Copyright  : 2017- www.jcohy.com
 * Created by jiac on 2:06 2017/12/16
 * Email: jia_chao23@126.com
 * ClassName: Category
 * Description:
 **/

@Entity
@Table(name = "jcohy_category")
public class Category extends AbstractModel implements Serializable {

    private String name;
    private String enName;
    private Long parentId;
    private Integer type;
    private Integer level;
    private String target;
    private Long orders;
    private Integer count;
    private boolean visible;

    private Set<Category> children = new LinkedHashSet<Category>();

    @Basic
    @Column(name = "name", nullable = true, length = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Basic
    @Column(name = "en_name", nullable = true, length = 100)
    public String getEnName() {
        return enName;
    }

    public void setEnName(String enName) {
        this.enName = enName;
    }


    @Basic
    @Column(name = "parent_id", nullable = true)
    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    @Basic
    @Column(name = "type", nullable = true)
    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    @Basic
    @Column(name = "level", nullable = true)
    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    @Basic
    @Column(name = "target", nullable = true, length = 10)
    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    @Basic
    @Column(name = "orders", nullable = true)
    public Long getOrders() {
        return orders;
    }

    public void setOrders(Long orders) {
        this.orders = orders;
    }

    @Basic
    @Column(name = "count", nullable = true)
    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    @Basic
    @Column(name = "visible")
    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    @Transient
    public Set<Category> getChildren() {
        return children;
    }

    public void setChildren(Set<Category> children) {
        this.children = children;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Category{");
        sb.append("name='").append(name).append('\'');
        sb.append(", enName='").append(enName).append('\'');
        sb.append(", parentId=").append(parentId);
        sb.append(", type=").append(type);
        sb.append(", level=").append(level);
        sb.append(", target='").append(target).append('\'');
        sb.append(", orders=").append(orders);
        sb.append(", count=").append(count);
        sb.append(", visible=").append(visible);
        sb.append(", children=").append(children);
        sb.append('}');
        return sb.toString();
    }
}