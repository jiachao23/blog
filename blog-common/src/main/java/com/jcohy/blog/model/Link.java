package com.jcohy.blog.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Copyright  : 2017- www.jcohy.com
 * Created by jiac on 2:17 2017/12/16
 * Email: jia_chao23@126.com
 * ClassName: Link
 * Description:
 **/

@Entity
@Table(name = "jcohy_link")
public class Link extends AbstractModel implements Serializable {

    private String title;
    private String url;
    private String description;
    /**
     * 0 可见 1 隐藏
     */
    private Integer status;
    private String email;
    private String logo;

    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Column(name = "title")
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    @Column(name = "url",length = 500)
    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
    }

    @Column(name = "description")
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    @Column(nullable = false, columnDefinition = "Integer default 0")
    public Integer getStatus() {
        return status;
    }
    public void setStatus(Integer status) {
        this.status = status;
    }

    @Column(name = "logo")
    public String getLogo() {
        return logo;
    }
    public void setLogo(String logo) {
        this.logo = logo;
    }
}
