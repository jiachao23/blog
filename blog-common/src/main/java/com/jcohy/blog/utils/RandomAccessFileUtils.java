package com.jcohy.blog.utils;

import com.jcohy.blog.dto.ReadFileResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.RandomAccessFile;

public class RandomAccessFileUtils {

    private static final Logger logger = LoggerFactory.getLogger(RandomAccessFileUtils.class);

    public static ReadFileResult readFromFile(String filePath, Long position) {

        ReadFileResult result = new ReadFileResult();
        RandomAccessFile file = null;

        try {
            file = new RandomAccessFile(filePath, "r");
            Long fileLength = file.length();

            if (position >= fileLength) {       // 要读取的位置超出了文件的大小
                result.setPosition(fileLength);
                result.setContent(null);
            } else {
                file.seek(position);
                int size = (int)(fileLength - position);
                byte[] bytes = new byte[size];

                file.read(bytes);

                result.setPosition(fileLength);
                result.setContent(new String(bytes));
            }
        } catch (Exception e) {
            logger.error("execute shell error: ", e);
        } finally {
            if (file != null) {
                try {
                    file.close();
                } catch (IOException e) {
                    logger.error("close file error: ", e);
                }
            }
        }

        return result;
    }
}
