package com.jcohy.blog.cache;

/**
 * Created by jiac on 2018/8/14.
 * ClassName  : com.star.uums
 * Description  :
 */
public interface CacheService {

    void put(String key, Object object, long timeout);

//    void put2DB(String key, Object object,long timeout);

    void put(String key, String value, long timeout);

//    void put2DB(String key, String value, long timeout);

    <T> T get(String key, Class<T> clazz);

    String get(String key);

    void delete(String key);

    int getKeysCount(String pattern);

    long getExpire(String key);
}
