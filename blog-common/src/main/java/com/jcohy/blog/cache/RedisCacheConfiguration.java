package com.jcohy.blog.cache;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import redis.clients.jedis.JedisPoolConfig;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by jiac on 2018/8/14.
 * ClassName  : com.star.uums.configuration
 * Description  :
 */
@Configuration
@EnableCaching
public class RedisCacheConfiguration extends CachingConfigurerSupport {

    @Autowired
    private RedisPoolProperties redisPoolProperties;


    /**
     * 生成key的策略
     * 此方法将会根据类名+方法名+所有参数的值生成唯一的一个key,即使@Cacheable中的value属性一样，key也会不一样
     * @return
     */
    @Bean
    @Override
    public KeyGenerator keyGenerator() {

        return new KeyGenerator() {
            @Override
            public Object generate(Object target, Method method, Object... params) {
                StringBuilder sb = new StringBuilder();
                sb.append(target.getClass().getName());
                sb.append(":" + method.getName() + ":");
                for (Object obj : params) {
                    sb.append(obj.toString());
                }
                return sb.toString();
            }
        };
    }

    @Bean
    public CacheManager cacheManager(RedisTemplate<?,?> redisTemplate) {
        RedisCacheManager redisCacheManager = new RedisCacheManager(redisTemplate);
        redisCacheManager.setUsePrefix(true);

        Map<String, Long> expires = new HashMap<>();
        expires.put("minCache", 1L*60*60);        // 设置过期时间 key is cache-name
        expires.put("maxCache", 2L*60*60);

        redisCacheManager.setExpires(expires);

        redisCacheManager.setDefaultExpiration(1*60*60);    // 默认过期时间：1 hours

        return redisCacheManager;
    }

    @Bean(name = "redisConnectionFactory")
    JedisConnectionFactory jedisConnectionFactory() {
        JedisPoolConfig poolConfig = new JedisPoolConfig();
        poolConfig.setMaxIdle(redisPoolProperties.getMaxIdel());
        poolConfig.setMinIdle(redisPoolProperties.getMinIdel());
        poolConfig.setMaxTotal(redisPoolProperties.getMaxActive());
        poolConfig.setMaxWaitMillis(redisPoolProperties.getMaxWait());

        JedisConnectionFactory factory = new JedisConnectionFactory();
        factory.setHostName(redisPoolProperties.getRedisHost());
        factory.setPort(redisPoolProperties.getRedisPort());
        factory.setDatabase(redisPoolProperties.getRedisDBIndex());
        factory.setUsePool(true);
        factory.setPoolConfig(poolConfig);
        return factory;
    }

    @Bean(name = "redisTemplate")
    public RedisTemplate<String, String> redisTemplate() {
        RedisTemplate<String, String> template = new RedisTemplate<>();

        RedisSerializer stringSerializer = new StringRedisSerializer();
        template.setConnectionFactory(jedisConnectionFactory());

        template.setKeySerializer(stringSerializer);
        template.setHashKeySerializer(stringSerializer);
        template.setValueSerializer(new GenericJackson2JsonRedisSerializer());
        return template;
    }
}
