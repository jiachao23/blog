package com.jcohy.blog.cache;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

/**
 * Created by jiac on 2018/8/14.
 * ClassName  : com.star.uums.cache
 * Description  :
 */
@Service
public class CacheServiceImpl implements CacheService{

    private Logger logger = LoggerFactory.getLogger(CacheServiceImpl.class);

    private ObjectMapper mapper = new ObjectMapper();

    @Resource(name = "redisTemplate")
    private RedisTemplate<String, String> redisTemplate;

//    @Resource(name = "db2RedisTemplate")
//    private RedisTemplate<String, String> db2RedisTemplate;

    @Override
    public void put(String key, Object object, long timeout) {
        try {
            String value = this.toJSON(object);
            redisTemplate.opsForValue().set(key, value, timeout, TimeUnit.SECONDS);
        } catch (Exception e) {
            logger.error("|Cache|put to cache error ", e);
        }
    }

//    @Override
//    public void put2DB(String key, Object object, long timeout) {
//
//    }

    @Override
    public void put(String key, String value, long timeout) {
        redisTemplate.opsForValue().set(key, value, timeout, TimeUnit.SECONDS);
    }

//    @Override
//    public void put2DB(String key, String value, long timeout) {
//
//    }

    @Override
    public <T> T get(String key, Class<T> clazz) {
        try {
            String json = redisTemplate.opsForValue().get(key);
            return this.fromJSON(json, clazz);
        } catch (Exception e) {
            logger.error("|Cache|get from cache error ", e);
        }
        return null;
    }

    @Override
    public String get(String key) {
        return redisTemplate.opsForValue().get(key);
    }

    @Override
    public void delete(String key) {
        redisTemplate.delete(key);
    }

    @Override
    public int getKeysCount(String pattern) {
        return redisTemplate.keys(pattern).size();
    }

    @Override
    public long getExpire(String key) {
        return redisTemplate.getExpire(key);
    }

    private  <T> T fromJSON(String json, Class<T> clazz) {
        if (json == null || clazz == null) {
            return null;
        }
        try {
            return mapper.readValue(json, clazz);
        } catch (Exception e) {
            logger.error("JSON to Object[" + clazz + "] error. json:" + json, e);
            return null;
        }
    }

    private String toJSON(Object object){
        if(object == null){
            return null;
        }
        try {
           return mapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            logger.error("Object to String error.", e);
            return null;
        }
    }
}
