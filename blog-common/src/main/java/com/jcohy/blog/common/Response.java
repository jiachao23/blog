package com.jcohy.blog.common;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * Created by jiac on 2018/7/17.
 * ClassName  : com.jcohy.common
 * Description  :
 */
@ApiModel(value = "接口响应对象")
public class Response<T> implements Serializable {
    private static final long serialVersionUID = -4752002035039882415L;

    @ApiModelProperty(value = "编码", required = true)
    @JsonProperty("code")
    private int code = 99;
    @ApiModelProperty(value = "错误消息")
    @JsonProperty("message")
    private String message = null;

    @ApiModelProperty(value = "数据")
    @JsonProperty("data")
    private T data;

    @ApiModelProperty(value = "数据总条数")
    @JsonProperty("total")
    private Long total;

    @ApiModelProperty(value = "本次返回的第一条数据序号")
    @JsonProperty("start")
    private Integer start;

    @ApiModelProperty(value = "本次返回记录条数")
    @JsonProperty("size")
    private Integer size;

    @ApiModelProperty(value = "排序字段")
    @JsonProperty("sort")
    private String sort;

    @ApiModelProperty(value = "排序方式:asc or desc")
    @JsonProperty("order")
    private String order;

    public Response() {
    }

    public Response(int code) {
        this.code = code;
    }

    public Response(int code, String message) {
        this.code = code;
        this.message = message;
    }

    /**
     * 异常编码
     *
     * @return code
     **/
    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Response resultStatus(int resultStatus) {
        this.code = resultStatus;
        return this;
    }

    /**
     * 异常消息
     *
     * @return message
     **/
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Response message(String message) {
        this.message = message;
        return this;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public Integer getStart() {
        return start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    @Override
    public String toString() {
        return "Response{" +
                "code=" + code +
                ", message='" + message + '\'' +
                ", data=" + data +
                ", total=" + total +
                ", start=" + start +
                ", size=" + size +
                ", sort='" + sort + '\'' +
                ", order='" + order + '\'' +
                '}';
    }

    public String toStringMini() {
        final StringBuilder sb = new StringBuilder("Response{");
        sb.append("code=").append(code);
        sb.append(", message='").append(message).append('\'');
        sb.append('}');
        return sb.toString();
    }


    /**
     * BeanConvert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private static String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
