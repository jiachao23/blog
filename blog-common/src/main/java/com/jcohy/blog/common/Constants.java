package com.jcohy.blog.common;


import com.jcohy.blog.model.Permission;

/**
 * Copyright  : 2017- www.jcohy.com
 * Created by jiac on 2:06 2017/12/16
 * Email: jia_chao23@126.com
 * ClassName: Constants
 * Description:
 **/
public interface Constants {

	public static final Permission share = new Permission("平台登录用户共享功能", "jcohy:share");
	public static int DEFAULT_PAGE_SIZE = 10;
	public static String ASC = "asc";
	public static String DESC = "desc";


}
