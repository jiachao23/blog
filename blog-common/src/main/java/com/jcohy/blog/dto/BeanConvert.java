package com.jcohy.blog.dto;

/**
 * Created by jiac on 2018/9/7.
 * ClassName  : com.jcohy.dto
 * Description  :
 */
public interface BeanConvert<T,R> {
    R convert(T t);
}
