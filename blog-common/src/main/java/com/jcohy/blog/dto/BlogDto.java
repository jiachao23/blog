package com.jcohy.blog.dto;

import java.util.Date;

/**
 * Created by jiac on 2018/7/18.
 * ClassName  : com.jcohy.dto
 * Description  :
 */
public class BlogDto {
    protected Long id;
    private String title;
    private String content;
    private Integer privacy;//权限级别.1 公开 0私密
    private Integer status;
    private String summary;
    private Integer type;
    private Integer isTop;
    private Integer isCommend;
    private Integer commentNum;
    private Integer shareNum;
    private Integer heartNum;
    private Integer readNum;
    private String coverURL;
    private String url;
    private String username;
    private String  categoryName;
    private Date publishTime;
    private String tags;
    protected Date createdTime;
    protected Date updatedTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getPrivacy() {
        return privacy;
    }

    public void setPrivacy(Integer privacy) {
        this.privacy = privacy;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getIsTop() {
        return isTop;
    }

    public void setIsTop(Integer isTop) {
        this.isTop = isTop;
    }

    public Integer getIsCommend() {
        return isCommend;
    }

    public void setIsCommend(Integer isCommend) {
        this.isCommend = isCommend;
    }

    public Integer getCommentNum() {
        return commentNum;
    }

    public void setCommentNum(Integer commentNum) {
        this.commentNum = commentNum;
    }

    public Integer getShareNum() {
        return shareNum;
    }

    public void setShareNum(Integer shareNum) {
        this.shareNum = shareNum;
    }

    public Integer getHeartNum() {
        return heartNum;
    }

    public void setHeartNum(Integer heartNum) {
        this.heartNum = heartNum;
    }

    public Integer getReadNum() {
        return readNum;
    }

    public void setReadNum(Integer readNum) {
        this.readNum = readNum;
    }

    public String getCoverURL() {
        return coverURL;
    }

    public void setCoverURL(String coverURL) {
        this.coverURL = coverURL;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Date getPublishTime() {
        return publishTime;
    }

    public void setPublishTime(Date publishTime) {
        this.publishTime = publishTime;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public Date getCreatedTime() {



        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Date getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BlogDto blogDto = (BlogDto) o;

        if (title != null ? !title.equals(blogDto.title) : blogDto.title != null) return false;
        if (content != null ? !content.equals(blogDto.content) : blogDto.content != null) return false;
        if (id != null ? !id.equals(blogDto.id) : blogDto.id != null) return false;
        if (privacy != null ? !privacy.equals(blogDto.privacy) : blogDto.privacy != null) return false;
        if (status != null ? !status.equals(blogDto.status) : blogDto.status != null) return false;
        if (summary != null ? !summary.equals(blogDto.summary) : blogDto.summary != null) return false;
        if (type != null ? !type.equals(blogDto.type) : blogDto.type != null) return false;
        if (isTop != null ? !isTop.equals(blogDto.isTop) : blogDto.isTop != null) return false;
        if (isCommend != null ? !isCommend.equals(blogDto.isCommend) : blogDto.isCommend != null) return false;
        if (commentNum != null ? !commentNum.equals(blogDto.commentNum) : blogDto.commentNum != null) return false;
        if (shareNum != null ? !shareNum.equals(blogDto.shareNum) : blogDto.shareNum != null) return false;
        if (heartNum != null ? !heartNum.equals(blogDto.heartNum) : blogDto.heartNum != null) return false;
        if (readNum != null ? !readNum.equals(blogDto.readNum) : blogDto.readNum != null) return false;
        if (coverURL != null ? !coverURL.equals(blogDto.coverURL) : blogDto.coverURL != null) return false;
        if (url != null ? !url.equals(blogDto.url) : blogDto.url != null) return false;
        if (username != null ? !username.equals(blogDto.username) : blogDto.username != null) return false;
        if (categoryName != null ? !categoryName.equals(blogDto.categoryName) : blogDto.categoryName != null)
            return false;
        if (publishTime != null ? !publishTime.equals(blogDto.publishTime) : blogDto.publishTime != null) return false;
        if (tags != null ? !tags.equals(blogDto.tags) : blogDto.tags != null) return false;
        if (createdTime != null ? !createdTime.equals(blogDto.createdTime) : blogDto.createdTime != null) return false;
        return updatedTime != null ? updatedTime.equals(blogDto.updatedTime) : blogDto.updatedTime == null;
    }

    @Override
    public int hashCode() {
        int result = title != null ? title.hashCode() : 0;
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + (content != null ? content.hashCode() : 0);
        result = 31 * result + (privacy != null ? privacy.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (summary != null ? summary.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (isTop != null ? isTop.hashCode() : 0);
        result = 31 * result + (isCommend != null ? isCommend.hashCode() : 0);
        result = 31 * result + (commentNum != null ? commentNum.hashCode() : 0);
        result = 31 * result + (shareNum != null ? shareNum.hashCode() : 0);
        result = 31 * result + (heartNum != null ? heartNum.hashCode() : 0);
        result = 31 * result + (readNum != null ? readNum.hashCode() : 0);
        result = 31 * result + (coverURL != null ? coverURL.hashCode() : 0);
        result = 31 * result + (url != null ? url.hashCode() : 0);
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + (categoryName != null ? categoryName.hashCode() : 0);
        result = 31 * result + (publishTime != null ? publishTime.hashCode() : 0);
        result = 31 * result + (tags != null ? tags.hashCode() : 0);
        result = 31 * result + (createdTime != null ? createdTime.hashCode() : 0);
        result = 31 * result + (updatedTime != null ? updatedTime.hashCode() : 0);
        return result;
    }
}
