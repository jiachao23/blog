package com.jcohy.blog.dto;

public class ReadFileResult {
    private Long position;
    private String content;

    public Long getPosition() {
        return position;
    }

    public void setPosition(Long position) {
        this.position = position;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
