package com.jcohy.blog.aspect;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jcohy.blog.annotation.ConsoleLog;
import com.jcohy.blog.model.Log;
import com.jcohy.blog.security.JwtAuthenticationRequest;
import com.jcohy.blog.security.JwtTokenUtil;
import com.jcohy.blog.service.ConsoleLogService;
import com.jcohy.blog.utils.IpKit;
import com.jcohy.blog.utils.PlaceholderParser;
import com.jcohy.blog.utils.Response;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.Map;

/**
 * Created by jiac on 2018/9/28.
 * ClassName  : com.jcohy.blog.aspect
 * Description  :
 */
@Aspect
@Component
public class LogAspect {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private ConsoleLogService consoleLogService;

    @Pointcut("@annotation(com.jcohy.blog.annotation.ConsoleLog)")
    public void annotationPointCut() {
    }

    @Around("annotationPointCut()")
    public Object around(ProceedingJoinPoint pjp) throws Throwable {
        // 拦截的方法参数
        Object[] args = pjp.getArgs();
        Log log = new Log();

        // 拦截的方法名称。当前正在执行的方法
        String methodName = pjp.getSignature().getName();

        // 拦截的放参数类型
        Signature sig = pjp.getSignature();

        MethodSignature msig = null;
        if (!(sig instanceof MethodSignature)) {
            throw new IllegalArgumentException("该注解只能用于方法");
        }

        msig = (MethodSignature) sig;
        Class[] parameterTypes = msig.getMethod().getParameterTypes();

        // 拦截的实体类，就是当前正在执行的controller
        Object target = pjp.getTarget();
        Object object = null;
        // 获得被拦截的方法
        Method method = null;
        try {
            method = target.getClass().getMethod(methodName, parameterTypes);
        } catch (NoSuchMethodException e1) {
            logger.error("", e1);
        } catch (SecurityException e1) {
            logger.error("", e1);
        }

        ConsoleLog consoleLogAnnotation = method.getAnnotation(ConsoleLog.class);
        log.setModule(consoleLogAnnotation.module());
        log.setDescription(consoleLogAnnotation.description());
        fillLog(args, log);

        //方法通知前获取时间
        long start = System.currentTimeMillis();
        //获取系统时间
        log.setStartTime(start);
        try {
            object = pjp.proceed();
            long end = System.currentTimeMillis();

            if(object instanceof Response) {
                Response response = (Response) object;
                if(response.get("code")!=null) {
                    log.setResult(String.valueOf(response.get("code")));
                }
            }
            //方法执行时间
            log.setSpendTime(Integer.valueOf((end - start) + ""));
            //保存进数据库
            consoleLogService.saveConsoleLog(log);
        } catch (Throwable e) {
            long end = System.currentTimeMillis();
            log.setSpendTime(Integer.valueOf((end - start) + ""));
        }
        return object;
    }

    private void fillLog(Object[] args, Log log) throws JsonProcessingException {

        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);

        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();

        //设置客户端ip
        log.setIp(IpKit.getClientIp(request));
        //请求路径
        String url = request.getRequestURI();
        //设置请求地址
        log.setUrl(url);
        Map<String, String[]> paramMap = request.getParameterMap();
        //获取登录用户账户信息
        String token = request.getHeader("Authorization");
        if (StringUtils.isNotBlank(token)) {
            Long userId = jwtTokenUtil.getUserIdFromToken(token);
            String username = jwtTokenUtil.getUsernameFromToken(token);
            log.setUserId(userId);
            log.setUsername(username);
        } else {
            if ("/auth".equals(log.getUrl())) {
                for (Object obj : args) {
                    if (obj instanceof JwtAuthenticationRequest) {
                        JwtAuthenticationRequest req = (JwtAuthenticationRequest) obj;

                        log.setUsername(req.getUsername());
                        break;
                    }
                }
            }
        }

        StringBuilder sb = new StringBuilder("[");
        for (int i = 0; i < args.length; i++) {
            Object obj = args[i];
            if (!(obj instanceof SecurityContextHolderAwareRequestWrapper)) {
                String parm = null;
                try {
                    parm = mapper.writeValueAsString(obj);
                    sb.append(parm);
                    sb.append(",");

                    String desc = PlaceholderParser.parse2(log.getDescription(), parm, i);
                    log.setDescription(desc);
                } catch (Exception e) {
                    logger.error("Parse JSON error ", e);
                }
            }
        }

        String params = mapper.writeValueAsString(paramMap);
        sb.append(params);

        sb.append("]");
        log.setParameter(sb.toString());
    }
}
