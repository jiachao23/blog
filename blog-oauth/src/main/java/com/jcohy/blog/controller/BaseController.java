package com.jcohy.blog.controller;

import com.jcohy.blog.common.Constants;
import com.jcohy.blog.security.JwtTokenUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * Created by jiac on 2018/9/28.
 * ClassName  : com.jcohy.blog.controller
 * Description  :
 */
public class BaseController {

    protected Logger logger = LoggerFactory.getLogger(this.getClass());

    @Value("${jwt.header}")
    protected String tokenHeader;

    @Autowired
    protected JwtTokenUtil jwtTokenUtil;

    @Autowired
    protected UserDetailsService userDetailsService;

    @Autowired
    protected MessageSource messageSource;

    /**
     * 建立分页排序请求
     *
     * @param page
     * @param size
     * @param direction
     * @param sortCol
     * @return
     */
    protected PageRequest buildPageRequest(int page, int size, String sortCol, String direction, String defaultSortCol) {
        if (StringUtils.isNotBlank(direction) && StringUtils.isNotBlank(sortCol)) {
            if (Constants.ASC.equalsIgnoreCase(direction)) {
                Sort sort = new Sort(Sort.Direction.ASC, sortCol);
                return new PageRequest(page, size, sort);
            } else if (Constants.DESC.equalsIgnoreCase(direction)) {
                Sort sort = new Sort(Sort.Direction.DESC, sortCol);
                return new PageRequest(page, size, sort);
            }
        }
        Sort sort = new Sort(Sort.Direction.DESC, defaultSortCol);
        return new PageRequest(page, size, sort);
    }

}
