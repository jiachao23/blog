package com.jcohy.blog.controller;

import com.jcohy.blog.annotation.ConsoleLog;
import com.jcohy.blog.cache.CacheService;
import com.jcohy.blog.enums.CachePrefix;
import com.jcohy.blog.security.JwtAuthenticationRequest;
import com.jcohy.blog.security.JwtUser;
import com.jcohy.blog.utils.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mobile.device.Device;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by jiac on 2018/9/28.
 * ClassName  : com.jcohy.blog.controller
 * Description  :
 */
@RestController
public class SysLoginController extends BaseController{

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private CacheService cacheService;

    @Value("${cache.user.expire_time}")
    private long expiration;

    @ConsoleLog(module = "用户登录",description = "用户登录")
    @PostMapping("${jwt.authentication.path}")
    public Response createAuthenticationToken( JwtAuthenticationRequest authenticationRequest, Device device){
        logger.info("|Login|User request {}", authenticationRequest);
        Response response = new Response();
        Authentication authenticate = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        authenticationRequest.getUsername(),
                        authenticationRequest.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authenticate);

        logger.info("|Login|User authentication={}", authenticate);
        JwtUser user = null;

        // 加载用户信息，生成token
        final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsername());
        final String token = jwtTokenUtil.generateToken(userDetails, device);
        user = (JwtUser) userDetails;

        cacheService.put(CachePrefix.JCOHY_USER_KEY.getKeyfix()+user.getId(),token,expiration);
        response = Response.ok().put("token", token);


        return response;
    }
}
