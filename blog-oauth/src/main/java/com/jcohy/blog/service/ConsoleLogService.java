package com.jcohy.blog.service;

import com.jcohy.blog.dto.LogDto;
import com.jcohy.blog.model.Log;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

/**
 * Created by jiac on 2018/9/28.
 * ClassName  : com.jcohy.blog.ConsoleLogService
 * Description  :
 */
public interface ConsoleLogService {
    /**
     * 保存日志
     *
     * @param log
     */
    void saveConsoleLog(Log log);

    Page<LogDto> queryLog(String username, String module, String description, Long begin, Long end, PageRequest pageRequest);
}
