package com.jcohy.blog.service.impl;

import com.jcohy.blog.model.User;
import com.jcohy.blog.security.JwtUserFactory;
import com.jcohy.blog.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by jiac on 2018/9/28.
 * ClassName  : com.jcohy.blog.service.impl
 * Description  :用户登录--获取用户信息
 */
@Service
public class JwtUserDetailsServiceImpl implements UserDetailsService {
    private Logger logger = LoggerFactory.getLogger(JwtUserDetailsServiceImpl.class);

    @Autowired
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        logger.info("loadUserByUsername: {}", username);
        //0:数据库用户
        User user = userService.getUser(username, 0);

        if (user == null) {
            user = userService.findUserByPhone(username, 0);
        }
        if( user == null ){
            throw new UsernameNotFoundException(String.format("No user found with username '%s'.", username));
        } else {
            List<String> permValues = userService.getAllPermissionValueByUserId(user.getId());
            return JwtUserFactory.create(user, permValues);
        }
    }
}
