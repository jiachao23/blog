package com.jcohy.blog.service.impl;

import com.jcohy.blog.model.User;
import com.jcohy.blog.repository.PermissionRepository;
import com.jcohy.blog.repository.UserRepository;
import com.jcohy.blog.security.JwtUserFactory;
import com.jcohy.blog.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by jiac on 2018/9/28.
 * ClassName  : com.jcohy.blog.service.impl
 * Description  :
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private PermissionRepository permissionRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserById(Long id) {
        User user = userRepository.findUserById(id);

        if (user == null) {
            throw new UsernameNotFoundException(String.format("No user found with Id '%s'.", id));
        } else {
            List<String> permValues = this.getAllPermissionValueByUserId(user.getId());
            return JwtUserFactory.create(user, permValues);
        }
    }

    @Override
    public User getUser(String userName, Integer type) {
        User user = userRepository.findByUsernameAndTypeAndDeleted(userName, type, false);
        return user;
    }

    @Override
    public User findUserByPhone(String username, int type) {
        return userRepository.findUserByPhoneAndTypeAndDeletedIsFalse(username, type);
    }

    @Override
    public List<String> getAllPermissionValueByUserId(Long userId) {
         return permissionRepository.findAllPermissionValueByUserId(userId);
    }

    @Override
    public Page<User> findAll(Pageable pageable) {
        return userRepository.findAll(pageable);
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public void logout(String sessionId) {

    }

    @Override
    public void saveOrUpdate(User user, Long userId) {
        userRepository.save(user);
    }

    @Override
    public boolean checkUser(String name) {
//        findByName(name);
//        return findByName(name) == null;
        return false;
    }

    @Override
    public void register(String name, String password, String email, String ip) {

    }

    @Override
    public void delete(Long id) {
//        if(id == null){
//            throw new ParamErrorException("主键不能为空");
//        }
//        userRepository.delete(id);
    }

    @Override
    public void updatePassword(User user, String oldpassword, String password1, String password2) {
//        if(StringUtils.isBlank(oldpassword) || StringUtils.isBlank(password1) || StringUtils.isBlank(password2)){
//            throw new ParamErrorException("参数不完整");
//        }
//
//        if(!password1.equals(password2)){
//            throw new ParamValueErrorException("两次输入密码不一致");
//        }
//
//        User dbUser = findById(user.getId());
//        String passwordMD5 = MD5Kit.generatePasswordMD5(password1, user.getSalt());
//        if(!user.getPassword().equals(passwordMD5)){
//            throw new ParamErrorException("旧密码不正确");
//        }
//        dbUser.setPassword(passwordMD5);
//        userRepository.saveAndFlush(dbUser);
    }

}
