package com.jcohy.blog.service;

import com.jcohy.blog.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.List;

/**
 * Created by jiac on 2018/9/28.
 * ClassName  : com.jcohy.blog.service
 * Description  :
 */
public interface UserService {

    UserDetails loadUserById(Long id);

    /**
     * 使用用户名和用户类型获取用户信息
     * 不包含已删除
     * @param userName
     * @param type
     * @return
     */
    User getUser(String userName, Integer type);

    User findUserByPhone(String username, int type);

    List<String> getAllPermissionValueByUserId(Long userId);

    /**
     * 分页查询
     * @param Pageable
     * @return
     */
    Page<User> findAll(Pageable Pageable);


    /**
     *  查询
     * @return
     */
    List<User> findAll();


    /**
     * 用户注销
     * @param sessionId
     */
    void logout(String sessionId);


    /**
     * 新增或者更新用户
     * @param user
     */
    void saveOrUpdate(User user, Long userId);

    /**
     * 检查用户是否存在
     * @param name
     * @return
     */
    boolean checkUser(String name);

    /**
     * 注册用户
     * @param name
     * @param password
     * @param email
     * @param ip
     */
    void register(String name, String password, String email, String ip);

    /**
     * 删除用户
     * @param id
     */
    void delete(Long id);

    /**
     * 修改用户密码
     * @param user
     * @param oldpassword
     * @param password1
     * @param password2
     */
    void updatePassword(User user, String oldpassword, String password1, String password2);
}
