package com.jcohy.blog.service.impl;

import com.jcohy.blog.dto.LogDto;
import com.jcohy.blog.model.Log;
import com.jcohy.blog.repository.LogRepository;
import com.jcohy.blog.service.ConsoleLogService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by jiac on 2018/9/28.
 * ClassName  : com.jcohy.blog.service.impl
 * Description  :
 */
@Service
public class ConsoleLogServiceImpl implements ConsoleLogService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    private LogRepository logRepository;

    @Override
    @Transactional
    public void saveConsoleLog(Log log) {
        if (log != null) {
            logRepository.save(log);
        }
    }

    @Override
    public Page<LogDto> queryLog(String username, String module, String description, Long begin, Long end, PageRequest pageRequest){
        Page<Log> logsPage = logRepository.findAll(new LogSpec(username, module, description, begin, end, pageRequest.getSort()), pageRequest);
        List<LogDto> logDtoList = new ArrayList<>();
        List<Log> logList = logsPage.getContent();
        for (Log log: logList) {
            LogDto dto = log2LogDto(log);
            logDtoList.add(dto);
        }

        Page<LogDto> logDtos = new PageImpl(logDtoList, pageRequest, logsPage.getTotalElements());
        return logDtos;
    }

    private LogDto log2LogDto(Log log) {
        LogDto dto = new LogDto();
        if (log != null) {
            dto.setId(log.getId());
            dto.setUserId(log.getUserId());
            dto.setUsername(log.getUsername());
            dto.setModule(log.getModule());
            dto.setDescription(log.getDescription());
            dto.setStartTime(timestamp2Date(log.getStartTime()));
            dto.setSpendTime(log.getSpendTime());
            dto.setParams(log.getParameter());
        }

        return dto;
    }

    private String timestamp2Date(Long timestamp) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return format.format(timestamp);
    }

    private class LogSpec implements Specification<Log> {
        private String username;
        private String module;
        private String description;
        private Long begin;
        private Long end;
        private Sort sort;

        public LogSpec(String username, String module, String description, Long begin, Long end, Sort sort) {
            if (end != null) {
                end += 86399999;    // 当天最后一毫秒
            }
            this.username = username;
            this.module = module;
            this.description = description;
            this.begin = begin;
            this.end = end;
            this.sort = sort;
        }

        @Override
        public Predicate toPredicate(Root<Log> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
            Predicate p1 = cb.like(root.get("username").as(String.class), "%" + username + "%");
            Predicate p2 = cb.like(root.get("module").as(String.class), "%" + module + "%");
            Predicate p3 = cb.like(root.get("description").as(String.class), "%" + description + "%");
            Predicate p4 = cb.ge(root.get("startTime").as(Long.class), begin);
            Predicate p5 = cb.le(root.get("startTime").as(Long.class), end);

            List<Predicate> predicates = new ArrayList<>();

            if (StringUtils.isNotBlank(username)) { predicates.add(p1); }
            if (StringUtils.isNotBlank(module)) { predicates.add(p2); }
            if (StringUtils.isNotBlank(description)) { predicates.add(p3); }
            if (begin != null) { predicates.add(p4); }
            if (end != null) { predicates.add(p5); }

            Predicate[] pre = new Predicate[predicates.size()];
            query.where(predicates.toArray(pre));
            // 添加排序功能
            for (Iterator<Sort.Order> iterator = sort.iterator(); iterator.hasNext(); ) {
                Sort.Order order = iterator.next();
                if (order.getDirection().isAscending()) {
                    query.orderBy(cb.asc(root.get(order.getProperty())));
                } else {
                    query.orderBy(cb.desc(root.get(order.getProperty())));
                }
            }
            return query.getRestriction();
        }


    }
}
