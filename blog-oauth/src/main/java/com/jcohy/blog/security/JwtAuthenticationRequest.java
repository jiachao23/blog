package com.jcohy.blog.security;

import java.io.Serializable;

/**
 * Created by jiac on 2018/9/28.
 * ClassName  : com.jcohy.blog.security
 * Description  :
 */
public class JwtAuthenticationRequest implements Serializable {

    private static final long serialVersionUID = -8445943548965154778L;

    private String username;
    private String password;

    public JwtAuthenticationRequest() {
        super();
    }

    public JwtAuthenticationRequest(String username, String password) {
        this.setUsername(username);
        this.setPassword(password);
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("JwtAuthenticationRequest{");
        sb.append("username='").append(username).append('\'');
        sb.append('}');
        return sb.toString();
    }
}

