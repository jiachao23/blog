package com.jcohy.blog.security;

import com.jcohy.blog.common.Constants;
import com.jcohy.blog.model.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static java.util.stream.Collectors.toSet;

/**
 * Created by jiac on 2018/9/28.
 * ClassName  : com.jcohy.blog.security
 * Description  :
 */
public class JwtUserFactory {
    private JwtUserFactory() {
    }

    public static JwtUser create(User user, List<String> permValues) {
        return new JwtUser(
                user.getId(),
                user.getUsername(),
                user.getEmail(),
                user.getPassword(),
//                mapToGrantedAuthorities(user.getRoles()),
                mapToGrantedAuthorities(permValues),
                user.getState(),
                user.getLocked(),
                user.getLastPasswordReset()
        );
    }

    // 该用户所对应的角色
    private static Set<GrantedAuthority> mapToGrantedAuthorities(List<String> permValues) {
//        Stream<Set<Permission>> permStreams = authorities.stream().map(role -> role.getPermissions());
//        Stream<Permission> perms = permStreams.flatMap(child -> child.stream());
//        Set<GrantedAuthority> result = perms.map(perm -> new SimpleGrantedAuthority(perm.getPermissionValue())).collect(toSet());
        Set<GrantedAuthority> result = new HashSet<>();
        if(permValues != null && !permValues.isEmpty()) {
            result = permValues.stream().map(item -> new SimpleGrantedAuthority(item)).collect(toSet());
        }

        //为每个登录用户添加了访问平台登录用户共享功能的权限
        result.add(new SimpleGrantedAuthority(Constants.share.getPermissionValue()));

        return result;
    }
}
