package com.jcohy.blog.security;

import com.jcohy.blog.filter.BUserInfoHolder;
import com.jcohy.blog.service.UserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by jiac on 2018/9/28.
 * ClassName  : com.jcohy.blog.security
 * Description  :
 */
public class JwtAuthenticationTokenFilter extends OncePerRequestFilter {

    @Autowired
    private UserService userService;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Value("${jwt.header}")
    private String tokenHeader;
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String authToken = request.getHeader(this.tokenHeader);
        // authToken.startsWith("Bearer ")
        // String authToken = header.substring(7);
        //String username = jwtTokenUtil.getUsernameFromToken(authToken);
        Long userId = jwtTokenUtil.getUserIdFromToken(authToken);

        //logger.info("checking authentication user " + username);

        if (userId != null && SecurityContextHolder.getContext().getAuthentication() == null) {

            // It is not compelling necessary to load the use details from the database. You could also store the information
            // in the token and read it from it. It's up to you ;)
            //UserDetails userDetails = this.userService.getUserDetails(username);
            UserDetails userDetails = userService.loadUserById(userId);

            // For simple validation it is completely sufficient to just check the token integrity. You don't have to call
            // the database compellingly. Again it's up to you ;)
            if (jwtTokenUtil.validateToken(authToken, userDetails)) {
                UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                logger.info("authenticated user " + userId + ", setting security context");
                SecurityContextHolder.getContext().setAuthentication(authentication);
                attachUserInfo(userId);
            }
        }

        filterChain.doFilter(request, response);
    }

    private void attachUserInfo(Long userId) {
        try {
            if (StringUtils.isNotBlank(String.valueOf(userId))) {
                BUserInfoHolder.put(userId);
            } else {
                BUserInfoHolder.remove();
            }
        }catch (Exception e){
            logger.error("get user id from header error",e);
        }
    }
}
