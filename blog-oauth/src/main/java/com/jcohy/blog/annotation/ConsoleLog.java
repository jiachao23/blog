package com.jcohy.blog.annotation;

import java.lang.annotation.*;

/**
 * Created by jiac on 2018/9/28.
 * ClassName  : com.jcohy.blog.annotation
 * Description  :
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ConsoleLog {

    String module() default "";

    String description() default "";
}
