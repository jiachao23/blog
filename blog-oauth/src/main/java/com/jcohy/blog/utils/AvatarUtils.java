package com.jcohy.blog.utils;

import org.apache.commons.lang3.StringUtils;

/**
 * Created by jiac on 2018/9/28.
 * ClassName  : com.jcohy.blog.utils
 * Description  :
 */
public class AvatarUtils {

    public static String randomAvatar(String prefix, String seed){
        if (StringUtils.isBlank(seed)){
            return null;
        } else {
            return prefix + "t_" + (Math.abs(seed.hashCode()) % 16 + 1) + ".png";
        }
    }
}
