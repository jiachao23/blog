package com.jcohy.blog.utils;



import com.jcohy.blog.model.Menu;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;

/**
 * Created by jiac on 2018/5/22.
 * ClassName  : com.star.tp.utils
 * Description  :
 */
public class TreeUtils {


    public static List<Menu> getTreeList(List<Menu> menuList, Locale locale) {
        List<Menu> nodeList = null;
        if (menuList == null) {
            return null;
        } else {
            if (menuList.size() > 0) {
                nodeList = new ArrayList<>();
                //第一层循环
                for (Menu node1 : menuList) {
                    boolean mark = false;
                    //如果node1是顶级节点，直接加入结果列表
                    if (node1.getParentId() != null) {
                        //node1非顶级节点
                        //再次循环menuList，找node1的父节点，把node1加入到其父节点的子节点列表中
                        for (Menu node2 : menuList) {
                            //1.
                            //把node1加入到其父节点的子节点列表中
                            if (node1.getParentId().equals(node2.getId())) {
                                mark = true;
                                if (node2.getChildren() == null) {
                                    node2.setChildren(new LinkedHashSet<>());
                                }
                                if (!Locale.CHINA.equals(locale)) {
                                    if (!StringUtils.isBlank(node1.getEnName())) {
                                        node1.setName(node1.getEnName());
                                    }
                                }
                                node2.getChildren().add(node1);
                                break;
                            }
                        }
                    }

                    if (!mark) {
                        if (!Locale.CHINA.equals(locale)) {
                            if (!StringUtils.isBlank(node1.getEnName())) {
                                node1.setName(node1.getEnName());
                            }
                        }
                        nodeList.add(node1);
                    }
                }
            }
        }
        return nodeList;
    }

}
