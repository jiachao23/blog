package com.jcohy.blog.filter;

/**
 * Created by jiac on 2018/9/28.
 * ClassName  : com.jcohy.blog.filter
 * Description  :
 */
public class BUserInfoHolder {

    private static final ThreadLocal<BUserInfoHolder> cache = new ThreadLocal<>();

    private Long userId;

    private BUserInfoHolder(Long userId){
        this.userId = userId;
    }

    public static Long getUserId(){
        BUserInfoHolder holder = cache.get();
        if (holder == null) {
            return null;
        }
        return holder.userId;
    }

    public static void put(Long userId) {
        BUserInfoHolder holder = new BUserInfoHolder(userId);
        cache.set(holder);
    }

    public static void remove() {
        cache.set(null);
    }

}
