package com.jcohy.blog.repository;

import com.jcohy.blog.model.Options;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Copyright  : 2017- www.jcohy.com
 * Created by jiac on 2:28 2017/12/16
 * Email: jia_chao23@126.com
 * ClassName: OptionsRepository
 * Description:
 **/
public interface OptionsRepository extends JpaRepository<Options,Long> {
}
