package com.jcohy.blog.repository;

import com.jcohy.blog.model.Log;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * Created by jiac on 2018/9/28.
 * ClassName  : com.jcohy.blog.repository
 * Description  :
 */
public interface LogRepository extends JpaRepository<Log, Long>, JpaSpecificationExecutor {
}
