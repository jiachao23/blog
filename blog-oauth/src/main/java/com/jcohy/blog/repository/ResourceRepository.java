package com.jcohy.blog.repository;

import com.jcohy.blog.model.Resource;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Copyright  : 2015-2033 Beijing Startimes Communication & Network Technology Co.Ltd
 * Created by jiac on 2018/2/6 13:38.
 * ClassName  : ResourceRepository
 * Description  :
 */
public interface ResourceRepository extends JpaRepository<Resource,Long> {

    /**
     * 查询所有可见
     * @param visible
     * @return
     */
    List<Resource> findAllByVisible(boolean visible);
}
