package com.jcohy.blog.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import com.jcohy.blog.model.System;
import java.util.List;

/**
 * Created by jiac on 2018/9/29.
 * ClassName  : com.jcohy.blog.repository
 * Description  :
 */
public interface SystemRepository extends JpaRepository<System, Long> {
    List<System> findAllByStatusAndDeletedIsFalse(@Param("status") Integer status);
}
