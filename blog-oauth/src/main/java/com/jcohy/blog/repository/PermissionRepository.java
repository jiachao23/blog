package com.jcohy.blog.repository;

import com.jcohy.blog.model.Permission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Set;

/**
 * Created by jiac on 2018/9/28.
 * ClassName  : com.jcohy.blog.repository
 * Description  :
 */
public interface PermissionRepository extends JpaRepository<Permission, Long> {
    @Query(value = "select a from Permission a")
    List<Permission> findAllPermission();

    List<Permission> findAllByDeletedFalseOrderByOrders();

    List<Permission> findAllByPermissionValueAndDeleted(String permissionValue, Boolean deleted);

    List<Permission> findAllByPermissionValueAndDeletedAndIdIsNot(String permissionValue, Boolean deleted, Long id);

    List<Permission> findAllByUrlAndMethodAndDeleted(String permissionValue, String method, Boolean deleted);

    List<Permission> findAllByUrlAndMethodAndDeletedAndIdIsNot(String permissionValue, String method, Boolean deleted, Long id);

    List<Permission> findAllByNameAndDeletedAndParentIdIsNull(String name, Boolean deleted);

    List<Permission> findAllByNameAndDeletedAndParentIdIsNullAndIdIsNot(String name, Boolean deleted, Long id);

    List<Permission> findAllByParentIdAndNameAndDeleted(Long parentId, String name, Boolean deleted);

    List<Permission> findAllByParentIdAndNameAndDeletedAndIdIsNot(Long parentId, String name, Boolean deleted, Long id);

    @Query("select max(p.orders) from Permission p where p.parentId is null")
    Integer getMaxOrdersParentIsNull();

    @Query("select max(p.orders) from Permission p where p.parentId=:permissionId")
    Integer getMaxOrdersSubOfPermission(@Param("permissionId") Long permissionId);

//    @Query(nativeQuery = true,
//            value = "select p.* from auth_permission p JOIN auth_role_permission rp ON p.id = rp.permission_id "+
//                    "WHERE rp.role_id=:roleId ORDER BY orders")
//    List<Permission> getRolePermissionsByRoleId(@Param("roleId") Long roleId);

    List<Permission> findAllByDeletedFalseAndUrl(String url);

    @Query(nativeQuery = true,
            value = "SELECT distinct a.permission_value FROM jcohy_permission a INNER JOIN jcohy_role_permission b ON a.id=b.permission_id " +
                    "INNER JOIN jcohy_user_role c ON b.role_id=c.role_id " +
                    "WHERE a.deleted=0 and c.user_id=:userId " +
                    "AND FIND_IN_SET(a.id, getPermissionChildList(:permissionId))")
    List<String> findUserAuthorizedPermissions(@Param("userId") Long userId, @Param("permissionId") Long permissionId);

    @Query(nativeQuery = true,
            value = "SELECT  p.id, p.permission_value, p.url, p.name , group_concat(DISTINCT rdc.country_id) " +
                    "FROM  jcohy_permission p " +
                    "INNER JOIN jcohy_role_permission rp ON p.id = rp.permission_id " +
                    "INNER JOIN jcohy_user_role ur ON rp.role_id = ur.role_id " +
                    "LEFT JOIN jcohy_role_datascope_country rdc ON rp.role_id = rdc.role_id " +
                    "WHERE ur.user_id=:userId  AND p.deleted=0 AND p.status=1 " +
                    "GROUP BY p.id")
    List<Object[]> findUserAllAuthorizedCountry(@Param("userId") Long userId);

    @Query(nativeQuery = true,
            value = "SELECT  p.id, p.permission_value, p.url, p.name , group_concat(DISTINCT rdc.country_id) " +
                    "FROM  jcohy_permission p " +
                    "INNER JOIN jcohy_role_permission rp ON p.id = rp.permission_id " +
                    "INNER JOIN jcohy_user_role ur ON rp.role_id = ur.role_id " +
                    "LEFT JOIN jcohy_role_datascope_country rdc ON rp.role_id = rdc.role_id " +
                    "WHERE ur.user_id=:userId  AND p.deleted=0 AND p.status=1 " +
                    "AND permission_value=:permValue " +
                    "GROUP BY p.id")
    List<Object[]> findUserAuthorizedCountryOfOnePermission(@Param("userId") Long userId, @Param("permValue") String permValue);

    @Query(nativeQuery = true,
            value = "SELECT  p.id, p.permission_value, p.url, p.name , group_concat(DISTINCT rdc.country_id) " +
                    "FROM  jcohy_permission p " +
                    "INNER JOIN jcohy_role_permission rp ON p.id = rp.permission_id " +
                    "INNER JOIN jcohy_user_role ur ON rp.role_id = ur.role_id " +
                    "LEFT JOIN jcohy_role_datascope_country rdc ON rp.role_id = rdc.role_id " +
                    "WHERE ur.user_id=:userId  AND p.deleted=0 AND p.status=1 " +
                    "AND FIND_IN_SET(p.id, getPermissionChildList(:permId)) " +
                    "GROUP BY p.id")
    List<Object[]> findUserAuthorizedCountryOfOnePermissionAndSub(@Param("userId") Long userId, @Param("permId") Long permId);

    @Query(nativeQuery = true, value = "SELECT DISTINCT a.permission_value FROM jcohy_permission a " +
            "INNER JOIN jcohy_role_permission b ON a.id = b.permission_id " +
            "INNER JOIN jcohy_user_role c ON b.role_id = c.role_id " +
            "WHERE a.deleted=0 AND c.user_id = :userId ")
    List<String> findAllPermissionValueByUserId(@Param("userId") Long userId);

    List<Permission> findAllByPermissionValueAndMethodAndDeletedIsFalse(@Param("code") String code, @Param("method") String method);

}
