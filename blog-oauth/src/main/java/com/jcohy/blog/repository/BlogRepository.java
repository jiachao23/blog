package com.jcohy.blog.repository;

import com.jcohy.blog.model.Blog;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Copyright  : 2017- www.jcohy.com
 * Created by jiac on 2:26 2017/12/16
 * Email: jia_chao23@126.com
 * ClassName: BlogRepository
 * Description:
 **/
public interface BlogRepository extends JpaRepository<Blog,Long> {

    /**
     * 获取分享博客列表
     *
     *            推荐状态
     * @param privacy
     *            权限
     * @param pageable
     * @return
     */
    Page<Blog> findByPrivacyOrderByShareNumDesc(int privacy, Pageable pageable);


    /**
     * 根据浏览数量获取博客
     *
     * @param privacy
     *            权限
     * @param pageable
     * @return
     */
    Page<Blog> findByPrivacyOrderByReadNumDesc(int privacy, Pageable pageable);

    /**
     * 根据权限获取博客
     *
     * @param privacy 权限
     * @param pageable
     * @return
     */
    Page<Blog> findByPrivacy(int privacy, Pageable pageable);

    /**
     * 根据权限获取博客
     *
     * @param privacy 权限
     * @return
     */
    List<Blog> findByPrivacy(int privacy);
}
